Python - základy programování
=============================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Úvod do jazyka Python. 
V tomto uvodud se seznáníme se základy jazyka, jeho syntaxí, základními datovými typy a operacemi, které nám poskytují.
Také se podíváme na konstrukce pro řízení toku programu, jako jsou cykly a větvení.
Následně se naučíme definovat funkce a používat.
Poslední dvě témata jsou venována zpracování výjimek a prací s řetězci.

 
Náplň kurzu:
------------

- **Přehled základních vlastností jazyka**
  * Proměnné a reference
  * Standardní datové typy
  * Řídící struktury
- **Organizace projektu**
  * Funkce
  * Třídy a metody
  * Moduly a balíčky
  * Distribuce software
- **Funkce a metody**
  * Poziční a pojmenované parametry
  * Pokročilé zpracování argumentů
  * Vnořené funkce a funkcionální prvky
  * Globální, lokální a vázané proměnné
- **Objektový model**
  * Objektově orientovaný návrh
  * Datové typy a operace
  * Speciální metody
  * Deskriptory
  * Dědičnost a polymorfie
  * Vícenásobná dědičnost a mixins
  * Základní návrhové vzory
- **Generátory a iterátory**
  * Sekvenční datové typy
  * Generované sekvence
  * Čtení textových souborů
- **Ukládání a zpracování dat**
  * Ukládání objektů
  * Datové formáty
  * Přístup k databázi
- **Online komunikace**
  * Webový klient a server
  * Práce s API
- **Ladění a logování**
  * Ladění pomocí výpisů
  * Standardní logovací knihovna
  * Debuggery
- **Testování**
  * Testování knihoven a aplikací
  * Organizace testů
- **Paralelní zpracování**
  * Vlákna a procesy
  * Komunikace a synchronizace
  * Výkonnostní omezení Pythonu
- **Pokročilá témata**
  * Generování kódu**
  * Monkey patching
