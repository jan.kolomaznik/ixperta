import pyfoot
from pyfoot import World, Actor
import random

width, height = 10, 10


class Apple(Actor):

    def __init__(self):
        super().__init__(path='hadi/Graphics/apple.png')

class SnakeTelo(Actor):

    
    def __init__(self):
        super().__init__(path='hadi/Graphics/hadtelo.png')

class SnakeHead(Actor):

    def __init__(self):
        super().__init__(path='hadi/Graphics/snakehead.png')
        self.run = False
        self.telo = [None]*3


    def act(self):
        if pyfoot.is_key_down("w"):
            self.rotation = 0
        if pyfoot.is_key_down("s"):
            self.rotation = 180
        if pyfoot.is_key_down("a"):
            self.rotation = 90
        if pyfoot.is_key_down("d"):
            self.rotation = 270

        x, y = self.location

        if not self.run:
            self.run = True
        elif self.rotation == 0:
            self.y -= 1
        elif self.rotation == 180:
            self.y += 1
        elif self.rotation == 90:
            self.x -= 1
        elif self.rotation == 270:
            self.x += 1

        if self.get_intersecting(SnakeTelo):
            pyfoot.stop()

        apple = self.get_intersecting(Apple)
        if apple:
            self.telo.append(None)
            self.get_world().remove(apple)
            put_apple(self.get_world())
            self.get_world().set_speed(len(self.telo)//3)

        part = self.telo.pop()
        if (part is None):
            part = SnakeTelo()
            self.get_world().add(part)
        self.telo.insert(0, part)
        part.set_location(x, y)

        
        
        if not (0 <= self.x < width):
            pyfoot.stop()  
        if not (0 <= self.y < height):
            pyfoot.stop()
  

def create_snake(snake_world):
    snake_head = SnakeHead()
    snake_head.set_location(width//2, height//2)
    snake_world.add(snake_head)

def put_apple(snake_world):
    apple = Apple()
    
    x = random.randrange(0, width)
    y = random.randrange(0, height)

    apple.set_location(x, y)
    snake_world.add(apple)
    

def init_world():
    snake_world = World(width,height,64)
    snake_world.set_bg('hadi/Graphics/tile.png')
    snake_world.set_speed(1)
    create_snake(snake_world)
    put_apple(snake_world)

if __name__  == '__main__':
    init_world()
    pyfoot.set_title("Snake")
    pyfoot.start()