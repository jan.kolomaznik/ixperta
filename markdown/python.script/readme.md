# Tvorba skriptů

## Úvod
Python je Interpretovaný jazyk, to znamená, že program se nepřekládá do instrukcí procesoru a nespouští se přímo.

Pro spuštění programu v pythonu tedy potřebujeme jiný program – interpret (jazyka Python).

Spuštění programu napsaném v Pythonu tak představuje spuštění interpretu, kterému se jako parametr předá soubor s programem.

### Zdrojový kód
Psát program znamená psát v libovolném textovém editoru zdrojový kód.

My jsme zatím používali nástroj Jupyter a jeho souboru **.ipynb**, ale klasičtější variantou je psát kód v Pythonu do souboru s příponou **.py**.

> #### Příklad
> Vytvořte soubor s názvem `greeting.py`, který bude obsahovat následující kód:
```python
def greeting(name = "World"):
   print(f"Hello {name}!")
  
greeting()
```
> A soubor spustěte přes příkazovou řádku pomoci příkazu: `python3 greeting.py`

#### Zjednodušení na linuxu
I v Linuxu se dá spouštění pythoního programu trochu vylepšit.

V Linuxu lze z pythoního programu (souboru se zdrojovým kódem) udělat spustitelný soubor příkazem `chmod a+x hello.py`.

Když takový soubor spustíme, pokusí se ho vykonat shell, takže je nutno shellu dát nějak vědět, že tento soubor má předat pythonímu interpretu.

V Linuxu se na to používají přípony souboru, ale technika prvního komentářového řádku, na kterém shellu předáváme příslušné informace, co s tím souborem má dělat.

V takovém případě je možná lepší ani neuvádět koncovku **.py**.

V tomto smyslu vypadá upravený program takto:
```python
#!/usr/bin/python3

print("Hello world!")
```

> #### Příklad (linux)
> Pokud pracujete na linux `hello.py` tak aby se spouštěl jako program: “./hello.py”.

V následujících příkazech budu používat `#!/usr/bin/python3` abychom demonstrovali, že jsou určené pro spouštění přes konzoli, ne přes Jupyter.

## Argumenty
V operačním systému, lze každý program spustit s argumenty `hello.py Pepa`

Pokud chcete zjistit všechny argumenty programu, musíte načíst knihovnu `sys`.
```python
#!/usr/bin/python3

import sys

print('Number of arguments:', len(sys.argv), 'arguments.')
print('Argument List:', str(sys.argv))
```

> #### Příklad
> Upravte soubor `hello.py` tak, aby v případě zadání právě jednoho argumentu: jména, vypsal: Hello jmeno!

## Import vlastních souborů
Máme teď dva soubory, jeden `hello.py`, který umí zpracovat vztup a druhý: `greeting.py`, který má funkci na pozdravení.

Pojďme je tedy spojit v jednom použít ten druhý, takže upravme `hello.py`:
```python
#!/usr/bin/python3
import greeting
import sys

name = sys.argv[1]
greeting.greeting(name)
```

Co se vypíše? A proč?

## Main script a proměnná `__name__`
Tato proměnná je přístupná v každém skriptu a obsahuje jeho jméno.

Jediná výjimka je skript, kterým byla celá aplikace spuštěná, ten má přiřazeno jméno `__main__` nezávisle na jeho skutečném jménu.

Toho se využívá tak že kód, který se má vykonat když je skript spuštěn se schová do bloku
```python
if __name__ == "__main__":
    pass
```

> #### Příklad
> Upravte scripty `hello.py` a `greeting.py` tak, aby každý vypisoval pozdrav pouze když je použit jako spouštěcí skript.

