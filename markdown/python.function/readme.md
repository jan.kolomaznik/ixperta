# Funkce
Funkce je část programu, kterou je možné opakovaně volat z různých míst kódu. 

- **vstup** - argumenty
- **výstup** - návratová hodnota 

![](media/function.png)

Definice funkce
```python
def secist(x, y):
    print("x je {}, y je {}".format(x, y))
    return x + y
```

Volání funkce

```python
secist(5, 6)
```

Lze při volání pojmenovat argumenty a pak je zadat v libovolném pořadí
```python
secist(y=6, x=5)
```

> #### Příklad:
> vytvoře funkci `mocnina`, která bude mít dva parametry: `zaklad` a `exponent` 
> 
> Funkci zavolejete ale zadejte prvni mocninu a pak základ. 
> 
```python
# Řešení
```

Pozor, funkci také nelze volat dříve než je nadefinována.
```python
funkce(1)
def funkce(a):
    print (a)
```

Atributy funkce jsou předávány jako reference
```python
def pridej(seznam, prvek):
    seznam.append(prvek)

a=[]
b=10
pridej(a,b)
a
```

Pokud ve funkci nedefinujeme návratovou hodnotu, vrací `None`
```python
def vypis():
    print('Telo funkce vypis()')
    
print("Vypis vraci:", vypis())
```

Občas se hodí nic nedělat
```python
def nic():
    pass
nic()
```

> #### Příklad:
> Vytvořte funkci jež navrátí **n-te** číslo fibonacciho posloupnosti. 
>
> *Každé číslo v posloupnosti je součtem dvou předchozích - [0, 1, 1, 2, 3, 5, 8]*
```python
# Řešení: definice funkce
```

```python
# Řešení: volání funkce
```

Typ parametru není typově určen.
```python
def funkce(a,b):
    return a+b

print(funkce(1,2))
print(funkce("a","b"))
```

## Výchozí hodnoty argumentů
Argumenty funkce mohou mít výchozí hodnotu, v takovém případě se nemusí při volání funkce zadávat.

**POZOR: Povinné argumenty musí být před nepovinnými!**
```python
def mocnina(x, exponent=2):
    return x ** exponent
    
print("Výchozí hodnota atributu", mocnina(3))
print("Změněná hodnota atributu", mocnina(3, exponent=4))
```

Parametry jsou předány na základě pozice.
```python
def funkce(p1, p2=4, p3='a'):
    print(p1,p2,p3)

funkce(1) # pouze povinný
funkce(1,2) # povinný a nepovinný
funkce(1,2,3) # všechny
funkce(p1=1,p2=2,p3=3) # jména
funkce(1,p3=2,p2=3) # jiné pořadí
funkce() # chyba
```

> #### Příklad:
> Vytvořte funkci jež bude obsahovat povinné a nepovinné parametry. 
> Tato funkce pozdraví uživatele následujícím způsobem
>
> `Ahoj <jmeno> <prijmeni>`
>
> přičemž parametr příjmení bude nepovinný. 
```python
# Řešení
```

Modifikace výchozího parametru
```python
def f(a, L=[]):
    L.append(a)
    return L

print(f(1))
print(f(2))
print(f(3))
```

## Proměnný počat argumentů
Proměnný počat argumentů: **poziční** = záleží na pořadí
```python
def zpracuj_posledni_soubor(*soubory):  # proměnný počet pozičních
    return "zpracováno: " + soubory[-1]

zpracuj_posledni_soubor('soubor1', 'soubor2', 'soubor3')
```

> #### Příklad:
> Vytvořte funkci, která převezme lib počet slov (řetězců) a spojí je ho jedné věty.
```python
# Řesení
```

Proměnný počat argumentů: **pojmenované** = musejí se pojmenovat
```python
def vypis_cele_jmeno(**data):  # proměnný počet pojmenovaných

    if "jmeno" in data and "prijmeni" in data:
        print(  "{} {}".format(data["jmeno"], data["prijmeni"])  )

    elif "prijmeni" in data:
        print(data["prijmeni"])
    
    elif "jmeno" in data:
        print(data["jmeno"])
    
    else:
        print("(neznámá osoba)")

vypis_cele_jmeno(jmeno="Milan", vyska=180, vek=20, prijmeni="Frajer")
```

### Konvence:
- **poziční** = `*args`
- **pojmenované** = `**kwargs`


Lze použít obojí najednou
```python
def vypis_vse(*args, **kwargs):
    print(args, kwargs)  # print() vypíše všechny své parametry

vypis_vse(1, 2, a=3, b=4)
```

Převod struktur na argumenty
__`*`__ nebo __`**`__ lze použít při volání funkce k rozbalení N-tic nebo slovníků!

```python
ntice = (1, 2, 3, 4)
slovnik = {"a": 3, "b": 4}

vypis_vse(ntice)                 # = vypis_vse((1, 2, 3, 4)) – jeden parametr, N-tice
vypis_vse(*ntice)                # = vypis_vse(1, 2, 3, 4) – čtyři parametry
vypis_vse(**slovnik)             # = vypis_vse(a=3, b=4) – dva pojmenované parametry
vypis_vse(*ntice, **slovnik)     # = vypis_vse(1, 2, 3, 4, a=3, b=4) – mix
```

## Dokumentace funkce
```python
def complex(real=0.0, imag=0.0):
    """Form a complex number.

    Keyword arguments:
    real -- the real part (default 0.0)
    imag -- the imaginary part (default 0.0)
    """
    pass
    
help(complex)   
```

## Ukazatele na funkce a návratové typy

Jako parametr můžeme předat i funkci
```python
def vypisPlus1(a):
    print(a+1)
def vypisPlus2(a):
    print(a+2)
def funkce(f,a):
    f(a)
funkce(vypisPlus1,10)
```

Více navratových hodnot
```python
def f():
    return 10,20

a,b = f()
print(a,b)
print(f())
```

```python
def secti(a,b):
    return a,b,a+b

"Prvni {0[0]}, Druhá {0[1]}, Soucet {0[2]}".format(secti(1,2))
```

> #### Příklad:
> Vytvořte funkci `swap()`, které dostane dva argumenty a vrátí je v opačném pořadí.
```python
# Řešení
```

Funkce je také možné definovat za běhu programu, třeba podle podmínky ...
```python
a = "a"
if a == "a":
    def funkce():
        print("funkce 1")
else:
    def funkce():
        print("funkce 2")
funkce()
```

