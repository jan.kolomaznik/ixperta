# Třídy

Třídy jsou programové struktury, které sdružují data (ve formě atributů) a finkcemi (ve formě metod).
Vytáří se tak nový datový typ, od kterého můžeme vytvořit libovolný počet instací.

Syntxe tříd v jazyce Python vychází z [C++](https://en.wikipedia.org/wiki/C%2B%2B), jazyka [Modula-3](https://en.wikipedia.org/wiki/Modula-3) a jazyka [Smalltalk](https://cs.wikipedia.org/wiki/Smalltalk).
Třídy Pythonu poskytují všechny standardní přístupy Objektově orientovaného programování: 
- **dědičnost tříd**: umožňuje více základních tříd, ptrhon podporuje podobně jako C++ výcenásobnou dědičnost. 
- **zapouzdření**: objekty mohou obsahovat libovolné množství a druhy dat. 
- **polymorfismus**: třídy jsou v pythonu dynamické a jsou vytvářeny za běhu. 
  Lze je tedy i po vytvoření dále upravovat.
  Podporovné implementace polymorfizmu (podle wikipedie) jsou:
  * jednomu objektu volat jednu metodu s různými parametry (ad-hoc polymorfismus);
  * objektům odvozeným z různých tříd volat tutéž metodu se stejným významem v kontextu jejich třídy;
  * přetěžování operátorů neboli provedení rozdílné operace v závislosti na typu operandů;
  * jedné funkci dovolit pracovat s argumenty různých typů.

### Jako v C++ ...
V terminologii C ++ jsou obvykle atributy a metody veřejné (i když Python umožňuje definovat i soukromé proměnné) a všechny metody jsou virtuální. 

Dále je možné většinu operátorů se speciální syntaxí (aritmetické operátory, indexování atd.) předefinovat pro instance třídy.

### Jako v Modula-3 ...
Stejně jako v Modula-3 neexistují žádné zkratky pro odkazování na členy objektu z jeho metod.
Metody jsou deklarovány s explicitním prvním argumentem představujícím objekt, který je implicitně poskytován voláním. 

### Jako v Smalltalku ...
Stejně jako v Smalltalku jsou i samotné třídy objekty. 
Tento objekt poskytuje sémantiku pro import a přejmenování.

Vestavěné typy použít jako základní třídy pro rozšíření uživatelem. 
