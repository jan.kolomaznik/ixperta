# Deskriptory

Jeden z nejmagičtějších operátorů v Pythonu je `.`, tečka.
Je magický v obou významech – většina lidí ho používá, ačkoli nemá tušení, co přesně dělá, a dá se předefinovat tolika různými způsoby, že to vydá na [celou přednášku](https://www.youtube.com/watch?v=NiSqG6s8skA).

Pomocí tečky zapisujeme tři operace: čtení atributu (`print(foo.bar)`), zapisování (`foo.bar = 3`) a mazání (`del foo.bar`).
Tady se zaměříme hlavně na nejmagičtější z nich, čtení.

Kdykoli atribut čteme pomocí tečky, hledá se několika místech:

* na samotné instanci objektu,
* pokud se tam nenajde, tak na třídě,
* pokud se nenajde ani tam, tak na rodičovských třídách (v případě vícenásobné dědičnosti podle [MRO](https://www.python.org/download/releases/2.3/mro/)),
* a pokud stále není k nalezení, vyhodí se `AttributeError`.

To je trochu zjednodušený, ale užitečný model.

Speciální metody, které se nevolají pomocí tečky, přeskakují první krok: metoda `__add__` tedy musí být definována na *třídě*, aby se zavolala pro `a + b`.

Podívejme se teď na získávání atributu trošku podrobněji. 
Je to poměrně komplikovaný proces a existuje několik způsobů, jak ho přizpůsobit. 

Nejjednodušší je dvojice speciálních metod:
* `__getattribute__`, která *kompletně předefinuje* funkci `.` pro čtení atributu, a
* `__getattr__`, která se zavolá, až když se atribut nenajde normálním způsobem.

První z nich nedoporučuji používat, protože je *příliš* obecná (pokusy se z ní dostat ke stavu objektu končívají nekonečnou rekurzí).
Příklad druhé:

```python
class Palette:
    red = 255, 0, 0
    green = 0, 255, 0
    
    def __getattr__(self, attr_name):
        prefix, sep, suffix = attr_name.partition('_')
        if prefix == 'dark':
            original_color = getattr(self, suffix)
            return tuple(c//2 for c in original_color)
        else:
            raise AttributeError(attr_name)

palette = Palette()
print(palette.dark_red)
```

(Předpokládám že znáte funkci `getattr`; kdyby ne: `getattr(foo, "bar")` dělá totéž co `foo.bar` – jen je jméno atributu předáno jako řetězec, takže může být např. v proměnné. Podobně existují `setattr(instance, attr_name, new_value)` a `delattr(setattr(instance, attr_name)`.)

Metoda `__getattr__` je většinou tak trochu kanón na vrabce: ve většině případů nepotřebujeme nastavit chování *všech* neexistujících atributů, ale jenom jednoho nebo několika konkrétních.
Například máme třídu pro 2D bod s atributy `x` a `y` a potřebujeme i atribut pro dvojici `(x, y)`.
Toto se často dělá pomocí dekorátoru `property`:

```python
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    @property
    def pos(self):
        return self.x, self.y

point = Point(41, 8)
print(point.pos)
```

Jak to ale funguje? Dekorátor `property` je třída, jakou můžete teoreticky napsat sami v Pythonu.
Je to *deskriptor*, objekt, který v rámci nějaké třídy *popisuje* jak přistupovat k nějakému atributu.

Nejlépe se deskriptory vysvětlí na příkladu:

```python
# (Omluvte prosím češtinu v kódu)

class Descriptor2D:
    """Popisuje atribut, který kombinuje dva jiné atributy do dvojice"""

    def __init__(self, name1, name2):
        self.name1 = name1
        self.name2 = name2
    
    def __get__(self, instance, cls=None):
        """Volá se, když je třeba načíst atribut dané `instance` na dané třídě `cls`.
        """

        if instance is not None:
            # Je-li instance nastavena, čteme atribut z ní.
            return getattr(instance, self.name1), getattr(instance, self.name2)
        else:
            # Je-li instance None, čteme atribut přímo ze třídy `cls`;
            # v tomto případě slušné deskriptory většinou vrací deskriptor samotný.
            return self

class Rect:
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
    
    pos = Descriptor2D('x', 'y')
    size = Descriptor2D('w', 'h')

rect = Rect(1, 2, 3, 4)
print(rect.pos)
print(rect.size)

# Čtení atributu přímo ze třídy:
print(Rect.pos)
```

Deskriptory jsou tedy součást třídy – atributy s nějakým jménem. Popisují, jak se bude přistupovat k atributu daného jména.

Existují dva druhy deskriptorů: *data descriptor* a *non-data descriptor*.
Liší se v tom, jestli popisují jen, jak se daný atribut *čte*, nebo i jak se do něj *zapisuje*.
Výše uvedený deskriptor je *non-data*: ovládá jen čtení. Zápis funguje jako u normálních atributů:
přepíše aktuální hodnotu – a nová hodnota se pak použije místo volání deskriptoru:

```python
rect.pos = 'haha'
print(rect.pos)
```

Abychom tomu zabránili, můžeme na deskriptoru nadefinovat speciální metodu `__set__` (nebo `__delete__`), která popisuje,
jak se atribut nastavuje (resp. maže).
Tím vznikne *data descriptor*:


```python
class Descriptor2D:
    def __init__(self, name1, name2):
        self.name1 = name1
        self.name2 = name2
    
    def __get__(self, instance, cls=None):
        if instance is not None:
            return getattr(instance, self.name1), getattr(instance, self.name2)
        else:
            return self

    def __set__(self, instance, new_value):
        a, b = new_value
        setattr(instance, self.name1, a)
        setattr(instance, self.name2, b)

    def __delete__(self, instance):
        delattr(instance, self.name1)
        delattr(instance, self.name2)

class Rect:
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
    
    pos = Descriptor2D('x', 'y')
    size = Descriptor2D('w', 'h')

rect = Rect(1, 2, 3, 4)
rect.pos = 123, 456
print(rect.pos)
```

Už zmíněný vestavěný deskriptor `property` je *data descriptor*.
Popisuje jak čtení, tak zápis atributu. Pokud mu nenastavíme funkci pro zápis, vyhodí ze své metody  `__set__` výjimku `AttributeError` se zprávou, že do atributu se zapisovat nedá. (To je trochu magická odchylka od normálního chování Pythonu, kdy atributy zapisovat jdou.)

Nejčastější příklad *non-data* deskriptoru je obyčejná funkce.
Každá funkce totiž funguje jako deskriptor: má speciální metodu `__get__`, která zajišťuje, že pokud je nastavena na třídě, daným atributem nedostaneme *funkci*, ale *metodu* (s „předvyplněným“ parametrem `self`).

```python
def foo(self):
    return 4

class C:
    foo = foo

c = C()
    
# Obyčejná funkce
print(C.foo)
print(foo)

# Metoda
print(C().foo)
print(foo.__get__(c))
```

Protože je to *non-data* deskriptor, můžeme v jednotlivých instancích třídy daný atribut přepsat něčím jiným, čímž metodu znepřístupníme.


Jako zajímavost uvedu *non-data* deskriptor, který přepisuje svůj vlastní atribut.
Funguje podobně jako `@property`, jen se výsledek vypočítá pouze jednou a uloží se jako normální atribut.
Při dalším přístupu k atributu už se použije uložená hodnota.

```python
class reify:
    def __init__(self, func):
        self.func = func

    def __get__(self, instance, cls=None):
        if instance is None:
            return self
        val = self.func(instance)
        setattr(instance, self.func.__name__, val)
        return val

class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    @reify
    def length(self):
        print('Running expensive computation...')
        return (self.x ** 2 + self.y ** 2) ** 0.5

vect = Vector(3, 4)
print(vect.length)
print(vect.length)
print(vect.length)
```

Kompletní implementace je např. ve frameworku Pyramid jako [pyramid.decorator.reify](http://docs.pylonsproject.org/projects/pyramid/en/latest/_modules/pyramid/decorator.html).


---
#### Tyto materiály vycházejí z materálů ČVUT
Uprav tuto stránku na [GitHubu](https://github.com/cvut/naucse.python.cz/blob/b181/lessons/intro/magic/index.md)

Pro kurz MI-PYT na ČVUT napsali Miro Hrončok, Petr Viktorin a další, 2016-2017.

Licence: [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)

Licence ukázek kódu: [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)