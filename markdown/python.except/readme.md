# Chyby a výjimky

**Výjimka** (z angličtiny Exception) je v informatice chápána jako výjimečná situace, která může nastat za běhu programu.

```python
1/0
```

Lze vyčíst:
- typ
- popis
- call stack (soubory, řádky)


## Zachycení a ošetření výjimek
```python
try:
    x = int(input("Zadej nulu: "))
    y = 6 / x

except ZeroDivisionError:
    print("Nelze dělit nulou.")
```

Získání typu výjimky přímo v kódu
```python
try:
    x = int(input("Zadej nulu: "))
    y = 6 / x

except ZeroDivisionError as e:
    print("Nelze dělit nulou.", e)
```

Zachycení výce typů výjimek
```python
try:
    x = int(input("Zadej nulu nebo písmeno: "))
    y = 6 / x

except ZeroDivisionError as e:
    print("Nelze dělit nulou: ", e)
    
except ValueError as e:
    print("Nezadáno číslo: ", e)    
```

Zachycení výce výjimek naráz
```python
try:
    x = int(input("Zadej nulu nebo písmeno: "))
    y = 6 / x

except:
    print("Zachycena nějaká chyba.")
```

Zachycení více vyjímek naráz s vymenováním
```python
try:
    x = int(input("Zadej nulu nebo písmeno: "))
    y = 6 / x

except (ZeroDivisionError, ValueError) as e:
    print("Zadáno pímeno nebo nula: ", e)
    
except:
    print("Zachycena nějaká chyba.")
```

Provedení kódu vždy, neávisle na tom jetli výjimka vznikla nebo ne
```python
try:
    x = int(input("Zadej nulu nebo písmeno: "))
    y = 6 / x

except:
    print("Zachycena nějaká chyba.")

finally:
    print("Tothle se vypíše vždy!")
```

Provedení kusu kódu, když výjimka nevznikla
```python
try:
    x = int(input("Zadej číslo: "))
    y = 6 / x

except:
    print("Zachycena nějaká chyba.")

else:
    print("Tohle se když výjimka nevznikla!")
```

> #### Příklad
> Vymyslete kód, který vyvolá vájimku a tu zachyťe a vypište. 
```python
# Řešení
```

## Kontext výjimek
CO se stane tady?
```python
try:
    a=10
    a/0
    b=20
except:
    pass
print(b)
```

Jaký je rozdíl teď?
```python
try:
   a=10
   a/0
except:
   pass
print(a)
```

## Vyvolání výjimky
```python
raise ValueError("Nesprávná hodnota.")
```

Zachycení výjimky, její ošetření a předání dál
```python
try:
    raise ValueError("Nesprávná hodnota.")
    
except ValueError:
    print("Něco se porouchalo...")
    raise
```

Nejlepší varianta: zachyceni změna typu a vyvolání dál
```python
try:
    raise FileNotFoundError("Soubor neexistuje.")
    
except FileNotFoundError as e:
    raise ValueError("Zadal jste špatné jméno souboru.") from e
```

> #### Příklad:
> Načtěte od uživatele poloměr kruhu a vypočtěte jeho obsah.
> - Pokud nezadá číslo, přinuťte ho opakovat zadání.
> - Pokud zadá záporné číslo, vyhoďte výjimku ArithmeticError. 
```python
# Řesení
```

  
