# Testovací framework (pytest)

Zatím jsme se dívali na možnosti Pythonu pro testy.
Prošli jsme si používání `assert` a __doctest__.

Na testy je v samotném Pythonu zabudovaná knihovna unittest.
Ta je ale celkem složitá na použití, proto ji my používat nebudeme.
Nainstalujeme si knihovnu __pytest__, která se používá mnohem jednodušeji a je velice populární.

Toto není jeinná knihona pro spasní testů v pythonu, přímo na wiki stránkách Pythonu je k dispozci senzam dospupných knihoven, které se zabývají tímto tématem: [PythonTestingToolsTaxonomy](https://wiki.python.org/moin/PythonTestingToolsTaxonomy).

Knihovny se instalují pomocí příkazu `pip`.
_Pokud tvoří v noteboocích, pak nezapomeňte na virtuální prostředí._
```
!pip install pytest
```

Import knihovny pak již klasicky:
```python
import pytest
```

## Kam psát testovacích funkce
Než se pustíme do samotného pracovní testů, pojďme se podívat na organizační stránku věci.

Testy můžete v podstatě napsat kdekoliv, přesto ale existují jistá doporučení a pravidla:

Pokud je váš projekt ještě malý a v začátcích, může být nejsnazší prostě vytvořit soubor _test.py_ a adresáři projektu/modulu.

    project/
    ├── my_modul/
    │   └── __init__.py
    └── test.py

S postupem časů je výhodnější vytvořit adresář _tests_ a v něm mít soubory začínající (nebo končící) slovem _test_.

    project/
    ├── my_modul/    <-- nebo src/
    |   ├── __init__.py
    |   ├── boo.py
    │   └── foo.py
    └── tests/
        ├── __init__.py
        ├── test_boo.py
        └── test_foo.py

Tato struktura řešení většinu oběžných obtíží, se kterými se přání kódu v Pythonu můžete setkat.
Pro více výhodách se dožtete v blogu [Ionel Cristian Mărieș](https://blog.ionelmc.ro/2014/05/25/python-packaging/#the-structure).

Případné soubory potřebné k testování bývá zvykem dávat do složky fixtures ve složce s testy.

## Spouštění testů
Když již máme testy napsané, jak je spouštět:

### Pomocí příkazové řádky
V kořenovém adresáči je soubor [test_mark.py](test_mark.py), ve které jsou define dummy metody a třídy.

Spuštění testů se pak provede příkazem:
```shell script
!pytest -v test_mark.py
```

V případě pytest platí:
- V testovacím souboru stačí mít funkci pojmenovanou `test_*` a __pytest__ pozná, že se jedná o test.
- V testech používáme obyčejný `assert`, nikoliv metody jako například v __unittest__.

Co se má testovat, se pytestu dá zadat pomocí argumentů příkazové řádky.
Můžou to být jednotlivé soubory nebo adresáře, ve kterých pytest rekurzivně hledá všechny soubory začínající na `test_`.
Vynecháme-li argumenty úplně, hledá rekurzivně v aktuálním adresáři.

```shell script
!pytest -v
```

> _To se často hodí, ale obsahuje-li aktuální adresář i vaše virtuální prostředí, pytest prohledá i to a často v něm najde nefunkční testy._

Pokud pytest nemůže naimportovat váš modul, můžete udělat několik věcí:
- Nainstalovat svůj balíček (například v režimu _develop_).
- Nastavit proměnnou prostředí _PYTHONPATH_ na ..

Testovat nainstalovaný balíček je výhodnější – ověříte zároveň, že nainstalovaný modul se chová dle očekávání. Je dobré testy psát tak, aby šli pouštět z jakéhokoliv adresáře, a pro jistotu je spouštět odjinud, než z adresáře s kódem. Odhalíte tím často balíčkovací chyby.

### Filtrování testu
Jak projekt postupně roste, tak se zvyšuje množství testů.
Postupem času se ukáže velmi náročné pouštět všechny testy, už jen z toho pohledu, že jejich průběh zabere větší množství času.

Další motivací je, že můžete použít jednu knihovnu testů na různé úrovně testů z testovací pyramidy.
Tedy na unit testy, integrační a akceptační, ...
Pro vývojáře pracujícím na určitém moulu nemá často smysl pouštět pravidelně všechny testy celé aplikace.

V takovém případě je dobré testy nějak filtrovat a třídit.
`pytest` poskytuje několik způsobů, jak toho dosáhnout:

- *Filtrování podle názvu*: `pytest` můžete omezit na spuštění pouze těch testů, jejichž plně kvalifikované názvy odpovídají konkrétnímu výrazu.
 Můžete to udělat pomocí parametru `-k`.
- _Rozsah adresáře_: Ve výchozím nastavení spustí pytest pouze ty testy, které jsou v aktuálním adresáři nebo pod ním.
- _Kategorizace testů_: `pytest` může zahrnovat nebo vyloučit testy z konkrétních kategorií, které definuje. Můžete to udělat pomocí parametru `-m`.

Zejména kategorizace testů je velmi mocný nástroj.
`pytest` umožňuje vytvářet vlastním značky (`mark`).
Test může mít více značek. Během spouštění můžete definovat, které testy se mají podle značek pouštět a které ne:

Podívejte se na soubor [test_mark.py](test_mark.py), ve které jsou define dummy metody a třídy.
```python
import pytest

@pytest.mark.webtest
def test_send_http():
    pass  # perform some webtest test for your app


def test_something_quick():
    pass


@pytest.mark.another
def test_another():
    pass


class TestClass:
    def test_method(self):
        pass
```

Nyní se pokusím spustit jen testy se značkou `webtest`:
```shell script
!pytest -v -m "webtest" test_mark.py
```

Nebo naopak bez značky `webtest`:
```shell script
!pytest -v -m "not webtest" test_mark.py
```

Další možností je spustit konkrétní test v konkrétní třídě:
```shell script
!pytest -v test_mark.py::TestClass::test_method
```

Nebo pustit všechny testy v rámci jedné třídy:
```shell script
!pytest -v test_mark.py::TestClass
```

A samozřejmě vše můžete kombinovat a vyjmenovat jen určitou skupinu testů, které se mají spusti:
```shell script
!pytest -v test_mark.py::TestClass test_mark.py::test_send_http
```

Idntifikátory testou jsou ve tvaru: `module.py::class::method` nebo `module.py::function`.
Identifikátory také definuje seskupení testům takže například: `module.py::class` vybere všechny testovací metody ve třídě.
Testy, se také daji rozlišit podle paramtrue `fixture`, např. `module.py::function[param]`, který budem probárat později.

Parametr `-rf` vypíše identifikátory pro neúspěšné testy.
Pomocí přepínače `--collectonly` vypíše `pytest` seznam identifikátorů a nespustí žádné testy:
```shell script
!pytest --collectonly test_mark.py
```

### Pytest parametr `-k exp`
Pomocí přepínače `-k` a výrazu můžeme filtrovat posty podle částečné shodu s názvem testu a ne s přesnou shodou, jako je tomu u přepínače `-m`.
řádku můžete zadat výraz, který implementuje dílčí shodu na názvy testů místo přesné shody na značkách, které poskytuje -m.
To usnadňuje výběr testů na základě jejich jmen:

Spuštění testů, které obsahují v názvu **html**.
```shell script
!pytest -v -k http test_mark.py
```
Nebo je možné k tomu přistoupit i opacne a pustit testy,které neobsahují `html`
```shell script
!pytest -k "not http" -v test_mark.py
```

Dokonce můžete použít operátor `or` a pustit testy které splňují dejden z výrazů:
```shell script
pytest -k "http or quick" -v test_mark.py
```

Podobně jako  `not` a `or` je možné použít i operátor `and` a kulaté závorky `(`, `)`.

### Registrace vlastních značek.
Registrace vlastní značek se prování pomocí souboru: [pytest.ini](pytest.ini)

```ini
# content of pytest.ini
[pytest]
markers =
    webtest: mark a test as a webtest.
    slow: mark test as slow.
```

Všechny dostupné značky si pak můžeme vypsat příkazem:
```shell script
!pytest --markers
```

Když definuje vlastní markery je dobré pouštět testy s přepínačem `--strict-markers`, který odhalí všechny nedefinované markery:
```shell script
!pytest --strict-markers
```

Pokud použijeme marker u třídy, pak označit všechny metody v této třídě, a je to kázáno v souboru [test_mark_classlevel.py](test_mark_classlevel.py)
```python
import pytest

@pytest.mark.webtest
class TestClass:
    def test_startup(self):
        pass

    def test_startup_and_more(self):
        pass
```

Pak otestujte příkazem:
```shell script
!pytest -v -m "webtest" test_mark_classlevel.py
```

### PLatformově závislé testy
Další hezkou vlastností je filtrace testů podle platformy.
V pytesy můžeme sety vytvořit markery jako `pytest.mark.darwin`, `pytest.mark.win32` atd.
Stále můžeme mít testy vez teď to značek, které poběží na všech platformách.
Pokud chcete spustit testy pouze pro konkrétní platformu, můžete použít následující plugin:

```python
# content of conftest.py
#
import sys
import pytest

ALL = set("darwin linux win32".split())

def pytest_runtest_setup(item):
    supported_platforms = ALL.intersection(mark.name for mark in item.iter_markers())
    plat = sys.platform
    if supported_platforms and plat not in supported_platforms:
        pytest.skip("cannot run on platform {}".format(plat))
```

nyní můžeme použít marker pro filtraci testu podle plaformy v souboru [test_mark_platform.py](test_mark_platform.py):
```python
import pytest


@pytest.mark.darwin
def test_if_apple_is_evil():
    pass


@pytest.mark.linux
def test_if_linux_works():
    pass


@pytest.mark.win32
def test_if_win32_crashes():
    pass


def test_runs_everywhere():
    pass
```

a testy spustit pomoci:
```shell script
!pytest -v test_mark_platform.py
```

### Preddefinované značky
pytest definuje několik výchozích značek:

- `skip` přeskočí test
- `skipif` přeskočí test, pokud se předaný výraz vyhodnotí jako True.
- `xfail` označuje že test by měl selhar
- `parametrize` vytvoří více variant testu s různými hodnotami jako argumenty.

Seznam dalších výchozích značek je také všech námi definovaný vypisuje příkaz `pytest --markers`.


[//]: <> (Přidat praktickou ukázku na skipif, xfail je dále...)



### Práce s časem
`pytest` rovněž umožňuje měřit dobu trvání testů.
Takto je možné najít nejpomalejší tesy v sadě.
Pomocí volby --durations k příkazu pytest můžete do výsledků testu zahrnout zprávu o trvání.
--durations očekává celočíselnou hodnotu n a ohlásí nejpomalejší počet testů.
```shell script
!pytest -vv --durations=3 test_mark.py
```

#### Modul pytest-timeout
Pytes disponuje řadu modulů, které rošiřují jeho schoplnositi.
Pro práci s časem se velmi hodí modul **pytest-timeout**.

Ten nainstalujeme příkazem:
```shell script
!pip install pytest-timeout
```

A umožňuje nám dvojí použití:

1. Definovat maximální čas, který mohou všechny testy trvat:
```shell script
pytest --timeout=300
```

Tuto hodnotu je možné také nastavit konfigurační ini souboru [pytest.ini](pytest.ini):
```ini
[pytest]
timeout = 300
```

Dále je možné definovat timeout pomocí značky přímo na metodě testu:
```python
@pytest.mark.timeout(60)
def test_foo():
    pass
```


### Nsazení na Gitlab
[//]: <> (Příklad nasazeni ní)

```yaml
image: python:latest

variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
    
cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - python -V
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate
  - pip install pytest

stages:
  - prepare
  - test
  - deploy
   

prepare:
    stage: prepare
    script: 
      - echo Done
    
test_app:
    stage: test
    script: 
      - pytest -v
        
deploy:
    stage: deploy
    only:
    - master
    script:
      - echo Deply

job:on-schedule:
    only: 
    - schedules
    script:
    - pytest -k "http"


```

[//]: <> (Najít knihovnu. která obsahuje špatný test)


## Testování v Jupyter notebook
Pro testování v Jupyter můžeme použít nástavbu `ipytest`
```
!pip install ipytest
```

Import knihovny a automatická konfigurace:
```python
import ipytest
ipytest.autoconfig()
```

Následně můžeme napsat test na funkce v jupyteru:
```python
%%run_pytest

def func(x):
    return x + 1

def test_answer():
    assert func(3) == 5
```

Pokud vše projede vypadá výstup následovně:
```python
%%run_pytest

def test_answer():
    assert func(3) == 4
```

Vykonání buněk v Jupyteru může vytvořit množství objektů, funkcí a proměnných a tím změnit stav celého dokumentu.
Pokud chceme aby po vykonání testů něco zůstalo v paměti (například funkce `test_answer`).

viz:
```python
test_answer
```

Můžeme použít: `%%run_pytest[clean]`, pak se automaticky smaže z paměti vše, co odpovídá pattern `[Tt]est*`
Prakticky to znamená, že se konají pouze testy v aktuální buňce.

```python
%%run_pytest[clean] -qq

def test_answer():
    assert func(3) == 4
```

Další informace a schopnosti knihovny ipytest na jejich [github/ipytest](https://github.com/chmp/ipytest).

## Hlavní výhody frameworku

Co dělá __pytest__ tak užitečným?
Pokud jste již dříve psali jednotkové testy v Pythonu, pak prvním řešením je často modul __Unittest__, který je přímo v Pythonu.
Unittest poskytuje vše co potřebuje pro psaní testů, ale má několik nedostatků.

- Předně pytest vyžaduje napsat mnohem méně kódu.
- Rošiřuje jej množství užitečných [pluginu](http://plugincompat.herokuapp.com/).
- Dovede spouštět i testy jiných frameworků (například unittestu).

Porovnejte následující:
```python
from unittest import TestCase

class TryTesting(TestCase):
    def test_always_passes(self):
        self.assertTrue(True)

    def test_always_fails(self):
        self.assertTrue(False)
```

Při spuštění testů přes unittest dostaneme následující výstup:
```shell script
$ python -m unittest discover
F.
============================================================
FAIL: test_always_fails (test_with_unittest.TryTesting)
------------------------------------------------------------
Traceback (most recent call last):
 File "/.../test_with_unittest.py", line 9, in test_always_fails
    self.assertTrue(False)
AssertionError: False is not True

------------------------------------------------------------
Ran 2 tests in 0.001s

FAILED (failures=1)
```

Jak se dalo očekávat, jeden test prošel a jeden selhal.

Dokázali jste, že unittest funguje, ale podívejte se, co jste museli udělat:
1. Importovat třídu `TestCase` z `unittestu`
2. Vytvořit `TryTesting` trídu jako potomka `TestCase`.
3. Pro každý testy napsat metodu do `TryTesting`
4. K provádění vyhodnocení použít jednu z metod `self.assert.*` z `unittest.TestCase`

Celé to připomíná staticky typované objektové jazyky a do světa Pythonu, který se snaží jít vždy přímo na věc, se to nějak nehodí.
Navíc tento kód musíte psát vždy opakovaně znovu a znovu aniž by Vám přinesl nějaký užite.

Pytest toto vše zjednodušuje:
```python
def test_always_passes():
    assert True

def test_always_fails():
    assert False
```

Není potřeba nic importovat, nic dědit, používate přímo `assert`.
Pytest ale není jeho chování a používá jej pro testy.

Po spuštění testů může výsledek vypadat následovně:
```shell script
$ pytest
================== test session starts =============================
platform darwin -- Python 3.7.3, pytest-5.3.0, py-1.8.0, pluggy-0.13.0
rootdir: /.../effective-python-testing-with-pytest
collected 2 items

test_with_pytest.py .F                                          [100%]

======================== FAILURES ==================================
___________________ test_always_fails ______________________________

    def test_always_fails():
>       assert False
E       assert False

test_with_pytest.py:5: AssertionError
============== 1 failed, 1 passed in 0.07s =========================
```

`pytest` prezentuje výsledky testu jinak než unittest.

Zpráva ukazuje:
1. Stav systému, včetně verzí Pythonu, pytestu a všech pluginů.
2. Kořenový adresář nebo adresář, ve kterém se má hledat konfigurace a testy
3. Počet testů, které běžec objevil

Výstup pak označuje stav každého testu pomocí syntaxe podobné `unittestu`:
1. Tečka (.) znamená, že test proběhl úspěšně.
2. F znamená, že test selhal.
3. E znamená, že test vyvolal neočekávanou výjimku.

U testů, které selžou, poskytuje zpráva podrobný rozpis selhání.
Ve výše uvedeném příkladu test selhal, protože `assert` False vždy selže.
Nakonec zpráva poskytuje celkovou zprávu o stavu testovací sady.

### Jak psát assery:
Posledním krokem test pomocí `assert` aktuální hodnoty oproti očekávané hodnotě.

`assert` můžeme zapsat následovně:
```python
assert sum([1, 2, 3]) == 6, "Should be 6"
```

Přímé řetězcová hodnota není povinná.

Existuje několik obecných osvědčených postupů, jak psát `assert`:

- Ujistěte se, že testy jsou opakovatelné, a spusťte test několikrát, abyste se ujistili, že poskytuje pokaždé stejný výsledek.

- Zkuste a potvrďte výsledky, které se vztahují k vašim vstupním datům, například zkontrolujte, zda je výsledkem skutečný součet hodnot v příkladu sum()

Pro přehled zde uvádím příklady pro porovnání assertů z `unittest` a ekvivalentní `assert`

|Method|Equivalent to|
|--- |--- |
|`.assertEqual(a, b)`|`assert a == b`|
|`.assertTrue(x)`|`assert bool(x) is True`|
|`.assertFalse(x)`|`assert bool(x) is False`|
|`.assertIs(a, b)`|`assert a is b`|
|`.assertIsNone(x)`|`assert x is None`|
|`.assertIn(a, b)`|`assert a in b`|
|`.assertIsInstance(a, b)`|`assert isinstance(a, b)`|


Zde je několik dalších příkladů:
```python
%%run_pytest

def test_uppercase():
    assert "loud noises".upper() == "LOUD NOISES"

def test_reversed():
    assert list(reversed([1, 2, 3, 4])) == [4, 3, 2, 1]

def test_some_primes():
    assert 37 in {
        num
        for num in range(1, 50)
        if num != 1 and not any([num % div == 0 for div in range(2, num)])
    }
```

> Kolik testů proběhlo?
> Není to moc?

Z Výše uvedeného je patrné, že naučení se pracovat s pytesty je mnohem snazší a rychleji, co je hlavním výhoda tohoto frameworku.

[//]: <> (## pytest a výstup na konzili -s prepínaš)

----
Zdroje:
- [pytest.org](https://docs.pytest.org/en/stable/)
- [Nauč se Python: Testování 1](https://naucse.python.cz/lessons/beginners/testing/)
- [Nauč se Python: Testování 2](https://naucse.python.cz/lessons/intro/testing/)
- [Getting Started With Testing in Python](https://realpython.com/python-testing/)
- [Effective Python Testing With Pytest](https://realpython.com/pytest-python-testing/)

