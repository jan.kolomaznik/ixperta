# For při generování seznamů
V pythonu můžeme zapsat `for` i do jako geneátor seznamu.

Syntaxce: `my_list = [promena for promena in generator]`

> #### Příklad:
> Vygenerujte pole čísel od 0 do 999.
```python
list_of_numbers = ???

# Výpis
list_of_numbers[:10]
```

Tanto zápis se považuje za bližší matematickému zápisu a tedy i "elegantnější".

### Kombinace s podmínkou
Při programovaní často stojíte před úkolem vytvořit senzam, který obsahuje jen určité prvky z pole.

Pro ilustraci tu máme klasický procedurární zápis:
```python
my_list = []
for number in list_of_numbers:
    if number % 2 == 0:
        my_list.append(number)

# Výpis
my_list[:10]
```

Python ale umožńuje připojit k for-cyklu i podmínku a tak udělat celou věc jedním výrazem.

Syntaxce: `my_list = [promena for promena in generator if podminka-pro-promenou]`
 
> #### Příklad:
> Přepište předchozí procedurární zápis.
```python
my_list = ???

# Výpis
my_list[:10]
```

### Kombinování více cyklů
V rámci jedné definice můžet zapsat více cyklů, každý pro jednu proměnnou.

Dejme tomu že v procedurárním zápise chcete zapsat vytvořit následující pole:
```python
def times_tables():
    lst = []
    for i in range(10):
        for j in range (10):
            lst.append(i*j)
    return lst
```

Řesení je pak opět jednořádkový zápis pole s generátorem
```python
[j*i for i in range(10) for j in range(10)]
```

> #### Příklad:
> Vaším úkolem je vygenerovat seznam možných id uživatelů.
> Každé id se skládá ze dvou písmen nasledovaný dojicí číslic (např: ac49). 
> Vaším ukolej je tyto uživatele zpracovat a proto potřebujete vygenerovat seznam všech možných id.
```python
lowercase = 'abcdefghijklmnopqrstuvwxyz'
digits = '0123456789'

[???]
```
