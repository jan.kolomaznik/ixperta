# Úvod multithreading vs. multiprocessing

Dovolím si tvrdit, že víc jak polovina dnes prodaných procesorů určených pro notebooky nebo desktop má dvě a více jader. 
Tento trend s jistotou pokračuje dál a věřím že za pár let budou běžné procesory s mnohem větším počtem jader. 

## Rozdíl mezi vlákny a procesy 
Vlákno (Thread) a proces (Process) jsi jsou v mnohém podobné. 
Oba mají identifikátor, množinu registrů které využívají, oba jsou v nějakém stavu plánování, mají nějakou prioritu, mohou měnit obsahy svých proměnných či alokovat nové zdroje atd. 
Avšak je tu několik rozdílů, které mohou hrát velkou roli, při rozhodování, kterou techniku paralelizace programu použít.

Hlavním rozdílem, mezi procesem a vláknem je sdílení paměti. 
Zatímco proces je robustní a samostatný celek, který má všechnu paměť sám pro sebe, vlákno sdílí svoji paměť s dalšími vlákny.

Komunikace mezi vlákny se nazývá synchronizace. 
V Pythonu nám pomohou objekty Event, RLock, Lock nebo Semaphore.

## „GIL“
**GIL** neboli **Global Interpreter Lock** je v podstatě mutex proti vykonávání vláken najednou.

V interpretru CPython (tj. interpret Python'u napsaný v C, navíc ten jediný „hlavní“), paměťová správa není thread-safe (jedno vlákno by mohlo rozbíjet paměť druhého) a proto vlákna musí být vykonávána sekvenčně. 
Tento problém řeší GIL – za cenu ztráty skutečného paralelismu pomocí vláken.

V jiných interpretrech Python'u (např. javovském Jythonu nebo C#-ovském IronPythonu) GIL není a je možno vlákna využít plně. 
V komunitě probíhá diskuse, zda by GIL neměl být z Python'u odstraněn. 
(Nutno ovšem podotknout, že tato diskuse probíhá prakticky po celou dobu existence GILu, takže…)

Více viz http://wiki.python.org/moin/GlobalInterpreterLock.

Modul podprocesů Python používá vlákna místo procesů. 

## Multithreading
Více vláken v rámci jednoho procesu vytvoříme pomocí knihovny *threading*

Každé vlákno provede specifický úkol, bude mít svůj vlastní kód, vlastní paměť zásobníku, ukazatel instrukcí a sdílenou paměť haldy. 

Pokud vlákno má nevracení paměti nebo pracuje s haldu, může poškodit ostatní vlákna a nadřazený proces.

```python
import threading

def calc_square(number):
    global squad
    print('Square: ', number * number)
    squad = number * number
    
def calc_quad(number):
    global quad
    print('Quad: ', number * number * number * number)
    quad = number * number * number * number

number = 7
squad = None
quad = None

thread1 = threading.Thread(target=calc_square, args=(number,))
thread2 = threading.Thread(target=calc_quad, args=(number,))
# Will execute both in parallel
thread1.start()
thread2.start()
# Joins threads back to the parent process, which is this
# program
thread1.join()
thread2.join()
print(squad, quad)
```

## Multiprocesing
Knihovna Multiprocesing vytváří processy operačního systému, přičemž každý má samostatný paměťový prostor.
Pro více jader CPU, obchází omezení GIL v CPythonu.
Podřízené procesy jsou závislé na hlavním prosesu Pythonu zničitelné (např. Volání funkcí v programu) a jejich použití je mnohem snazší. 

Nevýhodou tohoto přístupu jsou větší paměťové nároky a také větší a komplikovanější režiní nároky na zprávu procesů.

```python
import multiprocessing

def calc_square(number):
    global squad
    print('Square: ', number * number)
    squad = number * number
    
def calc_quad(number):
    global quad
    print('Quad: ', number * number * number * number)
    quad = number * number * number * number

number = 7
squad = None
quad = None

p1 = multiprocessing.Process(target=calc_square, args=(number,))
p2 = multiprocessing.Process(target=calc_quad, args=(number,))
p1.start()
p2.start()
p1.join()
p2.join()

# Wont print because processes run using their own memory location                     
print(squad, quad)
```
