# Multiprocessing
Multiprocessing je druhá knihovná v Pythonu, ktera parallení programovaní pomocí procesů.
Poskytuje API podobného multithrading knihovně.

Tato knihna poskytuj možnost lokálního i vzdáleného zpracování processu bez negatnívh dopadů Global Interpreter Lock.
Tato knihovna umožňuje programátovy využít plný výkon počítače.
Běží na Unixu i Windows.

Modul Multiprocessing poskytuje podobné metody a mechanizmy jako modult mutihradingu. 
Zavádí ale také některé nové nástroje, jako je například objekt Pool, který nabízí vhodný prostředek pro paralelizaci provádění funkce na více vstupních hodnot a distribuci vstupních dat mezi procesy (datový paralelismus). 
Následující příklad ukazuje běžné příklad vytužíti tohoto objektu v paci,

Tento základní příklad datového paralelismu pomocí poolu:
```python
from multiprocessing import Pool

def f(x):
    return x*x

with Pool(5) as p:
    print(p.map(f, [1, 2, 3]))
```

## Třída Process
V multiprocesu jsou procesy vytvářeny vytvořením objektu `Process` a následným zavoláním metody `start()`. 
API otoho modulu se tedy vyvhází z API `Thread`. 

Triviální příklad víceprocesového programu je:
```python
from multiprocessing import Process

def f(name):
    print('hello', name)

p = Process(target=f, args=('bob',))
p.start()
p.join()
```

Získání základních informací o procesu demonstrováno na dalším přikladu:
```python
from multiprocessing import Process
import os

def info(title):
    print(title)
    print('module name:', __name__)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())

def f(name):
    info('function f')
    print('hello', name)

info('main line')
p = Process(target=f, args=('bob',))
p.start()
p.join()
```

## Výměna dat mezi procesy
Narozdíl od vláken, procesy nezdílí pamět. 
Existují ale jiné metody jak si mobou procesy myměnovat data:

### Fronty
Jsou prvním prostředkem. jak si mohou procesu snadno vyměnovat data:
Frony jsou navíc processne (i vláknově) bezpečné objekty, tudím je nemusíme synchronizovat (například zámkem).
```python
from multiprocessing import Process, Queue

def f(q):
    q.put([42, None, 'hello'])

q = Queue()
p = Process(target=f, args=(q,))
p.start()
print(q.get())    # prints "[42, None, 'hello']"
p.join()
```

### Pipe
Funkce Pipe() vrací dvojici objektů propojených pipes, která je ve výchozím nastavení duplexní (obousměrná).
```python
from multiprocessing import Process, Pipe

def f(conn):
    conn.send([42, None, 'hello'])
    conn.close()

parent_conn, child_conn = Pipe()
p = Process(target=f, args=(child_conn,))
p.start()
print(parent_conn.recv())   # prints "[42, None, 'hello']"
p.join()
```

Dva objekty propojené objekty `Pipe()` mají (kromě jiného) metody `send()` a `recv()`.
Mějte na paměti, že data v potrubí mohou být poškozena, pokud se dvě vlákna pokusí číst nebo zapisovat na stejný konec kanálu současně. 
Samozřejmě nehrozí žádné poškození způsobené procesy používajícími různé konce potrubí současně.

## Synchronizace mezy procesy
Multiprocesing obsahuje ekvivalenty všech synchronizačních primitiv z vláken. 
Například lze pomocí zámku zajistit, že se na standardní výstup tiskne najednou pouze jeden proces:

```python
from multiprocessing import Process, Lock

def f(l, i):
    l.acquire()
    try:
        print('hello world', i)
    finally:
        l.release()
        
lock = Lock()

for num in range(10):
    Process(target=f, args=(lock, num)).start()
```
Bez použití výstupu zámku z různých procesů může dojít ke smíchání různých výpisů.

## Sdílení stavu mezi procesy
Jak je uvedeno výše, při současném programování je obvykle nejlepší vyhnout se používání sdíleného stavu, pokud je to možné. 
To platí zejména při použití více procesů.

Pokud však opravdu potřebujete použít některá sdílená data, pak multiprocesing poskytuje několik způsobů, jak toho dosáhnout.

### Sdílená paměť
Data lze ukládat do sdílené paměti pomocí tříd `Value` nebo `Array`. 
Například následující kód:
```python
from multiprocessing import Process, Value, Array

def f(n, a):
    n.value = 3.1415927
    for i in range(len(a)):
        a[i] = -a[i]

num = Value('d', 0.0)
arr = Array('i', range(10))

p = Process(target=f, args=(num, arr))
p.start()
p.join()

print(num.value)
print(arr[:])
```

Argumenty `'d'` a `'i'` použité při vytváření `num` a `arr` jakotypové kódy používané v modulu:
* `'d'` označuje float s dvojnásobnou přesností a 
* `'i'` označuje celé číslo se znaménkem. 

Tyto sdílené objekty budou bezpečné z hlediska procesů a vláken.

Pro větší flexibilitu při používání sdílené paměti lze použít modul `multiprocessing.sharedctypes`, který podporuje vytváření libovolných objektů typu ve sdílené paměti.

### Server proces
Objekt správce `Manager()` řídí server proces, a také bsahuje objekty Pythonu.
Umožňuje jiným procesům manipulovat s nimi pomocí proxy serverů.

Manager podporuje typy: `list`, `dict`, `Lock`, `Semaphore`, `Condition`, `Event`, `Barrier`, `Queue`, `Value`, `Array` a další. 
```python
from multiprocessing import Process, Manager

def f(d, l):
    d[1] = '1'
    d['2'] = 2
    d[0.25] = None
    l.reverse()

with Manager() as manager:
    d = manager.dict()
    l = manager.list(range(10))

    p = Process(target=f, args=(d, l))
    p.start()
    p.join()

    print(d)
    print(l)
```

Správci procesů je tedy flexibilnější než přistp sdílené paměti. 
Lze jej vytvořit pro podporu většího množství datových typů.
Správce může také sdílet procesy na různých počítačích v síti. 

Je ale pomalejší než použití sdílené paměti.