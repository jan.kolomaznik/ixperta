# Testování celých aplikaci

[todo]: <> (## Testování CLI aplikací)
[todo]: <> (## Testování Síťových aplikací)

```todo
#pip install pytest pytest
import pytest
import ipytest
ipytest.autoconfig()  In [3]:  from subprocess import Popen
from subprocess import PIPE  In [7]:  p = Popen('ls -lHa'.split(), stdout=PIPE, stderr=PIPE, universal_newlines=True)
output, errors = p.communicate()

print(output) 

```

```todo
def test_ls():
    with Popen('ls -lHa'.split(), stdout=PIPE, stderr=PIPE, universal_newlines=True) as p:
        output, errors = p.communicate()
        print(output)
```


```todo
#pip install pytest ipytest
import pytest
import ipytest
ipytest.autoconfig()  In [3]:  pip install pytest-console-scripts  Out [3]:  Requirement already satisfied: pytest-console-scripts in /opt/conda/lib/python3.8/site-packages (1.1.0)
Requirement already satisfied: mock>=2.0.0 in /opt/conda/lib/python3.8/site-packages (from pytest-console-scripts) (4.0.2)
Requirement already satisfied: pytest>=4.0.0 in /opt/conda/lib/python3.8/site-packages (from pytest-console-scripts) (6.1.2)
Requirement already satisfied: py>=1.8.2 in /opt/conda/lib/python3.8/site-packages (from pytest>=4.0.0->pytest-console-scripts) (1.9.0)
Requirement already satisfied: pluggy<1.0,>=0.12 in /opt/conda/lib/python3.8/site-packages (from pytest>=4.0.0->pytest-console-scripts) (0.13.1)
Requirement already satisfied: toml in /opt/conda/lib/python3.8/site-packages (from pytest>=4.0.0->pytest-console-scripts) (0.10.2)
Requirement already satisfied: attrs>=17.4.0 in /opt/conda/lib/python3.8/site-packages (from pytest>=4.0.0->pytest-console-scripts) (19.3.0)
Requirement already satisfied: iniconfig in /opt/conda/lib/python3.8/site-packages (from pytest>=4.0.0->pytest-console-scripts) (1.1.1)
Requirement already satisfied: packaging in /opt/conda/lib/python3.8/site-packages (from pytest>=4.0.0->pytest-console-scripts) (20.4)
Requirement already satisfied: six in /opt/conda/lib/python3.8/site-packages (from packaging->pytest>=4.0.0->pytest-console-scripts) (1.15.0)
Requirement already satisfied: pyparsing>=2.0.2 in /opt/conda/lib/python3.8/site-packages (from packaging->pytest>=4.0.0->pytest-console-scripts) (2.4.7)
Note: you may need to restart the kernel to use updated packages.
 In [10]:  %%run_pytest[clean] -s

def test_foo_bar(script_runner):
    ret = script_runner.run('ls', '-l')
    #print(ret.stdout)
    #assert ret.success
    #assert ret.stdout == '3.2.1\n'
    #assert ret.stderr == ''  Out [10]:  # Running console script: ls -l
# Script return code: 1
# Script stdout:

# Script stderr:
Traceback (most recent call last):
  File "/opt/conda/lib/python3.8/site-packages/pytest_console_scripts.py", line 196, in exec_script
    compiled = compile(script.read(), str(script), 'exec', flags=0)
  File "/opt/conda/lib/python3.8/codecs.py", line 322, in decode
    (result, consumed) = self._buffer_decode(data, self.errors, final)
UnicodeDecodeError: 'utf-8' codec can't decode byte 0xd0 in position 24: invalid continuation byte

.
1 passed in 0.01s
 In [12]:  !ls -l  Out [12]:  total 300
-rw-rw-r-- 1 jovyan  1000  7921 Dec  2 08:32 00-test.ipynb
-rw-rw-r-- 1 jovyan  1000 44887 Dec  2 11:58 01-test.python.ipynb
-rw-rw-r-- 1 jovyan  1000 54953 Dec  2 13:47 02-test.pytest.ipynb
-rw-rw-r-- 1 jovyan  1000 37038 Dec  2 15:44 03-test.code.ipynb
-rw-rw-r-- 1 jovyan  1000 43655 Dec  3 10:47 04-test.principles.ipynb
-rw-rw-r-- 1 jovyan  1000 14273 Dec  3 15:20 05-test.exceptions.ipynb
-rw-rw-r-- 1 jovyan  1000   632 Dec  3 08:44 conftest.py
-rw-rw-r-- 1 jovyan  1000   138 Dec  2 08:00 docker-compose.yml
-rw-rw-r-- 1 jovyan  1000  1303 Dec  2 08:00 example_doctest.py
-rw-rw-r-- 1 jovyan  1000  1634 Dec  2 08:00 isHoliday.py
drwxrwxr-x 2 jovyan  1000  4096 Dec  2 08:00 media
-rw-rw-r-- 1 jovyan  1000  7084 Dec  3 11:49 network.zip
drwxr-xr-x 2 jovyan users  4096 Dec  3 08:46 __pycache__
-rw-rw-r-- 1 jovyan  1000   109 Dec  2 12:33 pytest.ini
-rw-rw-r-- 1 jovyan  1000  2212 Dec  2 08:00 readme.md
drwxr-xr-x 3 jovyan users  4096 Dec  3 10:45 resources
-rw-rw-r-- 1 jovyan  1000   150 Dec  2 08:00 simple_doctest.py
drwxrwxr-x 2 jovyan  1000  4096 Dec  2 11:58 static
-rw-r--r-- 1 jovyan users  2754 Dec  3 15:54 TestDrivenDevelopment.ipynb
-rw-rw-r-- 1 jovyan  1000   144 Dec  2 08:00 test_mark_classlevel.py
-rw-rw-r-- 1 jovyan  1000   336 Dec  2 13:01 test_mark_platform.py
-rw-rw-r-- 1 jovyan  1000   312 Dec  2 22:13 test_mark.py
-rw-r--r-- 1 jovyan users  3903 Dec  3 19:50 Untitled1.ipynb
-rw-r--r-- 1 jovyan users  1253 Dec  2 08:41 Untitled.ipynb
-rw-r--r-- 1 jovyan users  6307 Dec  3 13:35 Vazby.ipynb
drwxrwxr-x 7 jovyan  1000  4096 Dec  2 08:32 venv
drwxrwxr-x 4 jovyan  1000  4096 Dec  3 12:44 xdudlak
 In [16]:  %%run_pytest[clean] -s

# we reuse a bit of pytest's own testing machinery, this should eventually come
# from a separatedly installable pytest-cli plugin. 
pytest_plugins = ["pytester"]

@pytest.fixture
def run(testdir):
    def do_run(*args):
        return testdir._run(*args)
    return do_run

def test_pyconv_latin1_to_utf8(run):
    result = run("ls", "-l")
    assert result.ret == 0
    with output.open("rb") as f:
        newcontent = f.read()
    assert content.encode("utf8") == newcontent  Out [16]:  F
=================================== FAILURES ===================================
__________________________ test_pyconv_latin1_to_utf8 __________________________

run = <function run.<locals>.do_run at 0x7f53e4fb3b80>

    def test_pyconv_latin1_to_utf8(run):
>       result = run("ls", "-l")

<ipython-input-16-75435677ab4f>:12: 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 

args = ('ls', '-l')

    def do_run(*args):
>       return testdir._run(*args)
E       AttributeError: 'Testdir' object has no attribute '_run'

<ipython-input-16-75435677ab4f>:8: AttributeError
=========================== short test summary info ============================
FAILED tmpynfz2dss.py::test_pyconv_latin1_to_utf8 - AttributeError: 'Testdir'...
1 failed in 0.02s
 In [17]:  Popen(["/usr/bin/git", "commit", "-m", "Fixes a bug."])  In [19]:  import subprocess  In [22]:  subprocess.Popen("ls").start()  In [24]:  subprocess.call(["ls", "-l"])  Out [24]:  0 In [26]:  import subprocess
from subprocess import Popen

p = Popen(["ls","-lha"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

output, errors = p.communicate()

print(output)  Out [26]:  total 360K
drwxrwxr-x 15 jovyan  1000 4.0K Dec  3 20:27 .
drwxr-xr-x  1 root   root  4.0K Jul 20 16:28 ..
-rw-rw-r--  1 jovyan  1000 7.8K Dec  2 08:32 00-test.ipynb
-rw-rw-r--  1 jovyan  1000  44K Dec  2 11:58 01-test.python.ipynb
-rw-rw-r--  1 jovyan  1000  54K Dec  2 13:47 02-test.pytest.ipynb
-rw-rw-r--  1 jovyan  1000  37K Dec  2 15:44 03-test.code.ipynb
-rw-rw-r--  1 jovyan  1000  43K Dec  3 10:47 04-test.principles.ipynb
-rw-rw-r--  1 jovyan  1000  14K Dec  3 20:10 05-test.exceptions.ipynb
-rw-------  1 jovyan users   38 Dec  2 22:14 .bash_history
drwxr-xr-x  5 jovyan users 4.0K Dec  2 11:54 .cache
-rw-rw-r--  1 jovyan  1000  661 Dec  3 20:22 conftest.py
-rw-rw-r--  1 jovyan  1000  138 Dec  2 08:00 docker-compose.yml
-rw-rw-r--  1 jovyan  1000 1.3K Dec  2 08:00 example_doctest.py
drwxrwxr-x  7 jovyan  1000 4.0K Dec  3 20:30 .git
-rw-rw-r--  1 jovyan  1000   77 Dec  2 12:00 .gitignore
-rw-r--r--  1 jovyan users  596 Dec  2 22:13 .gitlab-ci.yml
drwxr-xr-x  2 jovyan users 4.0K Dec  3 19:44 .ipynb_checkpoints
drwxr-xr-x  5 jovyan users 4.0K Dec  2 08:30 .ipython
-rw-rw-r--  1 jovyan  1000 1.6K Dec  2 08:00 isHoliday.py
drwxr-xr-x  3 jovyan users 4.0K Dec  3 08:12 .jupyter
drwxr-xr-x  3 jovyan users 4.0K Dec  2 08:30 .local
drwxrwxr-x  2 jovyan  1000 4.0K Dec  2 08:00 media
-rw-rw-r--  1 jovyan  1000 7.0K Dec  3 11:49 network.zip
drwxr-xr-x  2 jovyan users 4.0K Dec  3 20:22 __pycache__
drwxr-xr-x  3 jovyan users 4.0K Dec  2 11:56 .pytest_cache
-rw-rw-r--  1 jovyan  1000  109 Dec  2 12:33 pytest.ini
-rw-------  1 jovyan users    4 Dec  2 22:14 .python_history
-rw-rw-r--  1 jovyan  1000 2.2K Dec  2 08:00 readme.md
drwxr-xr-x  3 jovyan users 4.0K Dec  3 10:45 resources
-rw-rw-r--  1 jovyan  1000  150 Dec  2 08:00 simple_doctest.py
drwxrwxr-x  2 jovyan  1000 4.0K Dec  2 11:58 static
-rw-r--r--  1 jovyan users 2.7K Dec  3 15:54 TestDrivenDevelopment.ipynb
-rw-rw-r--  1 jovyan  1000  144 Dec  2 08:00 test_mark_classlevel.py
-rw-rw-r--  1 jovyan  1000  336 Dec  2 13:01 test_mark_platform.py
-rw-rw-r--  1 jovyan  1000  312 Dec  2 22:13 test_mark.py
-rw-r--r--  1 jovyan users 9.1K Dec  3 20:27 Untitled1.ipynb
-rw-r--r--  1 jovyan users 1.3K Dec  2 08:41 Untitled.ipynb
-rw-r--r--  1 jovyan users 6.2K Dec  3 13:35 Vazby.ipynb
drwxrwxr-x  7 jovyan  1000 4.0K Dec  2 08:32 venv
drwxrwxr-x  4 jovyan  1000 4.0K Dec  3 12:44 xdudlak

 In [27]:  from subprocess import Popen, PIPE

process = Popen(['ls', '-lHa'], stdout=PIPE, stderr=PIPE)
stdout, stderr = process.communicate()
stdout  Out [27]:  b'total 360\ndrwxrwxr-x 15 jovyan  1000  4096 Dec  3 20:27 .\ndrwxr-xr-x  1 root   root   4096 Jul 20 16:28 ..\n-rw-rw-r--  1 jovyan  1000  7921 Dec  2 08:32 00-test.ipynb\n-rw-rw-r--  1 jovyan  1000 44887 Dec  2 11:58 01-test.python.ipynb\n-rw-rw-r--  1 jovyan  1000 54953 Dec  2 13:47 02-test.pytest.ipynb\n-rw-rw-r--  1 jovyan  1000 37038 Dec  2 15:44 03-test.code.ipynb\n-rw-rw-r--  1 jovyan  1000 43655 Dec  3 10:47 04-test.principles.ipynb\n-rw-rw-r--  1 jovyan  1000 14273 Dec  3 20:10 05-test.exceptions.ipynb\n-rw-------  1 jovyan users    38 Dec  2 22:14 .bash_history\ndrwxr-xr-x  5 jovyan users  4096 Dec  2 11:54 .cache\n-rw-rw-r--  1 jovyan  1000   661 Dec  3 20:22 conftest.py\n-rw-rw-r--  1 jovyan  1000   138 Dec  2 08:00 docker-compose.yml\n-rw-rw-r--  1 jovyan  1000  1303 Dec  2 08:00 example_doctest.py\ndrwxrwxr-x  7 jovyan  1000  4096 Dec  3 20:37 .git\n-rw-rw-r--  1 jovyan  1000    77 Dec  2 12:00 .gitignore\n-rw-r--r--  1 jovyan users   596 Dec  2 22:13 .gitlab-ci.yml\ndrwxr-xr-x  2 jovyan users  4096 Dec  3 19:44 .ipynb_checkpoints\ndrwxr-xr-x  5 jovyan users  4096 Dec  2 08:30 .ipython\n-rw-rw-r--  1 jovyan  1000  1634 Dec  2 08:00 isHoliday.py\ndrwxr-xr-x  3 jovyan users  4096 Dec  3 08:12 .jupyter\ndrwxr-xr-x  3 jovyan users  4096 Dec  2 08:30 .local\ndrwxrwxr-x  2 jovyan  1000  4096 Dec  2 08:00 media\n-rw-rw-r--  1 jovyan  1000  7084 Dec  3 11:49 network.zip\ndrwxr-xr-x  2 jovyan users  4096 Dec  3 20:22 __pycache__\ndrwxr-xr-x  3 jovyan users  4096 Dec  2 11:56 .pytest_cache\n-rw-rw-r--  1 jovyan  1000   109 Dec  2 12:33 pytest.ini\n-rw-------  1 jovyan users     4 Dec  2 22:14 .python_history\n-rw-rw-r--  1 jovyan  1000  2212 Dec  2 08:00 readme.md\ndrwxr-xr-x  3 jovyan users  4096 Dec  3 10:45 resources\n-rw-rw-r--  1 jovyan  1000   150 Dec  2 08:00 simple_doctest.py\ndrwxrwxr-x  2 jovyan  1000  4096 Dec  2 11:58 static\n-rw-r--r--  1 jovyan users  2754 Dec  3 15:54 TestDrivenDevelopment.ipynb\n-rw-rw-r--  1 jovyan  1000   144 Dec  2 08:00 test_mark_classlevel.py\n-rw-rw-r--  1 jovyan  1000   336 Dec  2 13:01 test_mark_platform.py\n-rw-rw-r--  1 jovyan  1000   312 Dec  2 22:13 test_mark.py\n-rw-r--r--  1 jovyan users  9296 Dec  3 20:27 Untitled1.ipynb\n-rw-r--r--  1 jovyan users  1253 Dec  2 08:41 Untitled.ipynb\n-rw-r--r--  1 jovyan users  6307 Dec  3 13:35 Vazby.ipynb\ndrwxrwxr-x  7 jovyan  1000  4096 Dec  2 08:32 venv\ndrwxrwxr-x  4 jovyan  1000  4096 Dec  3 12:44 xdudlak\n'
```