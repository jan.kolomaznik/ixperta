# Negativní testování

Nejprve instalace potřebných závislostí a importy potřebných knihoven:
```python
#pip install pytest ipytest
import pytest
import ipytest
ipytest.autoconfig()
```

[//]: <> (## Testování chybných vstupů)

## Očekávání výjimek
POkud potřebuje v testu otestovat, že funkce/metoda skončí vyhozením výjimky, pak můžeme použít `pytest.raises`:

```python
%%run_pytest[clean]

def test_zero_division():
    with pytest.raises(ZeroDivisionError):
        1 / 0
```

pokud potřebujete pracovat s konkrétní výjimku, ne jenom typem:

```python
%%run_pytest[clean]
def f():
    f()
        
def test_recursion_depth():
    with pytest.raises(RuntimeError) as excinfo:
        f()
        
    assert "maximum recursion" in str(excinfo.value)
```

`excinfo` je instance `ExceptionInfo`, což je wrapper skutečné výjimky. 
Poskytuje atributy jako:
- .typ, 
- .value
- .traceback.

Pytest také umoje snadno testovat message výjimky, to se provádí pomocí parametry `match=`

```python
%%run_pytest[clean]

def myfunc():
    raise ValueError("Exception 123 raised")


def test_match():
    with pytest.raises(ValueError, match=r".* 123 .*"):
        myfunc()
```

## Očekávání že test spadne `@pytest.mark.xfail`
V neposlední řadě můžeme použít značku `xfail`.
Tu použíje v příadě, že víte, že test selže, ale nechce vidět výpis chyby.
Toto je podobné žešení jako skip test.

```python
%%run_pytest[clean]

@pytest.mark.xfail()
def test_f():
    f()
```

značka `xfail` má rovněž několik parametrů rošiřující nebo upravující její chování:

#### `condition=` parameter
Tento parametr umožǚuje test vyřadit z testovací sady na základě podmínky:
```pythons
%%run_pytest[clean]
import sys

@pytest.mark.xfail(sys.platform == "win32", reason="bug in a 3rd party library")
def test_function():
    assert False
```

#### `reason=` parameter
Defakto se jedná o poznámku k testu
```python
%%run_pytest[clean]

@pytest.mark.xfail(reason="known parser issue")
def test_function():
    pass 
```

#### `raises=` parameter
Přijímá typ nebo tuple s typy výjimek, které metoda může vyhodit.
```python
%%run_pytest[clean]

@pytest.mark.xfail(raises=RuntimeError)
def test_function():
    pass
```

#### Příklady použití značky `xfail`
```python
%%run_pytest[clean]

xfail = pytest.mark.xfail


@xfail
def test_hello():
    assert 0


@xfail(run=False)
def test_hello2():
    assert 0


@xfail("hasattr(os, 'sep')")
def test_hello3():
    assert 0


@xfail(reason="bug 110")
def test_hello4():
    assert 0


@xfail('pytest.__version__[0] != "17"')
def test_hello5():
    assert 0


def test_hello6():
    pytest.xfail("reason")


@xfail(raises=IndexError)
def test_hello7():
    x = []
    x[1] = 1
```

#### Použití značky `xfail` a pamaterizovatelného tesu
Značku lze použít i v případě že se jedná o parametrizovaný test.

```python
%%run_pytest[clean]

@pytest.mark.parametrize(
    ("n", "expected"),
    [
        (1, 2),
        pytest.param(1, 0, marks=pytest.mark.xfail),
        pytest.param(1, 3, marks=pytest.mark.xfail(reason="some bug")),
        (2, 3),
        (3, 4),
        (4, 5),
        pytest.param(10, 11, marks=pytest.mark.skipif(sys.version_info >= (3, 0), reason="py2k")),
    ],
)
def test_increment(n, expected):
    assert n + 1 == expected
```

[//]: <> (zotavení se po neporařeném testu.)

----
Zdroje:
- [Assertions about expected exceptions](https://docs.pytest.org/en/stable/assert.html#assertions-about-expected-exceptions)
- [XFail: mark test functions as expected to fail](https://docs.pytest.org/en/latest/skipping.html#xfail-mark-test-functions-as-expected-to-fail)
