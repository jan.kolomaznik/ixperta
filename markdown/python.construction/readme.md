# Základní konstrukce jazyka
Bloky kódu se v Pythonu neuzavírají do závorek, ale definují se pomocí odsazení.
- Zvyšuje to přehlédnout kódu
- **Musí se dodržovat**
- Bíle znaky jsou mimo použití v odsazení ignorovány
- Používá se **tabulátor** nebo mezery ([PEP8](https://www.python.org/dev/peps/pep-0008/) – Style Guide for Python Code*
)
  - *NELZE* kombinovat* 

### Řízení toku programu
**`if`**: větvení toku podle podmínky

**`for`**: cyklus přes *něco*

**`while`** cyklus s podmínkou

## Podmínka (`if`)
Vícenásobné větvení kódu

```python
promenna = 5

if promenna > 10:
    print("velká")
elif promenna < 10:
    print("malá")
else:
    print("akorát")
```

![](media/bloky.png)

> #### Příklad:
> Načtěte od uživatele vstup (číslo) a vypište, zda je číslo kladné, záporné.
```python
# Řešení
```

> #### Příklad:
> Vytvořte jednoduchou kalkulačku. 
>
> Uživatel zadá dvě čísla a poté mu bude prezentována nabídka operací 
>
> Po vybrání bude operace provedena a výstup vypsán.
> Operace jsou `+`, `-`, `*`, `/`
```python
# Řešení
```

## Smyčka (`for`)
= Iteruje prakticky přes cokoliv.

Iterace přes seznam (list)
```python
seznam  =  ["pes", "kočka", "myš"]
for zvire in seznam:
    print("{} je savec".format(zvire))
```

> #### Příklad:
> Načtěte od uživatele vstup slov oddělených mezerou.
> Metodou split jeje rozdělte na pole slov a vytvořte nové pole čísel s hodnotami délek každého slova.
```python
slova = input("Vlož slova oddělená mezerami (větu):").split()
# Řešení
```

Iterace přes číselný rozsah = přes seznam čísel
```python
rozsah = range(4)
for i in rozsah:
    print(i)
```

Iterace přes slovník
```python
slovnik = {"jedna": 1, "dva": 2, "tři": 3}
for klic in slovnik:
    print(klic)
```

Iterace slovníku přes klíče a hodnoty:
```python
slovnik = {"jedna": 1, "dva": 2, "tři": 3}
for klic, hodnota in slovnik.items():
    print(klic, hodnota)
```

### Funkce `range()`
Umí generovat i komplikovanější číselné řady
```python
cisla = range(0, 101, 10)
for x in cisla:
    print("Číslo je:",x)
```

> #### Příklad:
> Opět načťete slova od uživatele a rozdělte je na seznam.
>
> Do konzole vypište každé druhé slovo.
```python
# Řešení
```

> Upravte tak aby jste vypsali to stejné ale od konce
```python
# Řešení
```


### řetězce
řetězec je pole znaků, takže je možné jej také procházet pomocí `for`
```python
for pismeno in 'Python':
    print(pismeno.upper())
```

> #### Příklad:
> Do listu uložte slova různé délky. 
>
> Projděte list cyklem a vypište jen slova delší než 4 znaky.
```python
# Řešení
```

### Cyklus přes seznam s indexem
Máte seznam a chcete jej projít s indexem, něco jako ...
```python
i = 0
for barva in ["červená", "zelená", "modrá"]:
    print("pořadí: {}, barva: {}".format(i, barva))
    i += 1
```

Tak tak takhle **NE**

Můžeme použít funkcí `enumerate()` a rozbalení N-tice
```python
enumerate(["červená", "zelená", "modrá"])
```

Takže správnější řešení je: 
```python
for i, barva in enumerate(["červená", "zelená", "modrá"]):
    print("pořadí: {}, barva: {}".format(i, barva))
```

> #### Příklad:
> Vypište čísla od 1 do 100, přičemž:
> 
> - čísla dělitelná 3 nahraďte slovem "Fizz"
> - čísla dělitelná 5 nahraďte slovem "Buzz"
> - čísla dělitelná 3 a zároveň 5 slovem "FizzBuzz"
```python
# Řešení
```

### Procházení vícerozměrných listů
```python
matice = [ [1,0,0], [0,1,0], [0,0,1] ]

for i in range(0,len(matice)):
    print(matice[i])
```

> #### Příklad:
> Načťete soubor *building1retail.csv* a zjistěte jestli existuje závislost mezi teplotou a energií budovy.
> 
> Naším ukolem zjistit v kterých dnech byla teplota a spotřeba na horních 25% a kolik dní je společných.
```python
import csv
with open("static/building1retail.csv") as f:
    data = list(csv.reader(f))
len(data)
```
 
1
## Smyčka (`while`)
Opakuje se, dokud je podmínka splněna.
```python
x  =  0
while  x  <  4:
    print(x)
    x  +=  1 # pozor, žádné x++ neexistuje
```

Smyčku lze přerušit v libovolném místě příkazem `break`
```python
i=0
while True:
    if i > 10:
        break
    print(i)
    i=i+1
```
*Obdobně existuje i příkaz `continue`.

> #### Příklad:
> Upravte kalkulačku tak, aby se po vykonání výpočtu ptala, jestli chce uživatel skončit, nebo pokračovat.
```python
# Řešení
```

> #### Příklad:
> Hádání čísla trezoru.
```python
from random import randrange
secret_number = randrange(0, 10)
# Řešení
```

### Specialitka: přerušení vnějšího cyklu
```python
for x in range(10):
    for y in range(10):
        print(x,y,(x*y))
        if x*y > 50:
            break
    else:
        continue # co dělat když cyklus NE přerušen 
    break # jinak (tedy když vnitřní cyklus skonči)
```

