# Testování aplikací přes API

Nejprve intalace potřebných závislotí a importy potřebných knihoven:

```python
#pip install pytest ipytest
import pytest
import ipytest
ipytest.autoconfig()
```

[todo]: <> (## Mnohdy v jiném jazyce)
## Komunikace a vzdálené volání
Vaše programy často používají webová API. Při testování funkcionality API klientů se vynoří řada problémů:

výsledky volání API mohou být pokaždé různé,
* k některým volání API je potřeba mít přístupové údaje,
* API může být zrovna nedostupné.
* V zásadě můžete omockovat knihovnu requests tak, aby jednotlivá volání jako get() apod. vracela předem definovanou odpověď. Při ponoření do hloubky ale zjistíte, že komplexita takového mockování může velmi přesáhnout komplexitu samotného kódu, který testujete. Jednodušší je tak použít již hotové řešení. Jedno z nich je betamax.

Betamax umožňuje nahrát HTTP komunikaci do kazet (souborů), které se potom použijí při testech. V zásadě to funguje takto:

* Pokud daný HTTP požadavek ještě neproběhl, provede se a nahraje na kazetu.
* Pokud již proběhl, použije se daná kazeta pro simulaci.
Betamax funguje pouze s knihovnou requests při použití session.

V kombinaci s pytestem můžete použít předpřipravenou fixture:

```python
import betamax

with betamax.Betamax.configure() as config:
    # tell Betamax where to find the cassettes
    # make sure to create the directory
    config.cassette_library_dir = 'tests/fixtures/cassettes'

def test_get(betamax_session):
    betamax_session.get('https://httpbin.org/get')
```

Před spuštěním testu vytvořte složku tests/fixtures/cassettes. Po spuštění testu ji prozkoumejte. Měla by obsahovat soubor test_filename.test_get.json. To je nahraná kazeta. Každý další průběh testu nevykoná GET požadavek, ale pouze přehraje danou kazetu. Pokud chcete kazetu opět nahrát, prostě ji smažte a pusťte test znovu.

Celé to ale funguje pouze, pokud kód vykonávaný uvnitř testu použije speciální session, kterou máme od betamaxu. Jak to udělat?

Je třeba, aby implementační část kódu uměla session přejmout, například takto:

```python
class Client:
    def __init__(self, session=None):
        self.session = session or requests.Session()
        ...

def test_clent_foo(betamax_session):
    client = Client(session=betamax_session)
    assert client.foo() == 42
```

Pokud budete používat parametrizované testy, použijte betamax_parametrized_session, aby kazety měly odlišné jméno při odlišných parametrech.



[todo]: <> (## Testovací instance software)
## Práce s neznámým stavem
Při práci s webovými API často létají vzduchem citlivé údaje jako tokeny apod.

Vyvstávají dvě otázky:

1. Jak umožnit spuštění testů bez vlastního tokenu?
2. Jak citlivé údaje skrýt v kazetách a nedávat je do do gitu?
Na obě otázky se pokusím odpovědět jedním okomentovaným kódem:

```python
with betamax.Betamax.configure() as config:
    if 'AUTH_FILE' in os.environ:
        # If the tests are invoked with an AUTH_FILE environ variable
        TOKEN = my_auth_parsing_func(os.environ['AUTH_FILE'])
        # Always re-record the cassetes
        # https://betamax.readthedocs.io/en/latest/record_modes.html
        config.default_cassette_options['record_mode'] = 'all'
    else:
        TOKEN = 'false_token'
        # Do not attempt to record sessions with bad fake token
        config.default_cassette_options['record_mode'] = 'none'

    # Hide the token in the cassettes
    config.define_cassette_placeholder('<TOKEN>', TOKEN)
    ...

@pytest.fixture
def client(betamax_session):
    return Client(token=TOKEN, session=betamax_session)
```

Co když ale nevíme, jak bude vypadat citlivá část požadavku, protože se teprve někde spočítá a získá, jako například v případě Twitter API? Na tuto otázku podrobněji odpovídá dokumentace.

V každém případě je moudré před uložením do gitu zkontrolovat, že se v kazetách nenachází žádný citlivý údaj, a pokud tam je, přepsat kód tak, aby se tam nenacházel.

### Komprimované citlivé údaje
Problém může nastat, pokud je token či jiná citlivá informace uložena jako část v těle odpovědi (případně i požadavku) a zároveň je toto tělo zprávy zkomprimováno (defaultní chování, viz dokumentace). V takovém případě je potřeba k tomu, aby šlo v kazetě nahradit citlivé údaje, upravit hlavičku `Accept-Encoding` v `betamax_session` tak, aby neobsahovala `*`, `gzip`, `compress` ani `deflate`:

```python
betamax_session.headers.update({'Accept-Encoding': 'identity'})
```

### Které HTTP požadavky jsou stejné?
Podle čeho se vyhodnotí, že HTTP požadavek odpovídá nahrané interakci a má se pouze přehrát? Ve výchozím stavu podle HTTP metody a URL. Pokud tedy na jedno URL provedete dva POST požadavky s jiným tělem, betamax je bude považovat za stejné. Toto chování lze měnit zapnutím (nebo vypnutím) různých matcherů. Těch je v betamaxu celá řada a je jednoduché napsat si vlastní. Více informací najdete v dokumentaci.

[Youtube tutorial](https://www.youtube.com/watch?v=iFqF5IaWfy0)


## Restování selhání http komunikace
Použijem knihovnu `responses`:

Pokud se pokusíte načíst adresu URL, která zasáhne shodu, reakce zvýší ConnectionError:
```python
import responses
import requests

from requests.exceptions import ConnectionError

@responses.activate
def test_simple():
    with pytest.raises(ConnectionError):
        requests.get('http://twitter.com/api/1/foobar')
```

Nakonec můžete předat výjimku jako tělo ke spuštění chyby na žádost:
```python
import responses
import requests

@responses.activate
def test_simple():
    responses.add(responses.GET, 'http://twitter.com/api/1/foobar', body=Exception('...'))
    with pytest.raises(Exception):
        requests.get('http://twitter.com/api/1/foobar')
```


----
##### Zdroje:
- [Nauč se Python: Testování 2](https://naucse.python.cz/lessons/intro/testing/)