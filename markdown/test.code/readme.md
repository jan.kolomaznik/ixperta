# Testování kódu

V následující kapitole se ukážeme základy psaní testů pomocí knihovny __pytest__.

Import potřebných závislostí:
```python
#pip install pytest pytest
import pytest
import ipytest
ipytest.autoconfig()
```

```python
def add_DPH(prise):
    """
    Spočítá cenu s dph 21%.
    """
    pass
```

```python
%%run_pytest[clean]

def test_add_DPH():
    assert add_DPH(100) == 121
```


## Jednotkové testy
Při psaní jednotkových testů se doporučuje řídit se přístupem: __Arrange-Act-Assert__:

1. __Arrange__: Příprava testu, nastavení situace.
2. __Act__: volání testované funkce nebo metody
3. __Assert__: otestovat, výsledek funkce, změnu stavu.

Platí ze _zlaté pravidlo: 80:20_.
Tedy vždy začněte napsáním toho nejjednoduššího testu jednoduchého použití a průběhu.
Napsat takový test zabere nejmíň času (20%) a odhalí Vám nejvíc chyb (80%).

Protože testy musí být snadno udržovatelné a čitelné, doporučuji držet v nich jednotnou jasně definovanou strukturu:

```python
%%run_pytest[clean]

# Základní test má vždy nejjednodušší jméno
def test_isHoliday():
    """Basic test module isHoliday"""
    # setup
    from isHoliday import getholidays
    # when
    holidays = getholidays(2016)
    # then
    assert (24, 12) in holidays
```

Tento koncept se ale spíše hodí pro testování metod objektů v OOP.
Pokud se držíme spíše funkcionálního programování, pak by testy mohly vypadat následovně:
```python
%%run_pytest[clean]

def test_uppercase():
    assert "loud noises".upper() == "LOUD NOISES"

def test_reversed():
    assert list(reversed([1, 2, 3, 4])) == [4, 3, 2, 1]

def test_some_primes():
    assert 37 in {
        num
        for num in range(1, 50)
        if num != 1 and not any([num % div == 0 for div in range(2, num)])
    }
```

## Závislosti: `Fixture`
V případě testování OOP Kódu je důležité zpracovat závislosti.
V případě jednotkových testů __musíme__ odstraní všechny závislosti na ostatní třídy.
Pokud toto neučíme provádí jednotkové testy ale __integrační __!

Unittest, stejně jako většina ostatních testovacích knihoven, umožňuje definovat tyto závisloti _inmplicitne_ pomocí metod `setUp()` a `tearDown()`, které se aplikují před a po každým testu.
Takto můžeme zcela implicitně učinit závislost testu na konkrétní části dat nebo objektu.
V průběhu času mohou implicitní závislosti vést ke zbytečné složitosti kódu testů.
Testy by vám měly být co nejjednodušší.
Pokud je samotným testům obtížné porozumět, pak __nejsou__ užitečné.

_Pytest_ má jiný přístup, umožňuje nám deklarovat závisloti __explicitně__.
K tomuto slouží v pytest `fixtures funkce`, které vytvářejí data nebo inicializují nějaký stav systému pro testovací sadu.
Jakýkoliv test, který chce použít `fixture`, pak jej testovací funkce musí výslovně přijmout jako argument, takže závislosti jsou vždy uvedeny předem.

`Fixtures` je možné kombinovat s jinými `fixtures` tak že je deklarují jako závislosti pomocí argumentů funkce.

```python
%%run_pytest[clean]

@pytest.fixture
def smtp_connection():
    import smtplib

    return smtplib.SMTP("smtp.gmail.com", 587, timeout=5)


def test_ehlo(smtp_connection):
    response, msg = smtp_connection.ehlo()
    assert response == 250
    assert 0  # for demo purposes
```

### Kdy vytvářet Fixture
Představte si funkci `format_data_for_display()` pro zpracování dat vrácených nějakým API.
Data představují seznam lidí, z nichž každý má křestní jméno, příjmení a pracovní pozici.
Funkce by měla vypsat seznam řetězců, které obsahují celé jméno každého člověka (jeho křestní jméno následované jeho příjmením), dvojtečku a název pracovní pozice.
Chcete-li to otestovat, můžete napsat následující kód:
```python
%%run_pytest[clean]

def format_data_for_display(people):
    ...  # Implement this!

def test_format_data_for_display():
    people = [
        {
            "given_name": "Alfonsa",
            "family_name": "Ruiz",
            "title": "Senior Software Engineer",
        },
        {
            "given_name": "Sayid",
            "family_name": "Khan",
            "title": "Project Manager",
        },
    ]

    assert format_data_for_display(people) == [
        "Alfonsa Ruiz: Senior Software Engineer",
        "Sayid Khan: Project Manager",
    ]
```

Dále potřebujete také napsat funkci pro transformaci dat do formátu CSV.
Test by vypadal strašně podobně:
```python
%%run_pytest[clean]

def format_data_for_excel(people):
    ... # Implement this!

def test_format_data_for_excel():
    people = [
        {
            "given_name": "Alfonsa",
            "family_name": "Ruiz",
            "title": "Senior Software Engineer",
        },
        {
            "given_name": "Sayid",
            "family_name": "Khan",
            "title": "Project Manager",
        },
    ]

    assert format_data_for_excel(people) == """given,family,title
Alfonsa,Ruiz,Senior Software Engineer
Sayid,Khan,Project Manager
"""
```

Jak je vidět. můžeme použít stejná data pro více testů.
Tuto část kódu tedy můžeme použít do funkce označené dekorátorm `@pytest.fixture`:
```python
@pytest.fixture
def example_people_data():
    return [
        {
            "given_name": "Alfonsa",
            "family_name": "Ruiz",
            "title": "Senior Software Engineer",
        },
        {
            "given_name": "Sayid",
            "family_name": "Khan",
            "title": "Project Manager",
        },
    ]
```

`Fixture` pak můžete použít jako argument testů.
Jeho hodnotou bude návratová hodnota funkce zařízení:

```python
%%run_pytest[clean]

def test_format_data_for_display(example_people_data):
    assert format_data_for_display(example_people_data) == [
        "Alfonsa Ruiz: Senior Software Engineer",
        "Sayid Khan: Project Manager",
    ]

def test_format_data_for_excel(example_people_data):
    assert format_data_for_excel(example_people_data) == """given,family,title
Alfonsa,Ruiz,Senior Software Engineer
Sayid,Khan,Project Manager
"""
```

Každý test je nyní výrazně kratší, ale stále má jasnou cestu zpět k datům, na kterých závisí.
Nezapomeňte `fixture` výstižné pojmenovat, aby se i v budoucnu daly snadno použít při psaní nových testů.

### Určení pořadí vytváření `fixture`
Definice pořadí inicializace o uklidu `fixture`:
```python
%%run_pytest[clean]


@pytest.fixture
def first():
    print("Set up first fixture")
    yield
    print("Clean up first fixture")


@pytest.fixture
def second(first):
    print("Set up second fixture")
    yield
    print("Clean up second fixture")


def test_context_fixture_order(second):
    print("In the test")
    assert False
```

### Kdy se vyhnout `fixture`
Pokud potřebujete pro každý test trochu jiná data, pak `fixture` není dobrým řešením.
Hledá změn a po modifikaci často značně znesnadňuje pochopení kódu.
Stejným problém je i přidání mezivrstvy s abstrakcí.
V takových případech bývá snadnější pochopit situaci přímo z plain dat.

### Škálování `fixture`
Při vytváření `fixture` pro své testy brzy zjistíte, že pro definici nových `fixtures` můžete použít již dříve definované.
`Fixture` jsou modulární a mohou se pro svou definice mohou použít jinou `fixture`.
C okdyž ale zjistíte že potřebujete použít jednu `fixture` v různých modulech?

V takovém případě je doporučeným řešením vyextrahovat tyto obecně použitelné `fixture` do samostatného modulu, který pak jednoduše naimportujte všude, kde ji potřebujete.

__pytest__ prohledávání adresáře modulů a hledá souboru _conftest.py_.
Ve vašich testech pak můžete použít jakýkoliv `fixture` který je nadefinovaný v adresáři s testem a i nadřazených adresářích.
Proto je vhodné často používané `fixture` umístit sem.

#### Příklad:
Podívejte se do souboru [conftest.py](conftest.py).
A na následující test:

```python
%%run_pytest[clean]

def test_format_data_for_display(example_people_data_from_conftest):
    assert len(example_people_data_from_conftest) == 2
```

Pokud bychom chtěli viditelnosti `fixture` blíže specifikovat, můžeme použít parameter `scope=`.
Dostupné skopy jsou:
* `function`: výchozí rozsah, `fixture` je zničeno na konci testu.
* `class`: `fixture` je zničeno po provedení všech trestů ve třídě.
* `module`: `fixture` je zničeno po provedení posledního testu v modulu.
* `package`, `fixture` je zničeno po provedení všech testů b balíčku.
* `session`: `fixture` je použito pro všechny testy.

#### Příklad:
Vytvořte si `fixture` která vrátí náhodné číslo v intervalu od 1 do 100.
```python
from random import randrange

@pytest.fixture()
def random_value():
    return randrange(1,100)
```

Protože mý vychozí hodnotu `scope='function'`, pak v každém testu vrací jinou hodnotu:
```python
%%run_pytest[clean]

def test_ehlo(random_value):
    assert 0 == random_value


def test_noop(random_value):
    assert 0 == random_value
```

Pře Definujme si teď `fixture` tak a by měla `scope='session'`.
```python
@pytest.fixture(scope="session")
def random_value():
    return randrange(1,100)
```

Pokud provedeme testy znovu, pak náhodná hodnota buce u obou testů stejná:
```python
%%run_pytest[clean]

def test_ehlo(random_value):
    assert 0 == random_value


def test_noop(random_value):
    assert 0 == random_value
```

## Parametrizované testy
Častým případem při psaní testů je, že chceme otestovat funkcionalitu, ale na jiných datech.
Například funkci `is_palindrome`:
```python
def is_palindrome(s):
    return s == s[::-1]
```

A tuto funkci chceme otestovat:
```python
%%run_pytest[clean]

def test_is_palindrome_empty_string():
    assert is_palindrome("")

def test_is_palindrome_single_character():
    assert is_palindrome("a")

def test_is_palindrome_mixed_casing():
    assert is_palindrome("Bob")

def test_is_palindrome_with_spaces():
    assert is_palindrome("Never odd or even")

def test_is_palindrome_with_punctuation():
    assert is_palindrome("Do geese see God?")

def test_is_palindrome_not_palindrome():
    assert not is_palindrome("abc")

def test_is_palindrome_not_quite():
    assert not is_palindrome("abab")
```

Jedná se o velmi podobné testy líčící se je uhodnout a jménem
```python
def test_is_palindrome_<in some situation>():
    assert is_palindrome("<some string>")
```

Můžete použít `@pytest.mark.parametrize()` k vyplnění tohoto tvaru různými hodnotami, čímž výrazně snížíte svůj testovací kód:
```python
%%run_pytest[clean]

@pytest.mark.parametrize("palindrome", [
    "",
    "a",
    "Bob",
    "Never odd or even",
    "Do geese see God?",
])
def test_is_palindrome(palindrome):
    assert is_palindrome(palindrome)

@pytest.mark.parametrize("non_palindrome", [
    "abc",
    "abab",
])
def test_is_palindrome_not_palindrome(non_palindrome):
    assert not is_palindrome(non_palindrome)
```

Zde je ještě jeden příklad:
```python
%%run_pytest[clean]
from isHoliday import getholidays

@pytest.mark.parametrize('year', (2015, 2016, 2017, 2033, 2048))
def test_xmas(year):
    """Test whether there is Christmas"""
    holidays = getholidays(year)
    assert (24, 12) in holidays
```

První argument `parametrize()` slouží k pojmenování parametrů a druhým je `list` hodnot nebo `tuple`, které představují hodnoty parametrů testovací funkce.
Takto můžete všechny metody sloučit do jedné.

```python
%%run_pytest[clean]

@pytest.mark.parametrize("maybe_palindrome, expected_result", [
    ("", True),
    ("a", True),
    ("Bob", True),
    ("Never odd or even", True),
    ("Do geese see God?", True),
    ("abc", False),
    ("abab", False),
])
def test_is_palindrome(maybe_palindrome, expected_result):
    assert is_palindrome(maybe_palindrome) == expected_result
```

Nevýhoda více téměř stejných testů je patrná sama o sobě, nevýhoda cyklu je v tom, že celý test selže, i pokud selže jen jeden průběh cyklem.
Zároveň se průběh testu při selhání ukončí.

A ještě jeden příklad na svátky, tentokrát s více parametry:
```python
%%run_pytest[clean]

@pytest.mark.parametrize(
    ['year', 'month', 'day'],
    [(2015, 12, 24),
     (2016, 12, 24),
     (2017, 1, 1),
     (2033, 7, 5),
     (2048, 7, 6)],
)
def test_some_holidays(year, month, day):
    """Test a few sample holidays"""
    holidays = isholiday.getholidays(year)
    assert (day, month) in holidays
```

Vždy je dobré pokusit se nějaký test rozbít v samotném kódu, který testujeme, abychom se ujistili, že testujeme správně.
Přidáme tedy dočasně na konec funkce `getholidays()` tento pesimistický kus kódu:

    if year > 2020:
        # After the Zygon war, the puppet government canceled all holidays
        holidays = set()


## Testování modulů, funkcí a tříd
Nyni si zrekapitulujme jak:

### 1. Testovat moduly

```python
# Příklady
```

### 2. Testovat funkce

```python
# Příklady
```

### 3. Testovat třídy

```python
# Příklady
```

----
###### Zdroje:
- [isHoliday.py](https://gist.github.com/oskar456/e91ef3ff77476b0dbc4ac19875d0555e)
- [Getting Started With Testing in Python](https://realpython.com/python-testing/)
- [Effective Python Testing With Pytest](https://realpython.com/pytest-python-testing/)
- [Nauč se Python: Testování 2](https://naucse.python.cz/lessons/intro/testing/)

