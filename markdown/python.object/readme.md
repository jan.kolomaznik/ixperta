# Třídy a objekty
Svět vnímáme jako množinu objektů, které mezi sebou vzájemně interagují. 

V OOP se snažíme tyto objekty popsat
- vlastnosti objektů
- operace / metody, které může objekt provádět
- vztahy mezi nimi

![](media/oop.png)

K vnitřnostem objektu můžeme přistupovat pomocí tečky. Často představuje věci z reálného světa.
```python
objekt = "Iron Maiden"
objekt.split()
```

Objekky se organizují do hirearchické struktuty. Jejím kořenem je `Object`

## Základní pojmy OOP
### Třída
Obecný předpis pro vytvoření objektu. Obsahuje:

Atributy = proměnné zabalené v objektu.
Metody = funkce zabalené v objektu.

```python
class Pes(object):
    def stekni(self):
        print(self.jmeno + ": haf")
```

### Instance (objekt)
Vytvořený objekt, ten jeden konkrétní.

![](media/oop_trieda_vs_objekt.png)

Vytvoření instance třídy Pes
```python
alik = Pes()
alik.jmeno = "Alík" 
alik.stekni()
```

jiná instance téže třídy
```pytho
mazlik = Pes()
mazlik.jmeno = "Mazlík"
```

navzájem se neovlivňují
```python
print(alik.jmeno)
print(mazlik.jmeno)
```

avšak mají společné chování (definované v třídě Pes)
```python
alik.stekni()
mazlik.stekni()
```

## OOP v Pythonu
Python má oprotim jiným programovacim jazykům určitá specifika.

### Metody a `self`
První argument u **všech** metod. 

Automaticky ukazuje na aktuální instanci.

Test metoda bez argumentů vypadá následovnš:
```python
class Trida:
    # Metoda
    def metoda(self):
        pass
```

### Konstruktor `__init__`
Metoda, která se volá hned při vytvoření instance
```python
class Pes(object):
    def __init__(self, jmeno):
        self.jmeno = jmeno
    def stekni(self):
        print(self.jmeno + ": haf")
        
alik = Pes("Alik")
alik.stekni()
```

> #### Příklad
> Vytvořte třídu Kniha.
>
> Ta bude mít následující atributy:
> - `nazev`
> - `jmeno_autora`
> - `text`
>
> Ke knize vytvořte konstrukto, který bude vyžadovat `nazev` a `jmeno_autora`, ale tředí parametr `text` text bude volitelný.
```python
# Přiklad: trida
```
>
> Vytvořte třídu kniha s vaší oblíbenou knihou.
```python
# Přiklad: pouziti tridy
```

### Atributy (proměnná patřící objektu)
Atribut uchovává data patřící objektu.
Každý vytvořený objekt má atributy s jeho vlasními hodnotami.
V pythonu se mohou vytvořit kdykoliv.
```python
alik.rasa = "Jezevčík"
print(alik.rasa)
```

Lepší variantou je definovat je v konstruktoru
```python
class Pes(object):
    def __init__(self, jmeno):
        self.jmeno = jmeno
        
alik = Pes("Alik")
print(alik.jmeno)
```

A dají se kdykoliv měnit
```python
alik = Pes("Alik")
alik.jmeno = "Jezevčík"
print(alik.jmeno)
```

> #### Příklad:
> Vytvořte třídu Knihovna, která bude evidovat Knihy. 
> Knihovna bude mít metody na přidávání knihy a vypisání knih.
```python
# Řešení
```

### Privátní atributy
Pro lepší přehlednost je jepší metody a atributy.
Ty není vidět mimo Třídu. V pythonu je označují jednim nebo dvěma `_` před jménem.
- Pokud použijete jeden `_`: pak defakto není atribut privatní v pravém slova smyslu, ale všichni tomu budou rozmět a budu se tak k němu chovat.
- Pokud použije dva `__`: pak Pyhton vyhodí chybu, když se k atributu bude snažit někdo přistoupit.

Dvě `__`
```python
class Trida:
    def __init__(self):
        self.__soukromy_atribut = 123
    def print_soukromy_atribut(self):
        print(self.__soukromy_atribut)

a = Trida()
a.print_soukromy_atribut()
print(a.__soukromy_atribut) # chyba
```

Jedno `_`
```python
class Trida:
    def __init__(self):
        self._jako_soukromy = 123

a = Trida()
print(a._jako_soukromy)
```

Symbolem `_` můžeme označit i metody
```python
class  Smartphone (object):
    def __init__(self, oznaceni):
        casti = oznaceni.split()
        # privátní atributy
        self._vyrobce = casti[0]
        self._model = casti[1:]
        
    # privátní metoda
    def _update_systemu(self):
        print("updatuji system")
    def vypis_model(self):
        print(" ".join(self._model))
        
mobil = Smartphone("Huňavej BŽ 10000")
    
# NE - tohle nikdy
print(mobil._model)

# správně
mobil.vypis_model()
```

> #### Příklad:
> Upravte třídy Kniha a Knihovna tak aby jejich atributy byli chránenné proti nevhodnému změnění. 
> 
> Vytvořte metody, které umožní čtení nově chráněných atributů.
```python
# Řešení: Kniha
```

```python
# Řešení: Knihovna
```

![](media/harry-python.png)

## Dědičnost
Vyjadřuje hierarchii tříd. Jedná se o specializaci objektů.

```python
class Rodic:
    def __init__(self):
        self.x = 10
    def metodaRodice(self):
        print ('Ahoj z metody rodice')

class Potomek(Rodic):
    def __init__(self):
        self.y = 20
    def metodaPotomka(self):
        print('Ahoj z metody potomka')


a = Potomek()
a.metodaRodice()
a.metodaPotomka()
```

Atribut předka
```python
print(a.x)
```

Volání konstruktoru rodice
```python
class Rodic:
    def __init__(self):
        self.x = 10

class Potomek(Rodic):
    def __init__(self):
        super().__init__()
        self.y = 20

a = Potomek()
print(a.x)
```

### Komplexnější příklad
```python
class DopravniProstredek(object):
    def __init__(self, rychlost):
        self.rychlost = rychlost
    def cestuj(self, odkud, kam):
        print("cestuji rychlostí", self.rychlost, "km/h z", odkud, "do", kam)
```

```python
neco = DopravniProstredek(100000)
neco.cestuj("obýváku", "nekonečno a ještě dál")
```

```python
class Auto(DopravniProstredek):
    def nastartuj(self):
        print("startuji motor")
```

```python
skodovka = Auto(130)
skodovka.nastartuj()
skodovka.cestuj("Praha", "Plzeň")
```

> #### Příklad 
> Rozšiřte předchozí příklad o dědičnost.
> Třída Kniha může mít potomka PujcenaKniha, jenž bude mít navíc atribut datum, do kdy se má vrátit do knihovny.
```python
# Řešení
```

## Polymorfismus
Používám více různých objektů stejným způsobem.

více různých objektů
```python
class Auto(DopravniProstredek):
    def __init__(self, rychlost):
        super().__init__(rychlost)
        
    def nastartuj(self):
        print("startuji motor")
        
    def cestuj(self, odkud, kam):
        self.nastartuj()
        super().cestuj(odkud, kam)
        
trabant = Auto(90)
porsche = Auto(300)
kolobezka = DopravniProstredek(15)
```

využití stejným způsobem
```python
trabant.cestuj("Praha", "Brno")
porsche.cestuj("Praha", "Brno")
kolobezka.cestuj("Praha", "Brno")
```

## Skládání
Objekty obsahují jiné objekty.

```python
class Garaz(object):
    pass

class Kolo(DopravniProstredek):
    pass

g = Garaz()
g.prvni_misto = Auto(130)
g.druhe_misto = Kolo(30)
```

řetězení tečkové notace
```python
g.prvni_misto.cestuj("garáž", "práce")
```

### Skládání nebo dědičnost?
Typicky obojí dohromady.

- **Dědičnost** - "Co objekt je? Čeho je objekt specializací?"
- **Skládání** - "Z jakých komplexních částí je objekt složen?"

> #### Příklad 
> Vyvořte třídu Autor, která bude chovávat `jmeno` a `prijmeni` autora.
> Nezapomeňt atributy chranit proti nechtěnému změnění.
>  
> Upravte třídu Kniha by používala místo řetězce s jmenem autora objekt typu Autor.
```python
# Řešení: Autor
```

```python
# Řešení: Kniha
```

### Atributy třídy
Jedná se o proměnné, které napatří instaci objektu (jako například `jmeno` v našem příkladu se psy), ale třídě.
Dá se říci, že všichni psi sdílejí (přistupují) k jedné stejné proměnné.
```python
class Pes(object):

    # Atribut třídy Pes
    pocet_psu = 0
    def __init__(self, jmeno):
        self.jmeno = jmeno # atribut instance
        Pes.pocet_psu += 1 # atribut tridy
    
alik = Pes("Alik")
punta = Pes("Punta")
print(alik.jmeno)
print(punta.jmeno)
print(Pes.pocet_psu)
```


### Metody třídy
Podobně jak třída může mít atributy, ale také metody.
- Ty se označují *Annotací* `@classmethod`.
- Místo parametru `self` mají parametr `cls`
```python
class Trida():

    @classmethod
    def class_f(cls):
        print("Metoda tridy:" + str(cls))

t = Trida()
t.class_f()
Trida.class_f()
```

> #### Příklad:
> Vytvořte proměnou třídy, jenž bude počítat počet instancí *Knih*. 
> Tato hodnota bude vrácena skrze metodu třídy.
```python
# Řešení
```

#### *Poznámka*
Podobně jako atributy, které se dají definovat za běhu, dají se i metosdy přiřazovat oběktům za běhu.
```python
class Trida: 
    pass
    
def f(self):
    print("Funkce")

Trida.atribut = "a"
Trida.funkce = f

t1 = Trida()
t1.funkce()
```

### Destruktor
Speciální metoda `__del__`, která se volá v okamžiku zrušení objektu. 
Opak metody `__init__`
```python
class Pes:

    # Atribut třídy Pes
    pocet_psu = 0
    def __init__(self, jmeno):
        self.jmeno = jmeno # atribut instance
        Pes.pocet_psu += 1 # atribut tridy
        
        
    def __del__(self):
        Pes.pocet_psu -=1

alik = Pes("Alik")
punta = Pes("Punta")
print(Pes.pocet_psu)
del punta
print(Pes.pocet_psu)
```

> #### Pivní příklad
> - Vytvořte třídu SudPiva.
> - Vytvořte konstruktor, pomocí kterého nastavíte značku piva (proměnná) a objem sudu (vždy 50L).
> - Vytvořte metodu `natoc_pivo(objem_sklenice)`, která vypíše: *Točím pivo XXX do sklenice YYY, v sudu zbývá ZZZ.*
> - Ošetřete, pokud v sudu už není dostatek piva.
> 
> Použití:
> - Naplňte výčep sudy piva různých značek.
> - V cyklu natočte 30 náhodných půllitrů piva (použijte `random.choice`)
```python

import random

# TODO napsat třídu SudPiva

vycep = []
# TODO naplnit výčep
# TODO 30x natočit náhodné pivo z výčepu
```
