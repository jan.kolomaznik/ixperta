# Seznam pytest pluginu pro lepší svět

Pytest pluginu je velké množství.

```shell script
!pip search pytest | wc -l
```

Na oficiálním seznamu [pytest plugins](https://docs.pytest.org/en/2.7.3/plugins_index/index.html) jich najdete 105.
Na stránkách [pytest Plugins Compatibility](http://plugincompat.herokuapp.com/) jich je okolo 840.
Server [Pypi](https://pypi.org) lze jich najít mnohem více projektu (6000)

Ne všechny jsou užitečné, některé jsou spíše pro pobavení, například `pytest-neo` který upravý výstup pytestu, tak aby se podobal matrixu?

## Výstupy testů a reporty

### pytest-sugar
| [Pypi](https://pypi.org/project/pytest-sugar/)
| [GitHub](https://github.com/Teemu/pytest-sugar)
|

Patří mezi nejoblíbenější pluginy vubec.
`pytest-sugar` změní výchozí vzhled a chování pytestu, přidá indikátor průběhu a okamžitě zobrazí neúspěšných testy.

Nevyžaduje žádnou konfiguraci, stačí `pip install pytest-sugar`.

### pytest-clarity
| [Pypi](https://pypi.org/project/pytest-clarity/)
| [GitHub](https://github.com/darrenburns/pytest-clarity)
|

Pokud vám report popisující rozdíly mezi očekávaným a akuální stavem nestačí, tento plugin rozdíly ještě více zvrazní.

Jeho výhodou/nevýhodou je že pro jeho aktivaci musíte použít přepínač `-vv`

### pytest-tldr
| [Pypi](https://pypi.org/project/pytest-tldr/)
| [GitHub](https://github.com/freakboy3742/pytest-tldr)
|

Tento plugin je defakto opakem prvního pluginu, tedy zjednodušuje a minimalizuje výstup a odstraňuje z něj všechno zbytečné (barvy, ikony, ascii art ...)
Podobně jako `pytest-sugar` jej stačí pouze nainstalovat.

### pytest-html
| [Pypi](https://pypi.org/project/pytest-html/)
| [GitHub](https://github.com/pytest-dev/pytest-html) |

Pokud potřebuje výstupy z vašich testů vystavit pro ostatní vývoře, může se vám líbit tento plugin.

- Instalace pomocí: `pip install pytest-html`
- Použití pomocí přepínače: `pytest --html=report.html`

Tento plugin má bohaté možnosti konfigurace které najdete v dokumentaci.

### pytest-cov
| [Pypi](https://pypi.org/project/pytest-cov/)
| [GitHub](https://github.com/pytest-dev/pytest-cov)
| [Dokumentation](https://pytest-cov.readthedocs.io/en/latest/)
|

`pytest-cov` přidává podporu pokrytí kódu testy pro pytest.
Dokáže tedy ukázat, které řádky kódu byly testovány a které nikoliv, včetně výpočtu procenta pokrytí celého projektu.

- Instalace pomocí pip: `pip install pytest-cov`
- Pro získání jednoduchého přehledu: `pytest --cov=src tests/`

Výstup pytesy je pak rozšířen o následují report:

    ----------- coverage: platform linux, python 3.8.5-final-0 -----------
    Name              Stmts   Miss  Cover
    -------------------------------------
    src/__init__.py       0      0   100%
    src/graph.py         56     28    50%
    src/power.py         40      1    98%
    src/reset.py         31     31     0%
    src/weakness.py      47     47     0%
    -------------------------------------
    TOTAL               174    107    39%

pokud chce detailnější výstup, můžete si vygenerovat například html report: `pytest --cov-report html --cov=src tests/`
Výsledek pak najdete v souboru [htmlcov/](htmlcov/)

#### Fixtures a mark
Plugin také definuje jeden marker a fixture, který slouží vyloučení testu z coverage:

```python
@pytest.mark.no_cover
def test_foobar():
    # do some stuff that needs coverage disabled
```

```python
def test_foobar(no_cover):
    # same as the marker ...
```

## Změna chování pytest

### pytest-picked
| [Pypi](https://pypi.org/project/pytest-picked/)
| [GitHub](https://github.com/anapaulagomes/pytest-picked)
|

`pytest-picked` spustí jen ty testy, které se týkají kódu, který byl modifikovaný ale ještě nebyl commitnutý pomocí gitu.

- Instalace pomocí příkazu: `pip install pytest-picked`
- Použití pak moci přepínače: `pytest --picked`

### pytest-instafail
| [Pypi](https://pypi.org/project/pytest-instafail/)
| [GitHub](https://github.com/pytest-dev/pytest-instafail)
|

`pytest-instafail`  upravuje chování pytestu tak, že zobrazuje chyby okamžitě místo čekání na konec testovací relace.

- Instalace pomocí příkazu: `pip install pytest-instafail`
- Použití pak moci přepínače: `pytest --instafail`

    
### pytest-check
| [Pypi](https://pypi.org/project/pytest-check/)
| [GitHub](https://github.com/okken/pytest-check)
|

Tento plugin umožňuje aby selhání v testu neukončilo jebo běh, jinými slovy dovoluje aby jeden test selhal současně vícekrát, jedná se tedy o velmi kontroverzní plugin.
Je určitě nestmelené jej používat pro unit testy, případně integrační testy.

Pokud se jej ale rozhodne použít pro akceptační nebo end-to-end test, pak je naopak i docela užitečný,

Jeho dalším minusem ale je, že v těchto testech nemůže používat `assert`:
```python
import pytest_check as check


def test_example():
    a = 1
    b = 2
    c = [2, 4, 6]
    check.greater(a, b)
    check.less_equal(b, a)
    check.is_in(a, c, "Is 1 in the list")
    check.is_not_in(b, c, "make sure 2 isn't in list")
```

## Performance a čas

### pytest-xdist
| [Pypi](https://pypi.org/project/pytest-xdist/)
| [GitHub](https://github.com/pytest-dev/pytest-xdist)
|

`pytest-xdist` tento lehce problematický plugin umožňuje spouštět více testů paralelně pomocí parametru `-n`: například `pytest -n 2` by spustil vaše testy na dvou CPU.
Cílem je tedy zrychlit provádění testů.

Má některé zajímavé přepínače.
Například `--looponfail`, který automaticky znovu spustí vaše neúspěšné testy, což osobně považuje za velkou chybu toto použít.

Nebo schopnost spouštět testy vzdáleně přes ssh: `pytest -d --tx ssh=myhostpopen --rsyncdir mypkg mypkg`

### pytest-benchmark
| [Pypi](https://pypi.org/project/pytest-benchmark/)
| [GitHub](https://github.com/ionelmc/pytest-benchmark)
| [Dokumentation](https://pytest-benchmark.readthedocs.io/)
|

Další velmi zajímavý plugin, který umožňuje do restů integrovat měření výkonnosti.

Nemá smysl jej tedy používat pro jednotkové a integrační testy, ale je možné s ním realizovat performance test nebo měřit akceptační testy.

### pytest-profiling
| [Pypi](https://pypi.org/project/pytest-profiling/)
| [GitHub](https://github.com/manahl/pytest-plugins)
|

Podobný plugin jako benchmark.
Hlavní rozdíl je v tom že u předchozího musíte definovat co se do měření zařadí a co ne.
Tento plugin je spíše určení pro odhalení pomalých testů a fixtures.

## Práce s fixtures

### pytest-deadfixtures
| [Pypi](https://pypi.org/project/pytest-deadfixtures/)
| [GitHub](https://github.com/jllorencetti/pytest-deadfixtures)
|

Tento plugin odhaluje duplicitní a nepoužívané `fixtures`.

### pytest-fixture-tools
| [Pypi](https://pypi.org/project/pytest-fixture-tools/)
| [GitHub](https://github.com/pytest-dev/pytest-fixture-tools)
|

Jedná se o mocnější variantu předchozího pluginu.
Neumí se odhalovat nepoužívané `fixture`, ale duplicity už ano.

Na rozdíl od předchozí umí vytvořit graf závislosti mezi `fixture`.

## Práce se sítí
### pytest-socket
| [Pypi](https://pypi.org/project/pytest-socket/)
| [GitHub](https://pypi.org/project/pytest-socket/)
|

Tento plugin je bezpečnostní nutností v okamžiku testování aplikace, která komunikuje po síti.
Automaticky zakazuje všechna síťová spojení a nahrazuje chybou.

- Instance: `pip install pytest-socket`
- Konfigurace pomocí `pytest.ini`

    
    [pytest]
    addopts = --disable-socket

Nicméně možností konfigurace je více:
- Přístup k netu mžete řešit i programově pomocí `conftest.py`, makrů a `fixture`
- Případně povilit přístupy jen na určiteé adresu:


    [pytest]
    addopts = --allow-hosts=127.0.0.1,127.0.1.1


    
----
Zdroje:
- [pypi.org[pytest]](https://pypi.org/search/?q=pytest)
- [pytest Plugins Compatibility](http://plugincompat.herokuapp.com/)
- [8 great pytest plugins](https://opensource.com/article/18/6/pytest-plugins)
- [Pytest Plugins to Love](https://towardsdatascience.com/pytest-plugins-to-love-%EF%B8%8F-9c71635fbe22)
