# Základy jazyka

V následujícím bloku budeme Python používat jako "chytřejší kalkulačku".
Cílem je
- seznámit se základy jazyka
- s datovými typy

## Komentáře
V IPythonu je komentáře možné řešit v samostatném boku ve formátu `markdown`

Přímo v kódu je k uvození komentáře slouží znak `#`
```python
# Komentář
```

## Vystup konsole (`print()`)
Výsledek poslední operace se automaticky vypisuje na konzoli

Jinak je možné použít funkci `print()`
```python
# výpis na konzoli
print("ŽÁDNÉ středníky;")
print("ŽÁDNÉ { složené závorky }")
```

Vypsání více hodnot na jeden řádek
```python
print(1, 2, 3)
```

### Příklady implicitního vypisování
```python
10 + 20
```

```python
"slon"
```

## Proměnné
Symbolické jméno uchovávající určitou informaci.
- Stačí jen zadat jméno a přiřadit hodnotu
- Není potřeba deklarovat předem ani určovat typ 
- Nelze deklarovat proměnou bez přiřazení hodnoty
- Jednou definovaná proměnná v rámci skriptu stále uchovává stejnou hodnotu

```python
moje_oblibene_cislo = 42
```

#### Příklady:
```python
a = 2
print(a)
```

```python
b = 3.14
print(b)
```

```python
c = 'A'
print(c)
```

```python
d = "A"
print(d)
```

```python
a
```

```python
a = 10
print(a)
```

```python
a
```

## Vstup z konsole (`input()`)
Opakem příkazu `print()`, který zajišťuje výstup je `input()` pro vstup.

```python
a = input()
print(a)

```

Input se vstupní hláškou
```python
a = input("Strana: ")
print(a)
```


## Pravidla pro pojmenování proměnných
*[PEP8](https://www.python.org/dev/peps/pep-0008/) – Style Guide for Python Code*

Proměnné musí začínat písmenem či podtržítkem

Proměnné jsou *case sensitive*

```python
cislo = 1
Cislo = 2
číslo = 3
print(cislo, Cislo, číslo)
```

Pro více-slovné proměnné používáme jako oddělovač podtržítko `_` 
```python
moje_oblibene_cislo = 42
print(moje_oblibene_cislo)
```

Další ukázky 
```python
x = 10
print(x)
```

```python
_y = 20
$y = 50
9x = 30
print(_y)
print($y)
print(9x)

```

## Operátory přiřazení

Základní přiražení
```python
a = 1
b = a + 1
print(b)
```

Vícenásobné přiřazeni
```python
a, b, c = 1, 2, 3
print(a, b, c)
```

Prohození dvou proměných díky přiřazeni
```python
a = 1
b = 2
a, b = b, a
print(a, b)
```

> #### Příklad:
> Do proměnných `a,b,c` přiřaďte hodnoty `ahoj, 10, 20.23` a následně je vypište.
```python
# Řešení
```

### Operátor přiřazení kombinovavý s operací
Operátory přiřazení lze zkombinovat s operací, tedy zápis:
- `a = a + 1`
Je stejný jako zápis:
- `a += 1`

```python
a = 1
b = 1
a = a + 1
b += 1
print(a, b)
```

Takto se dají kombinovat i další binární operátory.

## Rezervovaná slova
Jedná se o slova, která nelze použít pro názvy proměnných.

`and `
`exec `
`not`
`assert`
`finally`
`or`
`break`
`for`
`pass`
`class`
`from`
`print`
`continue`
`lambda`
`yield`
`global`
`raise`
`def`
`if`
`return`
`del`
`import`
`try`
`elif`
`in`
`while`
`else`
`is`
`with `
`except`

## Konce řádků a středníky
Z mnoha programovacích jazyků jsme zvyklí ukončovat příkazy středníky.
To má velkou výhodu v tom, že když chceme napsat příkaz na více řádků, ničemu to nevadí.

V Pythonu platí co řádek to příkaz.
Když chceme napsat příkaz na více řádků, použijeme znak `\`.
```python
"Ahoj" +\
" " +\
"svetě."
```

## Import funkci, objeku, modulu
Pokud chceme použit něco, co není soušátí standatního balíčku, pak musíme provést import.

Například python nezná konstantu `pi`, ta je v modulu `math`.
```python
import math
math.pi
```

pokud bychom chtěli pouze `pi`
```python
from math import pi
pi
```
