# Functools

Modul Python **functools** nám poskytuje různé nástroje, které nám umožňují a povzbuzují k tomu, abychom psali znovu použitelný kód. 

Někteří z nich jsou:

- Partial functions
- Updating Partial wrappers
- Total ordering

## Partial functions
Partial funkce Python slouží k:

- Replikovat existující funkce s některámi argumenty již zadanými.
- Vytvoření nové verze funkce dobře zdokumentovaným způsobem.

Výše uvedené body lze dobře pochopit s některými příklady. 
Prozkoumejme je teď.

Předpokládejme, že máte funkci nazvanou multiplier, která jen násobí dvě čísla. 
Její definice vypadá takto:
```python
def multiplier(x, y):
    return x * y
    
multiplier(2, 3)
```

Co když chceme vytvořit nějaké nové funkce jako specální případy této funkce, například funkci zdvojnásobení nebo ztrojnásobení, budeme muset definovat nové funkce jako:

```python
def doubleIt(x):
    return multiplier(x, 2)

def tripleIt(x):
    return multiplier(x, 3)
    
    
doubleIt(2), tripleIt(3)
```

No, to bylo snadné, ale co se stane, když potřebujeme 1000 takových funkcí? 
Zde můžeme použít **Partial funkce**:

```python
from functools import partial

double = partial(multiplier, y=2)
triple = partial(multiplier, y=3)

'Double of 2 is {}'.format(double(5))
```

Můžeme dokonce vytvořit více částí ve smyčce:
```python
multiplier_partials = []
for i in range (1, 11):
    function = partial(multiplier, i)
    multiplier_partials.append(function)

print('Product of 1 and 2 is {}'.format(multiplier_partials[0](2)))
print('Product of 3 and 2 is {}'.format(multiplier_partials[2](2)))
print('Product of 9 and 2 is {}'.format(multiplier_partials[8](2)))
```

### Partial functions jsou self-documented
I když lze s částečnými funkcemi zacházet jako s zcela nezávislými funkcemi, samy o sobě nikdy neztratí paměť funkce, která je ovládá.

To lze prokázat z metadat doc.
```python
print('Function powering double is {}'.format(double.func))
print('Default keywords for double is {}'.format(double.keywords))
```

### Testování partial funkcí
Je jednoduché otestovat dílčí funkci. 
Můžeme dokonce otestovat jeho dokumentaci. 

Uvidíme, jak se to dělá:
```python
assert double.func == multiplier
assert double.keywords == {'y': 2}
```

Při spuštění tohoto skriptu se nezobrazí žádný výstup, protože tvrzení poskytují pouze chybový výstup, když selžou. 
Pokud projdou, tiše pokračují v provádění kódu.

### Aktualizujte metadata partial funkce pomocí functool.update_wrapper()
S modulem **functools** můžeme aktualizovat metadata funkce pomocí wrapperů.
Podívejme se na příklad útržku kódu, abychom objasnili, jak se to dělá:
```python
import functools as ft

def show_details(name, function):
    """Details callable object."""
    print('Name: {}'.format(name))
    print('\tObject: {}'.format(function))
    try:
        print('\t__name__: {}'.format(function.__name__))
    except AttributeError:
        print('\t__name__: {}'.format('__no name__'))
    print('\t__doc__ {}'.format(repr(function.__doc__)))
    return

show_details('raw wrapper', double)

print('Updating wrapper:')
print('\tassign: {}'.format(ft.WRAPPER_ASSIGNMENTS))
print('\tupdate: {}'.format(ft.WRAPPER_UPDATES))

ft.update_wrapper(double, multiplier)
show_details('updated wrapper', double)
```

Před aktualizací wrapper neměla partial funkce žádná data o svém názvu a správném řetězci doc.

## Total ordering pomocí functools
 Functools modul také poskytuje způsob, jak poskytovat automatické porovnávací funkce. 
 
Pro splnění výsledků je třeba splnit dvě podmínky:

- Definice alespoň jedné srovnávací funkce je třeba jako `le`, `lt`, `gt` nebo `ge`.
- Definice funkce `eq` je povinná.

Výchozí implementace bez Total_ordering
```python
class Number:
    def __init__(self, value):
        self.value = value
    def __lt__(self, other):
        return self.value < other.value
    def __eq__(self, other):
        return self.value == other.value

print(Number(2) == Number(2))
print(Number(2) == Number(3))
print(Number(1) < Number(2))
print(Number(10) > Number(21))
print(Number(10) <= Number(2))
print(Number(10) >= Number(20))
print(Number(2) <= Number(2))
print(Number(2) >= Number(2))
```

A s total ordeding
```python
from functools import total_ordering

@total_ordering
class Number:
    def __init__(self, value):
        self.value = value
    def __lt__(self, other):
        return self.value < other.value
    def __eq__(self, other):
        return self.value == other.value

print(Number(2) == Number(2))
print(Number(2) == Number(3))
print(Number(1) < Number(2))
print(Number(10) > Number(21))
print(Number(10) <= Number(2))
print(Number(10) >= Number(20))
print(Number(2) <= Number(2))
print(Number(2) >= Number(2))
```

To bylo ve skutečnosti snadné pochopit, protože nám umožnilo odstranit nadbytečný kód v naší definici třídy.

## Funkce `reduce()`
Smyčku for můžete zjednodušit pomocí funkce `functools.reduce()`. 
Tato funkce přijímá binární funkci `func` a iterovatelné vstupy jako argumenty a „redukuje“ vstupy na jednu hodnotu použitím kumulativních funkcí na páry objektů v iterovatelné.

Například `functools.reduce(operator.add, [1, 2, 3, 4, 5])` vrátí součet 1 + 2 + 3 + 4 + 5 = 15. 
stejným způsobem jako `akumulovat()`, kromě toho, že v nové sekvenci vrátí pouze konečnou hodnotu.
```python
import functools as ft
import operator

ft.reduce(operator.add, [1, 2, 3, 4, 5])
```
S použitím funkce `redukce()` se můžete uspěšně zbavit smyčky `for`:
```p
ft.reduce(max, [1, 5, -2, 3, 4])
```

Podle dokumentace je funkce `reduce()` ekvivalntem k:
```python
def reduce(function, iterable, initializer=None):
    it = iter(iterable)
    if initializer is None:
        value = next(it)
    else:
        value = initializer
    for element in it:
        value = function(value, element)
    return value
```

## Dekorátor funkce `@lru_cache`
Dekodér `@lru_cache` lze použít pro zabalení drahé, výpočetně náročné funkce pomocí mezipaměti *Least Recently Used*. 
To umožňuje volání funkcí, aby se budoucí volání se stejnými parametry mohla okamžitě vrátit namísto toho, aby byly přepočítávány.
```python
from functools import lru_cache

#@lru_cache(maxsize=None)  # Boundless cache
def fibonacci(n):
    if n < 2:
        return n
    return fibonacci(n-1) + fibonacci(n-2)
```

Nejprve si změříme výkon funkce bez dekorátoru, pak se dekorátorem:
```python
%%timeit
[fibonacci(i) for i in range(10)]
```

Ve výše uvedeném příkladu je hodnota `fibonacci(3)` vypočtena pouze jednou, zatímco pokud fibonacci nemá LRU cache, `fibonacci(3)` by byl vypočítán nahoru 230 krát.
Proto je `@lru_cache` obzvláště vhodný pro rekurzivní funkce nebo dynamické programování, kde drahá funkce může být volána vícekrát se stejnými přesnými parametry.

`@lru_cache` má dva argumenty:

- `maxsize`: Počet volání k uložení. 
  Když počet jedinečných volání překročí maxsize, mezipaměť LRU odstraní nejméně naposledy používané hovory.
- `typed` (přidáno v 3.3): Příznak pro určení, zda ekvivalentní argumenty různých typů patří do různých záznamů mezipaměti (tj. pokud se 3.0 a 3 počítají jako různé argumenty)

Vidíme také statistiky mezipaměti:
```python
fib.cache_info()
```

## Další funkce
Pro další zkoumání doporučuji se podívat na stranky [dokumentace](https://docs.python.org/3/library/functools.html).

Už jen proto, že jsme zde nevysvětlili všechny funkce, chybí například:
- `functools.cmp_to_key(func)`
- `class functools.partialmethod(func, *args, **keywords)`
- `@functools.singledispatch`

