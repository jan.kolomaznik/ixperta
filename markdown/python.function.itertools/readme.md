# Itertools

Itertools bylo nazýváno "drahokam" a "do značné míry nejlepší věc vůbec", a pokud jste o něm neslyšeli, pak vám chybí jeden z největších koutů standardní knihovny Python 3.

Pro učení, jaké funkce jsou k dispozici v modulu **itertools**, existuje několik vynikajících zdrojů. 
[Dokumentace](https://docs.python.org/3/library/itertools.html) sama o sobě jsou skvělým místem, kde pokračovat.

Jde o to, že **itertools** nestačí jen znát definice funkcí, které obsahuje. Skutečná síla spočívá ve skládání těchto funkcí, aby se vytvořil rychlý, paměťově efektivní a dobře vypadající kód.

V následující lekci budeme tyto funkce skádat v praktických příkladech, které vás budou povzbuzovat k „přemýšlení iterativně“. 
Příklady obecně začnou jednoduché a postupně se zvyšují složitosti.

## Co je to Itertools

Podle **itertools docs** je to „modul, který implementuje řadu iterátorových stavebních bloků inspirovaných konstrukty z APL, Haskell a SML… Společně tvoří„ iterátorovou algebru “.

### Funkce `zip()`

Volně řečeno to znamená, že funkce v itertools „fungují“ na iterátorech, aby produkovaly složitější iterátory. 
Zvažte například vestavěnou funkci `zip()`, která bere libovolný počet `iterables` jako argumenty a vrací iterátor přes n-tice odpovídajících prvků:

```python
list(zip([1, 2, 3], ['a', 'b', 'c']))
```

> #### Příklad
> Přepište následující kód pomocí funkce zip:
```python
def find_diference(a_list, b_list):
    min_index = min(len(a_list), len(b_list))
    for i in range(min_index):
        if a_list[i] != b_list[i]:
            return a_list[i], b_list[i]
            
print(find_diference([1,2,3],[1,3,2]))
print(find_diference([1,2,3],[1,2,3]))
```

Jak přesně funguje `zip()`?

### Funkce `iter()`

`[1, 2, 3]` a `['a', 'b', 'c']`, stejně jako všechny seznamy, jsou iterovatelné, což znamená, že mohou své prvky vrátit po jednom. 
Technicky je každý objekt Pythonu, který implementuje metody .`__iter__()` nebo `__getitem__()`.
 
Vestavěná funkce `iter()`, když je volána na seznamu, vrácí právě takvový iterátor.

```python
iter([1, 2, 3, 4])
```

Funkce `zip()` funguje v podstatě tím, že volá `iter()` na každém jeho argumenty, pak postupovat každý iterator vrátil `iter()` s `next()` a agregovat výsledky do n-tic. 
Iterátor vrácený `zip()` iteruje přes tyto n-tice.

> #### Příklad
> Funkce zip se ale dá použit i "rozbalení" senzamu tuplu.
> Mějme seznam trojic obsahují jméno, věk a váhu a vytvořte tři seznamy (jmen, věků a vah)
```python
def unzip(persons):
    names = []
    ages = []
    weights = []
    for person in persons:
        names.append(person[0])
        ages.append(person[1])
        weights.append(person[2])
    return names, ages, weights
    
unzip([
    ("Honza", 36, 85),
    ("Jirka", 34, 105),
    ("Eva", 26, 60)
])
```

### Funkce `map()`

Vestavěná funkce `map()` je dalším „operátorem iterátoru“, který ve své nejjednodušší podobě aplikuje funkci jednoho parametru na každý prvek iterovatelného jednoho prvku najednou:

```python
list(map(len, ['abc', 'de', 'fghi']))
```

Funkce `map()` funguje tak, že volá `iter()` na svém druhém argumentu, pokračuje metodou `next()`, dokud není iterátor vyčerpán.
Funkce předana jeho prvnímu argumentu se palikuje na hodnoty vrácené `next()` v každém kroku. 
**Funkce `map()` ale navrací výsledeky ale iterátor na výsledky!**

Ve výše uvedeném příkladu je použita funkce `len()` na každém prvku seznam `['abc', 'de', 'fghi']`, aby vrátil iterátor přes délky každého řetězce v seznamu.

Protože iterátory jsou iterovatelné, můžete kombinovat `zip()` a `map()` a vytvořit iterátor nad kombinací prvků ve více než jedné iterovatelné. 

Například následující součty odpovídající prvky ze dvou seznamů:

```python
list(map(sum, zip([1, 2, 3], [4, 5, 6])))
```

To je to, co je míněno funkcemi v itertoolech tvořících „iterátorovou algebru“. 
Itertools je nejlépe považováno za soubor stavebních bloků, které lze kombinovat do specializovaných „datových potrubí“, jako je tomu v příkladu výše.

## Proč Itertools používat?
Existují dva hlavní důvody, proč je tato „iterátorová algebra“ užitečná: zlepšila se efektivita paměti (prostřednictvím lazy vyhodnocení) a rychlejší doba provádění. 


> #### Příklad: 
> Zvažte následující problém:
>
> Vezmeme-li seznam vstupů hodnot a kladné celé číslo n, zapište funkci, která rozděluje vstupy do skupin délky n. 
> Pro jednoduchost předpokládejme, že délka vstupního seznamu je dělitelná n. 
> Například, jestliže vstupy = `[1, 2, 3, 4, 5, 6]` a `n = 2`, vaše funkce by měla vrátit se `[(1, 2), (3, 4), (5, 6)]`.
>
> Zolte naivní přístup pomocí pracovního seznau a forcyklu nebo comprehensions:

```python
def naive_grouper(inputs, n):
    pass # Řešení
    
```

Když to otestujete, uvidíte, že funguje podle očekávání:

```python
nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
naive_grouper(nums, 2)
```

Co se stane, když se pokusíte předat mu seznam, řekněme, 100 milionů prvků? 
Budete potřebovat spoustu paměti! 
I když máte k dispozici dostatek paměti, program bude chvíli viset, dokud nebude seznam výstupů naplněn.

```python
%%timeit
for _ in naive_grouper(range(100000000), 10):
    pass
```

Implementace `list` a `tuple` v `naive_grouper()` vyžaduje přibližně 4,5GB paměti pro zpracování rozsahu (100000000). 
Práce s iterátory tuto situaci výrazně zlepšuje. Zvažte následující:

```python
def better_grouper(inputs, n):
    iters = [iter(inputs)] * n
    return zip(*iters)
```

V této malé funkci se toho děje hodně, takže si to rozeberme konkrétním příkladem.
Výraz `[iters (input)] * n` vytvoří seznam `n` odkazů na stejný iterátor:

```python
nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
iters = [iter(nums)] * 2
list(id(itr) for itr in iters)  # IDs are the same.
```

Následně `zip(*iters)` vrací iterátor přes páry odpovídajících prvků každého iterátoru v iters. 
Když je první prvek, 1, převzat z „prvního“ iterátoru, „druhý“ iterátor nyní začíná na 2, protože je to jen odkaz na „první“ iterátor, a proto byl posunut o jeden krok. Takže první n-tice vytvořená zipem () je (1, 2).

V tomto okamžiku začínají „iterátory“ v itersu 3, takže když `zip()` táhne 3 z „prvního“ iterátoru, dostane 4 z „druhého“, aby vytvořil n-tici (3, 4). 
Tento proces pokračuje, dokud `zip()` konečně nevytvoří (9, 10) a „oba“ iterátory v iters nejsou vyčerpány:

```python
nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list(better_grouper(nums, 2))
```

Funkce `better_grouper()` je lepší z několika důvodů. 
- Zaprvé, bez odkazu na vestavěný modul `len()`, může mít parametr `better_grouper()` jakýkoli iterovatelný argument (i nekonečné iterátory). 
- Za druhé, vrácením iterátoru spíše než seznamu, může `better_grouper()` zpracovat enerables iterables bez problémů a používá mnohem méně paměti.

```python
%%timeit
for _ in better_grouper(range(100000000), 10):
    pass
```

Nyní, když jste viděli, co je to **itertools** („iterator algebra“) a proč byste jej měli používat, zlepšila se efektivita paměti a rychlejší doba provádění.

## Funkce `itertools.zip_longest()`

Problém s `better_grouper()` spočívá v tom, že nezpracovává situace, kdy hodnota předaná druhému argumentu není v prvním argumentu faktorem délky iterovatelné:

```python
nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list(better_grouper(nums, 4))
```

Prvky 9 a 10 chybí ve seskupeném výstupu. 
To se děje proto, že `zip()` přestane agregovat prvky, jakmile je nejkratší iterovatelná složka vyčerpána. 
Bylo by více smysluplné vrátit třetí skupinu obsahující 9 a 10.

K tomu můžete použít `itertools.zip_longest()`. 
Tato funkce přijímá libovolný počet iterables jako argumenty a argument klíčového slova `fillvalue`, který je standardně nastaven na None. 

Nejjednodušší způsob, jak získat rozdíl mezi `zip()` a `zip_longest()`, je podívat se na některý příklad výstupu:

```python
import itertools as it
x = [1, 2, 3, 4, 5]
y = ['a', 'b', 'c']
print(list(zip(x, y)))
print(list(it.zip_longest(x, y)))
```

> #### Příklad
> Vytvořte funkci `grouper`, která bude bude chybějící prvky doplňovat výchozí hodnotou
```python
def grouper(inputs, n, fillvalue=None):
    pass # Řešení
```

```python
nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list(grouper(nums, 4))
```

## Et tu, Brute Force?

Zde je společný problém ve stylu rozhovoru:

> Máte tři dolarové bankovky v hodnotě 20 dolarů, pět dolarových bankovek v hodnotě 10 dolarů, dvě dolarové bankovky v hodnotě 5 dolarů a pět dolarových bankovek v hodnotě 1 USD. 
> Kolik způsobů můžete zaplatit 100$ učet?
> Jaké jsou kombinace dolarových bankovk?

Chcete-li vyřešit tento problém pomocí "brutální síly" tento problém, stačí postupně zkoušet vybítat všechny možné kombinace z peněženky a kontrolovat, jetli jejich součet se nerovná 100$.

Nejprve vytvořte bankovek, které máte ve své peněžence:

```python
wallet = [20, 20, 20, 10, 10, 10, 10, 10, 5, 5, 1, 1, 1, 1, 1]
```

Volba *k položek* z množiny o velikosti *n* se nazývá *kombinace* a itertools má tady funkce `itertools.combinations()`.
Ta má dva argumenty - iterovatelné vstupy a kladné celé číslo n - a vytváří iterátor nad n-ticemi všech kombinací n prvků ve vstupech.

Chcete-li například zobrazit seznam bankovek v peněžence, postupujte takto:

```python
list(it.combinations(wallet, 3))
```

Chcete-li tento problém vyřešit, můžete smyčku od `1` do `len(wallet)`, pak zkontrolujte, které kombinace každé velikosti přidat až 100 dolarů:

```python
def wallet_100(wallet):
    result = []
    for n in range(1, len(wallet) + 1):
        for combination in it.combinations(wallet, n):
            if sum(combination) == 100:
                result.append(combination)
    return result
```

Pokud tisknete značky make_100, všimnete si, že existuje mnoho opakovaných kombinací.
Chcete-li odstranit duplikáty z značky make_100, můžete je převést na množinu:

```python
wallet = [20, 20, 20, 10, 10, 10, 10, 10, 5, 5, 1, 1, 1, 1, 1]
set(wallet_100(wallet))
```

Takže existuje pět způsobů, jak změnit účet za 100 dolarů s účty, které máte v peněžence.

### Brute Force 2
Zde je variace na stejný problém:

> Kolik způsobů je třeba provést za účet ve výši 100 USD za použití libovolného počtu 50 USD, 20 USD, 10 USD, 5 USD a 1 dolarových bankovek?

V tomto případě nemáte přednastavenou kolekci bankovek, takže potřebujete způsob, jak generovat všechny možné kombinace pomocí libovolného počtu účtů. 
K tomu budete potřebovat funkci `itertools.combinations_with_replacement()`.

Funguje stejně jako `combinations()`, přijímá iterovatelné vstupy a kladné celé číslo n a vrací iterátor přes n-tice prvků ze vstupů. 
Rozdíl je v tom, že `itertools.combinations_with_replacement()` umožňuje opakování prvků v n-ticích, které vrací.

Například:

```python
list(it.combinations_with_replacement([1, 2], 2))
```

Pro srovnání kasický `combinations()`

```python
list(it.combinations([1, 2], 2))
```

> #### Příklad
> Upravte předchozí příklad s bankovkami:
```python
def bills_100(bills):
    pass # Řešení
```

V tomto případě nemusíte odstraňovat žádné duplikáty, protože `combinations_with_replacement()` vevytváří žádné duplikáty:

```python
bills = [50, 20, 10, 5, 1]
len(bills_100(bills))
```

Pokud spustíte výše uvedené řešení, můžete si všimnout, že trvá chvíli, než se výstup zobrazí. 
To proto, že musí zpracovat 96,560,645 kombinací!

Další funkcí "itertools" je `permutations(), která přijímá jednu iterovatelnou a produkuje všechny možné permutace jejích prvků:

```python
list(it.permutations(['a', 'b', 'c']))
```

> #### Příklad
> Kolik vznikne premutací pro desetiptvkový seznam?
```python
# Řesení
```

Fenomén jen několika vstupů produkujících velký počet výstupů se nazývá kombinatorická exploze a je to něco, co je třeba mít na paměti při práci s `combinations()`, `combinations_with_replacement()` a `permutations()`.

Obvykle je nejlepší vyhnout se algoritmům brutální síly, i když existují případy, kdy je třeba je použít (například pokud je správnost algoritmu kritická, nebo je třeba zvážit každý možný výsledek). V takovém případě jste na to použili itertooly.

## Sekvence čísel

Pomocí itertools můžete snadno generovat iterátory nad nekonečnými sekvencemi. 
V této sekci budete zkoumat numerické posloupnosti, ale nástroje a techniky, které zde vidíte, nejsou v žádném případě omezeny na čísla.

### Sudá a lichá čísla

Pro první příklad vytvoříte pár iterátorů přes sudá a lichá celá čísla, aniž by to výslovně dělalo aritmetiku. 

Nejprve se podívejme na řešení pomocí generátorů:
```python
def evens():
    """Generate even integers, starting with 0."""
    n = 0
    while True:
        yield n
        n += 2

evens = evens()
list(next(evens) for _ in range(5))
```

```python
def odds():
    """Generate odd integers, starting with 1."""
    n = 1
    while True:
        yield n
        n += 2

odds = odds()
list(next(odds) for _ in range(5))
```

To je docela jednoduché, ale s itertools to můžete udělat mnohem kompaktněji.
Funkce, kterou potřebujete, je `itertools.count()`, která dělá přesně to, co zní: počítá se standardně s číslem 0.

```python
counter = it.count()
list(next(counter) for _ in range(5))
```

Můžete začít počítat z libovolného čísla, které se vám líbí, pomocí parametru `start=`, který je standardně nastaven na 0. 
Můžete také nastavit parametr kroku `step=`, abyste určili interval mezi čísly.7

> #### Příklad
> Pomocí `count()` vegenerujte sudou a lichou řay čísel
```python
# Řešení sudá
```

```python
# Řešení lichá
```

Další příklady použití:
```python
count_with_floats = it.count(start=0.5, step=0.75)
list(next(count_with_floats) for _ in range(5))
```

```python
negative_count = it.count(start=-1, step=-0.5)
list(next(negative_count) for _ in range(5))
```

V některých ohledech je `count()` podobný vestavěné funkci `range()`, ale `count()` vždy vrací nekonečnou posloupnost. 
Možná by vás zajímalo, co je to nekonečná posloupnost, protože je nemožné zcela iterovat. 

Příklad použítí nekonečného iterátoru, která emuluje chování vestavěné funkce `enumerate()`:

```python
list(zip(it.count(), ['a', 'b', 'c']))
```

Je to jednoduchý příklad, ale přemýšlejte o tom: právě jste vypsali seznam bez smyčky a bez znalosti délky seznamu předem.

## Rekurze

Rekurzivní volání funkce, je způsob, jak popsat posloupnost čísel.
Jeden z nejznámějších recenačních vztahů je ten, který popisuje Fibonacciho sekvenci.

Fibonacciho posloupnost je posloupnost 0, 1, 1, 2, 3, 5, 8, 13, .... Začíná 0 a 1 a každé následující číslo v pořadí je součtem předchozích dvou. 
Čísla v této sekvenci se nazývají Fibonacciho čísla. 

To je obyčejné vidět Fibonacci sekvenci produkovanou s generátorem:
```python
def fibs():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b
```

Vztah opakování popisující Fibonacciho čísla se nazývá relace opakování druhého řádu, protože pro výpočet dalšího čísla v sekvenci je třeba se za ním podívat zpět na dvě čísla.
Jak možná hádáte, vztah opakování prvního řádu.

V této části budete konstruovat funkce pro tvorbu libovolné sekvence, jejíž hodnoty lze popsat s relací opakování prvního nebo druhého řádu.

Už jste viděli, jak `count()` může generovat posloupnost nezáporných celých čísel, sudých celých čísel a lichých celých čísel. 
Můžete jej také použít ke generování posloupnosti *3n = 0, 3, 6, 9, 12,…* a *4n = 0, 4, 8, 12, 16,…*.

```python
count_by_three = it.count(step=3) # 0, 3, 6, 9, 12, ...
count_by_four = it.count(step=4)  # 0, 4, 8, 12, 16, ...
```

Ve skutečnosti může `count()` vytvářet sekvence násobků libovolného čísla, které si přejete. 
Tyto sekvence lze popsat pomocí relací opakování prvního řádu. 

Dalším jednoduchým příkladem sekvence prvního řádu je konstantní posloupnost *n, n, n, n, n…*, kde *n* je jakákoliv hodnota, kterou byste chtěli. 
Tuto sekvenci vygenerujeme funkcí `repeat()`:

```python
all_ones = it.repeat(1)  # 1, 1, 1, 1, ...
all_twos = it.repeat(2)  # 2, 2, 2, 2, ...
```

Další užitečnou funkcí je `itertools.cycle()`. 
Tato funkce přebírá iterovatelné vstupy jako argument a vrací nekonečný iterátor nad hodnotami vstupů, které se vrátí na začátek, jakmile je dosažen konec vstupů.
Chcete-li tedy vytvořit alternativní posloupnost jedniček a minus jedniček, můžete to udělat takto:

```python
alternating_ones = it.cycle([1, -1])  # 1, -1, 1, -1, 1, -1, ...
```

Cílem této části je však vytvořit jednu funkci, která může generovat jakýkoli vztah opakování prvního řádu.
 
Jeden způsob, jak toho dosáhnout, je `itertools.accumulate()`.

Funkce `accumulate()` má dva argumenty 
- iterovatelné vstupy
- binární funkční func (tj. Funkci s přesně dvěma vstupy) 
- a vrátí iterátor nad akumulované výsledky použití func na prvky vstupů. 

Je zhruba ekvivalentní následujícímu generátoru:
```python
def accumulate(inputs, func):
    itr = iter(inputs)
    prev = next(itr)
    for cur in itr:
        yield prev
        prev = func(prev, cur)
```

Příklad pomocí `itertools`:
```python
import operator
list(it.accumulate([1, 2, 3, 4, 5], operator.add))
```

První hodnota v iterátoru vrácená funkcí `accumulate()` je vždy první hodnotou ve vstupní sekvenci. 
Ve výše uvedeném příkladu je to 1 - první hodnota v `[1, 2, 3, 4, 5]`.

Další hodnota ve výstupním iterátoru je součtem prvních dvou prvků vstupní posloupnosti: `add(1, 2) = 3`. 
Pro vytvoření další hodnoty `accumulate()` vezme výsledek `add(1, 2)` a přidá na třetí hodnotu ve vstupní sekvenci.

Druhý argument `accumulate()` má výchozí hodnotu `operator.add()`.

> #### Příklad
> Použijte vestavěnou funkci `min()` pro získámí seznmau s nejmenšími prvky.
```python
seznam = [9, 21, 17, 5, 11, 12, 2, 6]
```

Pro více komplekní příklad je možné předat jako paramter **lambda** funkci.

```python
list(it.accumulate([1, 2, 3, 4, 5], lambda x, y: (x + y) / 2))
```

## Karetní příklad
Předpokládejme, že vytváříte pokerovou aplikaci. 
Budete potřebovat balíček karet. 
Můžete začít definováním seznamu hodností (eso, král, královna, kluk, 10, 9 atd.) 
A seznam barev (srdce, diamanty, kluby a piky):

```python
ranks = ['A', 'K', 'Q', 'J', '10', '9', '8', '7', '6', '5', '4', '3', '2']
suits = ['H', 'D', 'C', 'S']
```

### Vytvoření herního balíčku
Můžete představovat kartu jako n-tici, jejíž první prvek je hodnost a druhým prvkem je barva. 
Balíček karet by byl sbírkou takových n-tic. 
Balíček by se měl chovat jako skutečná věc, takže má smysl definovat generátor, který vydává karty po jednom a vyčerpá se, jakmile budou rozdány všechny karty.

Jedním ze způsobů, jak toho dosáhnout, je napsat generátor s vnořenou smyčkou přes hodnosti a barvy:

```python
def cards():
    """Return a generator that yields playing cards."""
    for rank in ranks:
        for suit in suits:
            yield rank, suit
```

A ještě jednou to stejné pomocí comprahations:
```python
cards = ((rank, suit) for rank in ranks for suit in suits)
```

Někteří by však mohli namítnout, že je to ve skutečnosti obtížnější pochopit, než explicitnější vnořené pro smyčku.

Pomáhá si podívat na vnořené smyčky z matematického hlediska - to znamená jako karteziánský součin dvou nebo více iterovatelných. 
V matematice, karteziánský součin dvou souborů A a B je soubor všech n-tic formy (a, b) kde a je element A a b je element B.

Funkce `itertools.product()` je určena právě pro tuto situaci. 
Na vstupu je libovolný počet iterables a vrací iterátor přes n-tice v kartézském součinu:

*Poznámka: Kartéský součin je anglicky: **Cartesian product***
```python
list(it.product([1, 2], ['a', 'b']))
```

Funkce `product()` není v žádném případě omezena na dva iterables. 
Můžete ji příjmout libovolný počet - ani nemusí mít stejnou velikost.
 
> #### Příklad:
> Zjistěte, zda můžete předpovědět, jaký bude vypadat výsleke pro `[1, 2, 3]`, `['a', 'b']`, `['X']`.
```python
# Ŕešení
```

Pomocí funkce `product()` můžete karty přepisovat do jednoho řádku:
```python
cards = it.product(ranks, suits)
```

### Zamíchání karet
Karetní balíček vygenerování, musí jej ale před samotnou hrouz umšt zamíchat:

```python
import random

def shuffle(deck):
    """Return iterator over shuffled deck."""
    deck = list(deck)
    random.shuffle(deck)
    return iter(tuple(deck))

cards = shuffle(cards)
```

Při tomo ale není použito itertools a navíc je vytvářena kopie balíčku kater.
Jistou možností je karty generovat již zamíchané.
V dokumentaci je uvedena následující metoda:
```python
def random_product(*args, **kwds):
    "Random selection from itertools.product(*args, **kwds)"
    pools = map(tuple, args) * kwds.get('repeat', 1)
    return tuple(random.choice(pool) for pool in pools)
```

### Snímnutí balíčku
Jako zdvořilost se hráčům pokru nabízí snímnutí balíčku.
Pokud si představíte, že karty jsou naskládány na stůl, hráč si vybere číslo `n` a pak odstraní první `n` karty z horní části zásobníku a přesune je na dno.

Pokud znáte něco o `cut()`, můžete tak učinit takto:
```python
def cut(deck, n):
    """Return an iterator over a deck of cards cut at index `n`."""
    if n < 0:
        raise ValueError('`n` must be a non-negative integer')

    deck = list(deck)
    return iter(deck[n:] + deck[:n])

cards = cut(cards, 26)  # Cut the deck in half.
```

Funkce `cut()` nejprve převede balíček na seznam, takže jej můžete rozdělit na dvě poloviny a ty opět sloučit ale v opačném pořadí.
Chcete-li zaručit, že se vaše řezy chovají podle očekávání, musíte zkontrolovat, zda je n nezáporné. 
Pokud to tak není, raději udělejte výjimku, aby se nic nedělo.

Funkce `cut()` je velmi jednoduchá, ale trpí několika problémy. 
Když vytváříme seznam, vytvoříte kopii původního seznamu a vrátíte nový seznam s vybranými prvky. 
S balíčkem pouze 52 karet je tento nárůst složitosti prostoru triviální, ale můžete snížit režii paměti pomocí ite0rtools. 
K tomu budete potřebovat tři funkce: `itertools.tee()`, `itertools.islice()` a `itertools.chain()`.

Podívejme se, jak tyto funkce fungují.

#### Funkce `tee()`
Funkce `tee()` může být použita k vytvoření libovolného počtu nezávislých iterátorů z jednoho iterovatelného. 
Iterátory jsou vráceny v n-tici délky n.

```python
iterator1, iterator2 = it.tee([1, 2, 3, 4, 5], 2)
print(list(iterator1))
print(list(iterator1))  # iterator1 is now exhausted.
print(list(iterator2))  # iterator2 works independently of iterator1
```

Zatímco `tee()` je užitečný pro vytváření nezávislých iterátorů, je důležité trochu porozumět tomu, jak funguje pod kapotou. 
Při volání `tee()` vytvořit n nezávislé iterátory, každý iterátor pracuje v podstatě s vlastní fronty FIFO.
Když je hodnota extrahována z jednoho iterátoru, tato hodnota je připojena k frontám pro ostatních iterátorů. 
Pokud je tedy jeden iterátor vyčerpán před ostatními, každý zbývající iterátor bude mít v paměti kopii celého iterovatelného puvodního iterátoru.
 
Z tohoto důvodu by mělo být `tee()` používáno s opatrností. 
Pokud vyčerpáváte velké části iterátoru před tím, než začnete pracovat s ostatními vrácenými `tee()`, můžete být vhodnější přenést vstupní iterátor do seznamu nebo n-tice.

#### Funkce `islice()`
Funkce `islice()` funguje stejně jako řezání seznamu nebo n-tice. 
Předáte ji iterovatelný, počáteční a zastavovací bod, stejně jako řezání seznamu.

Můžete také volitelně zadat hodnotu kroku.
 
Největší rozdíl je samozřejmě v tom, že `islice()` vrací iterátor.
```python
# Slice from index 2 to 4
list(it.islice('ABCDEFG', 2, 5))
```

```python
# Slice from beginning to index 4, in steps of 2
list(it.islice([1, 2, 3, 4, 5], 0, 5, 2))
```

```python
# Slice from index 3 to the end
list(it.islice(range(10), 3, None))
```

```python
# Slice from beginning to index 3
list(it.islice('ABCDE', 4))
```

Poslední dva příklady uvedené výše jsou užitečné pro zkrácení iterables. 
Jako bonus navíc `islice()` nepřijme záporné indexy pro pozice start / stop a hodnotu kroku, takže nemusíte vznášet výjimku, pokud je n negativní.

#### Funkce `chain()`
Poslední funkce, kterou potřebujete, je `chain()`. 
Tato funkce bere libovolný počet iterables jako argumenty a "spojí" je dohromady. 
Například:
```python
list(it.chain('ABC', 'DEF'))
```

```python
list(it.chain([1, 2], [3, 4, 5, 6], [7, 8, 9]))
```

> #### Příklad:
> Nyní, když máte vše potřebné, můžete přepsat funkci `cut()`, aby se balíček karet rozdělil, aniž by se v paměti uložily karty s plnou kopií:
```python
def cut(deck, n):
    """Return an iterator over a deck of cards cut at index `n`."""
    pass # Řešení
```

```python
cards = cut(cards, 26)
list(cards)
```

### Rozdání karet hráčům
Dalším krokem je rozdat karty hráčům.
Můžete napsat funkci `deal()!, která vezme balíček, počet hračl (hand) a početkaret na ruce a vrátí n-tici obsahující zadaný počet rukou.

K zápisu této funkce nepotřebujete žádné nové funkce itertools. 

> #### Příklad:
> Než si přečtete dopředu, podívejte se, co můžete přijít sami.
```python
def deal(deck, num_hands=1, hand_size=5):
    pass # Řešení
```

Zde je jedno řešení:
```python
def deal(deck, num_hands=1, hand_size=5):
    iters = [iter(deck)] * hand_size
    return tuple(zip(*(tuple(it.islice(itr, num_hands)) for itr in iters)))
```

Začnete vytvořením seznamu závislých iterátorů.
Potom tento seznam opakujete, při každém kroku odstraníte karty num_hands a uložíte je do n-tic.

Následně pomocí funkce `zip()` tyto spojíme do výsledného seznamu.
 
Tato implementace nastavuje výchozí hodnoty pro num_hands na 1 a hand_size na 5 - možná vytváříte aplikaci „Five Card Draw“.

```python
p1_hand, p2_hand, p3_hand = deal(cards, num_hands=3)
print(p1_hand)
print(p2_hand)
print(p3_hand)
```

Co si myslíte, že stav karet je nyní, když jste rozdali tři karty pěti karet?
```python
len(list(cards))
```

Patnáct karet rozdaných je spotřebováno z karty iterátoru, což je přesně to, co chcete. 
Tak, jak hra pokračuje, stav iterátoru karet odráží stav balíčku ve hře.

## Odbočení: Práce se seznam seznamů

V předchozím příkladu jste použili `chain()` pro navázání jednoho iterátoru na konec druhého. 
Funkce `chain()` má metodu třídy `.from_iterable()`, která trvá jeden iterovatelný jako argument. 
Prvky iterovatelné musí být samy o sobě iterovatelné, takže čistý efekt je ten, že `chain.from_iterable()` vrací flattens pro své argumenty:
```python
list(it.chain.from_iterable([[1, 2, 3], [4, 5, 6]]))
```

Funkce `chain.from_iterable()` je užitečná, když potřebujete vytvořit iterátor nad daty, která byla „roztrhaná“.

## Filtrování
Dalši užitečnou funkcí je filtrování. 
K tomu slouží funkce:
- V Pythonu 2.7: `ifilter()` a `ifilterfalse()`,
- ale v pythonu 3.7: `filterfalse()`
To funkce mají dva argumenty: f
- funkci, která vrací hodnotu `True` nebo `False` (nazývanou predikát) 
- a iterovatelné vstupy. 
Vrátí iterátor nad prvky vstupů, pro které predikát vrátí hodnotu `True`(`False`).

```python
only_positives = it.filterfalse(lambda x: x <= 0, [0, 1, -1, 2, -2])
list(only_positives)
```

Funkce `takewhile()` bere predikát a iterovatelné vstupy jako argumenty a vrátí iterátor nad vstupy, které se zastaví na první instanci prvku, pro který predikát vrátí hodnotu False:
```python
result = it.takewhile(lambda x: x < 3, [0, 1, 2, 3, 4])  # 0, 1, 2
list(result)
```

Funkce dropwhile () dělá přesně opak. 
Vrátí iterátor začínající prvním prvkem, pro který predikát vrátí hodnotu False:
```python
result = it.dropwhile(lambda x: x < 3, [0, 1, 2, 3, 4])  # 3, 4
list(result)
```

## Seskupení
Funkce `itertools.groupby()` umožňuje seskupení objektů v iterovatelné. 
To trvá iterable vstupy a klíč, a vrací objekt obsahovat iterators nad elementy vstupů seskupených podle klíče.

Zde je jednoduchý příklad skupiny ():
```python
data = [{'name': 'Alan', 'age': 34},
        {'name': 'Catherine', 'age': 34},
        {'name': 'Betsy', 'age': 29},
        {'name': 'David', 'age': 33}]

grouped_data = it.groupby(data, key=lambda x: x['age'])
for key, grp in grouped_data:
    print('{}: {}'.format(key, list(grp)))
```

Není-li zadán žádný klíč, `groupby()` nastaví seskupení podle „identity“ - to znamená, že agreguje identické prvky v iterovatelném:

```python
for key, grp in it.groupby([1, 1, 2, 2, 2, 3]):
    print('{}: {}'.format(key, list(grp)))
```

Pro učení, jaké funkce jsou k dispozici v modulu **itertools**, existuje několik vynikajících zdrojů. 
[Dokumentace](https://docs.python.org/3/library/itertools.html) sama o sobě jsou skvělým místem, kde pokračovat.
