# Moduly

Zatím jsme tvořili programy v Pythonu jako soubory do Jupyteru nebo skripy.
Velmi rycle, ale zjistíme, že některé funkce a objekty bychom potřebovali použít ve více notebocích/skripteck.

V tomto případě nám velmi dobře poslouží moduly.
V této lekci se podíváme jak na to.

Za příklad si vezmeme kód Ondřeje Caletky, který umožňuje určit české svátky
v zadaném roce. 

 * [oskar456/isholiday.py](https://gist.github.com/oskar456/e91ef3ff77476b0dbc4ac19875d0555e)

## Slovníček pojmů

Než se pustíme do samotného výkladu, zavedeme některé pojmy tak,
aby mezi nimi nedošlo v textu záměně.
Anglické pojmy v závorce jsou převzaty z oficiálního [glosáře](https://packaging.python.org/glossary).

* **(importovatelný) modul** (_Module_ ∪ _Import Package_) je cokoliv,
  co se dá importovat z Pythonu, v tomto textu tedy především Python soubor nebo adresář s nimi;
* **balíček** (_Distribution Package_) je instalovatelný archiv obsahují
  _importovatelné moduly_ pro Python a další potřebné soubory, může být i rozbalený;
* **zdrojový balíček** (_Source Distribution_, `sdsit`) je varianta zabaleného _balíčku_ ve zdrojové formě;
* **binární balíček** (_Binary Distribution_, `bdsit`) je varianta zabaleného _balíčku_ v nezdrojové (např. zkompilované) formě;
* **projekt** (_Project_) je knihovna, framework, skript, plugin, aplikace apod. (či jejich kombinace), které balíme do _balíčků_.

## Struktura modulu

Python jako modul považuje, mezi jinými, adresář obsahující soubor `__init__.py`
Tento adresář může také obahov další skripty a jiné datové soubory.

    .
    ├── kalendar
    │   ├── isholiday.py
    │   └── __init__.py
    ├── LICENSE
    ├── MANIFEST.in
    ├── README
    └── setup.py


Ve stejné urovni se pak můžeou nacházet další soubotry jako licence, readme nebo setup.py distribuci modulu přes pip.

> #### Příklad
> Vytovřte si adresář `kalendar` a do něj stáhněte soubor [oskar456/isholiday.py](https://gist.github.com/oskar456/e91ef3ff77476b0dbc4ac19875d0555e) a vytvořte v něm prázdný soubor `__init__.py`

### Soubor `__init__py`
V souboru `__init__.py` by tak prakticky žádný kód kromě importů být neměl.

Nadefunjte tydy import funkcí `getholidays`, `isholiday` v souboru `__init__.py`: 

    from .isholiday import getholidays, isholiday
    
    __all__ = ['getholidays', 'isholiday']

Tečka v příkazu `import` není chyba: je to zkratka pro aktuální modul.
Můžeme psát i `from isholiday.holidays import ...`, což ale trochu ztěžuje případné přejmenování modulu.

Ono `__all__` pak explicitně definuje rozhraní modulu. 
Například s původním modulem šlo provést `from isholiday import datetime`, ale asi by nikdo nečekal, že tahle možnost bude nutně zachována i v příštích verzích knihovny.
Seznamem `__all__` dáte najevo, že tyhle funkce nejsou jen „náhodné importy“, a zároveň tím zamezíte různým varováním o importovaném ale nevyužitém modulu, které může hlásit vaše IDE nebo linter.

* **Poznámka**: 
Python samotný pak `__all__` používá jako seznam proměnných importovaných přes `from isholiday import *`. 
Tento způsob importu nevidíme rádi, protože znepřehledňuje kód, to ale neznamená, že to musíme uživatelům naší knihovny znepříjemňovat (např. pro interaktivní režim).

### Použití modulu
Když máme modul nadefnovaný, můžeme jej teď jednduše načíst příkazem `import`.

```python
import kalendar

kalendar.getholidays(2019)
```
