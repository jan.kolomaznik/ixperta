# Řetězce: Formátování
Pokud potřebuje do řetězce vložit nějaké hodnoty, pak je lepší použít formátovací řetězce a nešítat je.

Existují dva možné způsoby:

## Pomocí `f"`
POkud před řetězec zapíšeme **f** a do řetězce název proměnné ve složených závorkách, pak nám Python řetězec automaticky naformátuje:
```python
p = 2
s = "Ahoj"
f"Rekni {p}-krat {s}."
```

### POmocí metody `format()`
Pokud do řetězce vložíme prázdné složené zavorky, můžeme po jeho definici na něm zavolat metodu `format`.
```python
"řetězec {} ze {} částí".format("složený", 3.14)
```

Tento přístup je mnohem flexibilnějsí něž předchozí.

Změna pořadí parametrů formátování
```python
'Cisla: {1} {0}'.format(1,2)
```

Pokud chceš nahradit hodnoty v jiném pořadí, nebo když chceš aby šablona
byla čitelnější, můžeš do „kudrnatých“ závorek napsat jména:

```python
vypis = 'Ahoj {jmeno}! Výsledek je {cislo}.'.format(cislo=7, jmeno='Elvíro')
print(vypis)
```

Formátování se používá skoro všude, kde je
potřeba „hezky“ vypsat nějakou hodnotu.
Formátování desetinných čísel
```python
'{:+.3f}'.format(42.42424242)
```

Zarovnání na deset znaků z leva
```python
'{:10}'.format('test')
```

Zarovnání na deset znaků z prava
```python
'{:>10}'.format('test')
```

Zarovnání na deset znaků na střed
```python
'{:^10}'.format('test')
```

Kombinace zarovnání a čísel
```python
print('{:.10}'.format('Dlouhý text \ text text'))
print('{:.2f}'.format(3.141592653589793))
print('{:9.2f}'.format(3.141592653589793))
print('{:09.2f}'.format(3.14159265))
```

### Speciální znaky pro textovou konzoli, pouze pro `print()`
Speciální znaky: uvozovky
```python
print(" ' ")
print(' " ')
print('Tohle je uvozovka \'. ')
```

Speciální znaky 2: Konce řádků
```python
print('První.\nDruhý.')
```

Speciální znaky 2: Tabulátory
```python
print("sloupec1\tsloupec2")
```

Text na více řádků
```python
print(""" Tohle je 
    řetězec,
    který
    má mnoho
    řádků. """)

```

Raw string
```python
print(r'C:\some\name')
```

> #### Příklad:
> Příkazem `print` vypište následující texty:
> - tohle je lomítko\nápoj vypadá jinak.
> - ' " ' "
> - Toto je moje exexexpřítelkyně.
```python
# Řešení
```


Vyhledávání pozice v řetězci
```python
slovo = 'Python'
slovo.find('ho')
```

> #### Příklad:
> Vypište na které pozici se vyskytuje slovo kůň ve větě 
> *Příšerně žluťoučký kůň úpěl ďábelské ódy.*
```python
# Řešení
```

Zpětným lomítkem se dají přidávat i exotické znaky, které nemáš na klávesnici.
Ty se dají zapsat jako `\N` a jméno znaku v složených („kudrnatých“) závorkách.
Třeba následující znaky.
(Do konzole na Windows bohužel nemusí jít všechny ypsat, ale aspoň první by jít měl):

```python
print('--\N{LATIN SMALL LETTER L WITH STROKE}--')
print('--\N{SECTION SIGN}--')
print('--\N{PER MILLE SIGN}--')
print('--\N{BLACK STAR}--')
print('--\N{SNOWMAN}--')
print('--\N{KATAKANA LETTER TU}--')
```
