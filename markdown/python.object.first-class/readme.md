# First-Class Objects

V Pythonu jsou funkce first-class objekty. 
To znamená, že funkce mohou být předávány a používány jako argumenty, stejně jako jakýkoli jiný objekt (řetězec, int, float, seznam atd.). 
Máme následující tři funkce:

```python
def say_hello(name):
    return "Hello {}".format(name)

def be_awesome(name):
    return "Yo {}, together we are the awesomest!".format(name)

def greet_bob(greeter_func):
    return greeter_func("Bob")
```

Tady `say_hello()` a `be_awesome()` jsou obyčejné funkce, které očekávají jméno zadané jako řetězec. 
Funkce `greet_bob()` však očekává funkci jako argument. 
Můžeme například předat funkci `say_hello()` nebo `be_awesome()`:

```python
greet_bob(say_hello)
```

**Jak je to možné, předávat funkci jako atribut?**

## Funkce jsou objekty

Všechna data v programu Python jsou reprezentována objekty nebo vztahy mezi objekty. 
Věci jako řetězce, seznamy, moduly a funkce jsou všechny objekty. 
Na funkcích v Pythonu není nic zvláštního.

Mějme funci `yell`:
```python
def yell(text):
    return text.upper() + '!'
```

Protože funkce `yell` je objekt v Pythonu, můžete jej přiřadit jiné proměnné, stejně jako jakýkoli jiný objekt:
```python
bark = yell
```

Tento řádek tuto funkci nezavolá. 
To trvá objekt funkce odkazoval `yell` a vytvoří druhé jméno ukazovat na to, `bark`. 
Nyní můžete také vykonat stejný základní objekt funkce voláním `bark`:
```python
bark('woof')
```

Funkční objekty a jejich názvy jsou dvě samostatné záležitosti. 
**Další důkazy**: Původní název funkce (`yell`) můžete odstranit. 
Protože další název (bark) stále ukazuje na základní funkci, můžete tuto funkci stále volat prostřednictvím ní:
```python
del yell
print(bark('hey'))
print(yell('hello?'))
```

A protože funkce je tedy objekt, může mít i atributy jako objekt.
Například Python připojuje identifikátor řetězce ke každé funkci v době vytvoření pro účely ladění. 
K tomuto internímu identifikátoru můžete přistupovat pomocí atributu __name__:
```python
bark.__name__
```

Atributr `__name__` vrací stále původní název funkce: „yell“, nemá vliv na to, jak k němu máte přístup z kódu. 
Tento identifikátor je pouze ladící pomůcka. 
Proměnná ukazující na funkci a funkci samotnou jsou dvě samostatné záležitosti.

Stejně tak metody, platí pro něto stejné, co platí pro funkce.

## Funkce v datových strukturách

Protože funkce jsou First-Class objekty, můžete je ukládat do datových struktur, stejně jako s jinými objekty. 
Můžete například přidat funkce do seznamu:
```python
funcs = [bark, str.lower, str.capitalize]
funcs
```

Přístup k objektům funkcí uloženým v seznamu funguje stejně jako u jiných objektů:
```python
for f in funcs:
    print(f, f('hey there'))
```

## Práce u kazateli na funkce

Protože funkce jsou objekty, můžete je předat jako argumenty dalším funkcím. 
Zde je funkce `greet`, která formátuje uvítací řetězec pomocí objektu funkce, který mu byl předán, a poté jej vytiskne:
```python
def greet(func):
    greeting = func('Hi, I am a Python program')
    print(greeting)
```

Výsledný pozdrav můžete ovlivnit přechodem do různých funkcí. 
Například předáním funkce `bark`:
```python
greet(bark)
```

Schopnost předávat objekty funkcí jako argumenty jiným funkcím je silná. 
To vám umožní abstractní chování programu.
V tomto příkladu zůstane funkce pozdravu stejná, ale můžete ovlivnit její výstup předáním různých formátovacích funkcí.

Funkce, které mohou přijímat jiné funkce jako argumenty, se také nazývají funkce **vyššího řádu**. 
Jsou nutností pro funkční programovací styl.

Klasickým příkladem funkcí vyššího řádu v Pythonu je vestavěná `map` funkce. 
Zde je návod, jak můžete formátovat posloupnost pozdravů najednou `map`:
```python
list(map(bark, ['hello', 'hey', 'hi']))
```

## Vnořené funkce
Python umožňuje definovat funkce uvnitř jiných funkcí. 
Často se nazývají vnořené funkce nebo vnitřní funkce. 
Zde je příklad:
```python
def speak(text):
    def whisper(t):
        return t.lower() + '...'
    return whisper(text)

speak('Hello, World')
```

Co se tady děje? Pokaždé, když zavoláte `speak`, definuje **novou vnitřní funkci** `whisper` a pak ji voláme.
Vnitřní funkce ale nelze volat mímo "hlavní" funkci (pokud ji sami nepředáme ven).
```python
whisper('Yo')
```

nebo
```python
speak.whisper
```

Ale co kdybyste chtěli získat přístup k funkci vnořeného šepotu zvenčí? 
Funkce jsou objekty - vnitřní funkci můžete vrátit volajícímu rodičovské funkce.

Zde je například funkce definující dvě vnitřní funkce. 
V závislosti na argumentu předaném funkci nejvyšší úrovně vybere a vrátí volajícímu jednu z vnitřních funkcí:
```python
def get_speak_func(volume):
    def whisper(text):
        return text.lower() + '...'
    def yell(text):
        return text.upper() + '!'
    if volume > 0.5:
        return yell
    else:
        return whisper
```

Všimněte si, jak `get_speak_func` ve skutečnosti nevolá jednu ze svých vnitřních funkcí - jednoduše vybere příslušnou funkci založenou na argumentu svazku a poté vrátí objekt funkce:
```python
get_speak_func(0.3)
```

```python
get_speak_func(0.7)
```

Samozřejmě můžete pokračovat a zavolat vrácenou funkci, buď přímo, nebo nejprve přiřazením názvu proměnné:
```python
speak_func = get_speak_func(0.7)
speak_func('Hello')
```

To znamená, že funkce mohou přijímat chování pouze prostřednictvím argumentů, ale mohou také vrátit chování. 
Jak je to cool?

## Funkce mohou zachytit místní stav
Nejen, že funkce mohou vrátit další funkce, tyto vnitřní funkce mohou také zachytit a nést některé z rodičovských funkcí s nimi.

Mějme například následující funkci:
```python
def make_adder(n):
    def add(x):
        return x + n
    return add

plus_3 = make_adder(3)
plus_5 = make_adder(5)
```

V tomto příkladu `make_adder` slouží jako továrna k vytvoření a konfiguraci funkcí „adder“. 
Všimněte si, jak mohou funkce „adder“ stále přistupovat k argumentu **n** funkce `make_adder`,
 
Potom mužeme tyto používat následovně:
```python
plus_3(4), plus_5(4)
```

## Objekty jako funkce (Functors)
Objekt není v Pythonu funkce. 

Mohou však být "volatelné jako funkce", což vám v mnoha případech umožňuje zacházet s nimi jako s funkcemi.

Pokud je objekt volatlený (**callable**), znamená to, že na něm můžete použít kulaté závorky a předat mu argumenty volání funkce. 

Zde je příklad volaného objektu:
```python
class Adder:
    def __init__(self, n):
         self.n = n
    def __call__(self, x):
        return self.n + x
```

a použití:
```python
plus_3 = Adder(3)
plus_3(4)
```

Na pozadí se „volání“ instance objektu realizuje voláním magické metody `__call__` objektu.

Samozřejmě, že ne všechny objekty budou volatlené. 
Proto je k dispozici vestavěná funkce, která umožňuje ověřit, zda se objekt jeví jako volaný nebo ne:
```python
print("plus_3: ", callable(plus_3))
print("bark:   ", callable(bark))
print("False:  ", callable(False))
```

### Kompexnější příklad
Problém je navrhnout třídu / metodu, která bude volat jinou metodu třídění založenou na typu vstupu. 
Pokud je vstup typu `int`, pak by měla být volána funkce `Mergesort` a pokud je vstup typu `float`, pak `Heapsort` jinak zavolá funkci `quicksortu`

```python
# Python code to illustrate program 
# without functors 
class GodClass(object): 
    def do_sort(self,x): 
        x_first=x[0] 
        if type(x_first) is int : 
            return self.__merge_sort(x) 
        if type(x_first) is float : 
            return self.__heap_sort(x) 
        else : 
            return self.__qick_sort(x) 
    def __merge_sort(self,a): 
        #" Dummy MergeSort " 
        print("Data is Merge sorted")
        return a 
    def __heap_sort(self,b): 
        # " Dummy HeapSort " 
        print("Data is Heap sorted")
        return b 
    def __qick_sort(self,c): 
        #  "Dummy QuickSort" 
        print("Data is Quick sorted")
        return c 
# Here the user code need to know about the conditions for calling different strategy 
# and making it tightly coupled code. 
      
godObject=GodClass() 
godObject.do_sort([1,2,3])
```

V tomto kódu existují určité zjevné mezery v návrhu
1. Vnitřní implementace by měla být skryta před uživatelským kódem, tj. Abstrakce by měla být zachována
2. Každá třída by měla zvládnout jednu odpovědnost / funkčnost.
3. Kód je pevně spojen.

#### Použití Functoru
```python

# Python code to illustrate program 
# using functors 
  
class Functor(object): 
    def __init__(self, n=10): 
        self.n = n 
             
    # This construct allows objects to be called as functions in python 
    def __call__(self, x) : 
        x_first=x[0] 
        if type(x_first) is int : 
            return self.__merge_sort(x) 
        if type(x_first) is float : 
            return self.__heap_sort(x) 
        else : 
            return self.__qick_sort(x) 
            
    def __merge_sort(self,a): 
        #" Dummy MergeSort " 
        print("Data is Merge sorted")
        return a 
    def __heap_sort(self,b): 
        # " Dummy HeapSort " 
        print("Data is Heap sorted")
        return b 
    def __qick_sort(self,c): 
        #  "Dummy QuickSort" 
        print("Data is Quick sorted")
        return c 
  
# Now let's code the class which will call the above functions. 
# Without the functor this class should know which specific function to be called  
# based on the type of input 
  
### USER CODE 
class Caller(object): 
    def __init__(self): 
        self.sort=Functor() 
      
    def do_sort(self,x): 
# Here it simply calls the function and doesn't need to care about 
# which sorting is used. It only knows that sorted output will be the  
# result of this call 
        return self.sort(x) 
  
Call=Caller() 
  
# Here passing different input 
print(Call.do_sort([5,4,6])) # Mergesort 
  
print(Call.do_sort([2.23,3.45,5.65])) # heapsort 
print(Call.do_sort(['a','s','b','q'])) # quick sort 
# creating word vocab 

```

Výše uvedený návrh usnadňuje změnu strategie nebo implementace bez narušení jakéhokoli uživatelského kódu.
Uživatelský kód může výše uvedený funktor spolehlivě používat, aniž by věděl, co se děje pod kapotou,
aby byl kód oddělen, snadno rozšiřitelný a udržovatelný.

Nyní, spolu s funkcemi v pythonu, jste také pochopili strategický vzor v Pythonu, který vyžaduje oddělení mezi třídou, která volá specifickou funkci a třídu, kde jsou strategie uvedeny nebo vybrány.

Hezký tutorál [tady](https://github.com/dbrattli/OSlash/wiki/Functors,-Applicatives,-And-Monads-In-Pictures)

## Shrnutí
- Vše v Pythonu je objekt, včetně funkcí. 
  Můžete je přiřadit proměnným, uložit je do datových struktur a předat nebo vrátit do az jiných funkcí (funkce první třídy).
- Prvotřídní funkce vám umožní abstractní chování ve vašich programech.
- Funkce mohou být vnořeny a mohou s nimi zachytit a nést některé z rodičovských funkcí. 
  Funkce, které to dělají, se nazývají uzávěry.
- Objekty mohou být callable, což vám umožní v mnoha případech s nimi zacházet jako s funkcemi.
