#!/usr/bin/env python

"""
Make sure our python interpretter is sane
>>> 2+5
7
"""
if __name__ == '__main__':
    import doctest
    doctest.testmod()