# Python jako testovací nástroj
Python lze použít jako šikovný testovací nástroj.

## Používání assertů ve vlastním kódu
Co tedy je test?

V podstatě se jedná o zápis nějakého předpokladu (anglicky assertion od slovesa assert – předpokládat) do kódu.
V Pythonu existuje přímo klíčové slovo `assert`, které dělá přesně to: kontroluje zda nějaký předpoklad platí a, pokud ne, vyhodí `AssertionError` výjimku. Zápis:

```python
assert expression1, expression2
```

je ekvivalentní zápisu:

```python
if __debug__:
    if not expression1: raise AssertionError(expression2)
```

kde `__debug__` je interní proměnná Pythonu, která je vždy pravdivá, pokud nebyl Python interpreter spuštěn s podporou optimalizací (-O).

Jak takové testy správně použít?

### Jak a kdy používat `assert`
V dokumentaci pythonu se píše:

> Python’s assert statement is a debugging aid, not a mechanism for handling run-time errors.
> The goal of using assertions is to let developers find the likely root cause of a bug more quickly.
> An assertion error should never be raised unless there’s a bug in your program.

Tedy cílem je informovat programátor o *krytických* chybách programu.
Není takto zpracovávat události jako je například chybějící soubor.

Narozdíl od jiných testovacích nástrojů `asserts` se řadí to takzvaného *white-box testování*, tedy testování zevnitř, kdy je kompletně znám stav testovaného objektu.
Jakmile je program odladěn, k těmto chybám by již něměl odocházet a toto testování tedy již není potřeba.

Assert by nám tedy měli usnadni odhalování chyb při vniku aplikace.

### Modelový příklad
Představte si že tvoříte online obchod.
Obchodník by chtěl umožnit svým zákazníkům uplatňovat různé slevy.

Vytvoříme tedy metodu, která tuto slevu vypočítá.

```python
def apply_discount(product, discount):
    price = int(product['price'] * (1.0 - discount))
    assert 0 <= price <= product['price']
    return price
```

Assert tesyjem že za všch možných podmínek bude výsledná cena v rozumných mezích.

###### Příklad validního použití

- Our example product: Nice shoes for \$149.00
- 25% off -> \$111.75
```python
shoes = {'name': 'Fancy Shoes', 'price': 14900}
apply_discount(shoes, 0.25)
```

###### Nevalidní použití
- A "200% off" discount:
```python
apply_discount(shoes, 2.0)
```

- A "25% off" discount:
```python
apply_discount(shoes, 25)
```
- A "-30% off" discount:
```python
apply_discount(shoes, -0.3)
```

### Úskalí v používání Asserts
Při použití assert mějte na paměti tyto dva problémy:

První je riziko zavedení bezpečnostních problémů a chyb do aplikace.
Druhé je syntaktický problém, který svádí k psaní `assert` v kódu, které se nikdy neuplatní.

#### 1. Bezpečnostní riziko a potenciální chyby
Je nutné mít neustále na paměti, že v  produkčním prostředí se `assert` globálně vypínají.
Takto se k `assets` chovají i jiné programovací jazyky, tak že se nejedná o specialitu jazyk Python.
Z toho důvodu se používábí `assert` jako rychlého a snadného způsobu validace dat stává velmi nebezpečné.

###### Anti Příklad:
```python
def delete_product(product_id, user):
    assert user.is_admin(), 'Must have admin privileges to delete'
    assert store.product_exists(product_id), 'Unknown product id'
    store.find_product(product_id).delete()
```

Co se stane když `assert` budou vypnutá?

V takovém případě je mnohem lepší použít `if` a `raise` raději než `assert`.
```python
def delete_product(product_id, user):
    if not user.is_admin():
        raise AuthError('Must have admin privileges to delete')

    if not store.product_exists(product_id):
        raise ValueError('Unknown product id')

    store.find_product(product_id).delete()
```

Tento přístup je mnohem bezpečnější a v kombinaci s vlastními výjimkami má i větší vypovídací hodnotu.

#### 2. Asserty, které nikdy nelžou
Diky suntaxi pythonu je snadné napsat `assert` špatně.
Stačí si splést `assert` klíčové slovo s `assert metodou.
Zvažte následující příklad:

```python
assert(1 == 2, 'This should fail')
```

Kulaté závorky zde reprezentují `tuple` a nikoliv argumenty metody!
Syntakticky je tedy takto zapsaný `assert` zcela zprávny.
Neprázdný `tuple(1 == 2, 'This should fail')` python vyhodnotí jako `true`


## Doctesty
Python přímo obsahuje i další způsoby testování kódu.
Doctesty jsou jedním z nich.

Jedná se asi nejsnazší způsob psaní testu, se kterým se v Pythonu můžeme setkat.

Doctest je v podstatě přepis session (dialogu) z interaktivního interpretu do dokumentačního řetězce modulu, třídy, metody nebo funkce.
Asi nejlépe je to pochopitelné na příkladu:

###### Soubor [simple_doctest.py](simple_doctest.py):

    """
    Make sure our python interpreter is sane
    >>> 2+5
    7
    """
    if __name__ == '__main__':
        import doctest
        doctest.testmod()

Výstup volání python python test.python.1.py by měl být prázdný a návratová hodnota by měla být 0.

```
!python simple_doctest.py
```

Při spuštění s parametrem -v dostaneme podrobnější informace o tom, co se testuje a s jakým výsledkem:
  
```
!python simple_doctest.py -v
```
  
Doctesty v dokumentačních řetězcích lze libovolně kombinovat s psanou dokumentací (ta musí být od doctestu oddělena prázdným řádkem).
To z nich činí výborný nástroj pro dokumentaci API, protože dávají uživateli jasnou představu, jaký kód má fungovat.
Fakt, že jsou současně spustitelné, nám zaručuje, že taková dokumentace bude velmi snadno udržovatelná v konzistentním stavu s kódem.

Rozsáhlejší příklad z reálného světa by mohl vypadat nějak takto:

###### Soubor [example_doctest.py](example_doctest.py):
```python
"""
This is the "example" module.

The example module supplies one function, factorial().  For example,

>>> factorial(5)
120
"""
def factorial(n):
    """Return the factorial of n, an exact integer >= 0.
 
    >>> [factorial(n) for n in range(6)]
    [1, 1, 2, 6, 24, 120]
    >>> factorial(30)
    265252859812191058636308480000000
    >>> factorial(-1)
    Traceback (most recent call last):
        ...
    ValueError: n must be >= 0
 
    Factorials of floats are OK, but the float must be an exact integer:
    >>> factorial(30.1)
    Traceback (most recent call last):
        ...
    ValueError: n must be exact integer
    >>> factorial(30.0)
    265252859812191058636308480000000
 
    It must also not be ridiculously large:
    >>> factorial(1e100)
    Traceback (most recent call last):
        ...
    OverflowError: n too large
    """
 
    import math
    if not n >= 0:
        raise ValueError("n must be >= 0")
    if math.floor(n) != n:
        raise ValueError("n must be exact integer")
    if n+1 == n:  # catch a value like 1e300
        raise OverflowError("n too large")
    result = 1
    factor = 2
    while factor <= n:
        result *= factor
        factor += 1
    return result

if __name__ == "__main__":
    import doctest
    doctest.testmod()
```

```
!python example_doctest.py -v
```

### Načtení ze souboru
Doctesty je také možné napsat v samostatném textovém souboru a načíst je spustit.

###### Soubor [exemaple_doctest.txt](static/example_doctest.txt )
    The ``Example`` module
    ======================
   
    Using ``factorial``
    -------------------
   
    This is an example text file in reStructuredText format.  First import
    ``factorial`` from the ``example`` module:
   
        >>> from example_doctest import factorial
   
    Now use it:
   
        >>> factorial(6)
        120
      
který můžeme spusti  zpříkazové řádky:
```
!python -m doctest -v static/example_doctest.txt
```

Nebo načíst a spustit pomocí kódu _(nefunkční v jupyteru)_:
```python
import doctest
doctest.testfile("example.txt")
```

Základem je tedy práce v konzoli:
> ##### Příklad:
> Použijte jupyter konzoli ...

### Kontext
Při psaní testů je důležitý kontext, ve kterém se testy provádějí.
Doctest provádí každý text v lokální mělké kopii `m` globálních proměnných.

Pomocí metody `testmod()` a parametru `globs=your_dict` je možné si tento kontext upravit.

### Testování výjimek
Doctect umožňuje i testování zda metoda vyhodí příslušnou výjimku

```python
[1, 2, 3].remove(42)
```

Pak do testů zapište jen první řádek s popisem výjimky:

    >>> [1, 2, 3].remove(42)
    Traceback (most recent call last):

### Nevýhody
Jakkoli jsou doctesty jednoduché na psaní a výborné pro dokumentaci, jejich využití na větší testy nebývá doporučováno – doctesty se obecně špatně udržují a obtížné ladí.

V našem příkladě, pokud bychom měli chybu v metodě `__init__` která by zapříčinila selhání první řádky testu, bychom dostali ještě čtyři nic neříkající chyby o tom že proměnná d není definována a určitě by chvíli trvalo, než bychom našli ten relevantní řádek testu, na kterém je skutečná chyba, a tím kus kódu kde chyba skutečně je.

Další velká výhoda doctestu je, že se spoléhají na textovou reprezentaci, která musí být stejná – tedy test:

    >>> s = u'Unicode string'
    >>> s
    'Unicode string'

vždy selže, jelikož se bude porovnávat přímo textová hodnota (s ‚u‘ na začátku), která nebude stejná.
Obdobný assert by ale prošel zcela bez problémů:

```python
assert u'Unicode string' == 'Unicode string'
```

Další problém s textovou reprezentací může být se slovníky, které nemají definováno pořadí klíčů (do verze 3.6).
To se tak může lišit (a liší) mezi jednotlivými implementacemi Pythonu.
To znamená, že test:

    >>> {'a': 1, 'b': 42, 42: 27}
    {'a': 1, 'b': 42, 42: 27}

může a nemusí projít (jedna z nejhorších možných vlastnosti pro test).

_Od verze 3.7 dál dist v Pythonu garantuje uchování pořadí klíčů podle pořadí vkládání._


[//]: <> (Unit test zmínit základy)

## Typová kontrola
Důležitou otázkou bezpečnosti a kvality kódu v Pythonu je typová kontrola.
Protože Python je dynamick typovaná jakyk, **musí** programátor sám vědět, jakého typu jsou proměnné, se kterými právě pracuje.

Situace se ale komplikuje tím, že spousta typů je kompatibilních a je tedy možné je v rámci metod použít.
Otázkou je, jak tedy typy testovat:

### Přístup k testování
První otázkou je se rozhodnout, jestli je testovat pomocí `if-raise` nebo pomocí `assert`.

> ##### Diskuze:
> Pojďme zvážit výhody a nevýhody každého přístupu.

### Semi Statické typování:
Prvním napál funkčním řešením je zavést semi statické typování do pythonu.
Nejlépe je vidět na příkladě:

```python
def fib(n):
    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a+b

list(fib('a'))
```

a semistatický příklad:
```python
def fib(n: int) -> Iterator[int]:
    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a+b
```

### Testování instance v kódu
Pokud bychom chtěli typ otestovat v kódu, pak můžeme použít if:

```python
def fib(n):
    if type(n) is not int:
        raise ValueError("n must be int")

    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a+b

list(fib('a'))
```

Pokud bychom ale chteli otestovat více přípustných typů, pak je toto nešikovný způsob, proto je lepší rovnou použít `isinstance`
```python
def fib(n):
    if not isinstance(n, (float, int)):
        raise ValueError("n must be number")
 
    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a+b

list(fib('a'))
```

[//]: <> (## Okrajové případy)

[//]: <> (Další technologie z Testing Your Code)

----
Zdroje:
- [The Python Standard Library: The assert statement](https://docs.python.org/3/reference/simple_stmts.html?highlight=assert#the-assert-statement)
- [The Python Standard Library: doctest](https://docs.python.org/3/library/doctest.html)
- [Testování v Pythonu](https://www.zdrojak.cz/clanky/testovani-v-pythonu/)
- [What Are Assertions & What Are They Good For?](https://dbader.org/blog/python-assert-tutorial)
- [Mupy.org](http://mypy-lang.org/)
- [Testing Your Code](https://docs.python-guide.org/writing/tests/)
