# Seznámení s jazykem Python

Python?
- Monty Python’s Flying Circus (BBC, 1970s) 

Následovník jazyka ABC
- 80’s
- pro odborníky, neprogramátory

![Guido van Rossum - 1991](media/guidovanrossum.jpg)

## Charakteristika

- snadný a intuitivní jazyk
- otevřená specifikace a implantace jazyka 
- kód snadno srozumitelný 
  - skoro jako běžná angličtina
- vhodný pro běžné každodenní úkoly
- umožňující vývoj v krátkém čase
- objektově orientovaný - vše je objekt
- bohatá standardní knihovna a množství knihoven 
- v současnosti dvě verze Python 2.x a 3.x 
  - (nekompatibilní) 
- dynamická typová kontrola
- automatická správa paměti
- podpora mnoha paradigmat
  - OOP
  - Funkcionální
  - Imperativní

## K čemu se python hodí?
Konzolové nástroje, nástroje pro správu systému
- skripty v OS, (yum/dnf - fedora)

Webové aplikace
- Aplikační rozhraní, serverové CGI skripty
- Django, web2py, Flask

**Machine learning**
- scikit, numpy,  etc.

GUI (Graphic User Interface)
- Trinker - nativní GUI na dané platformě
- wxPytohn - GUI API založeno na C++ knihovně. 
- Dabo -  vysokoúrovňová knihovna
- pyQT, pyGTK, pyWin32 

Datábáze a ORM
- Oracle, MySQL, PostgreSQL, SQLite
- Pickle - ukládání objektů do souborů

Integrace komponent
- Propojení správa částí implementovaných v jiných jazycích
- Cython - C/C++ kód a knihovy

Komunikace
- komunikace skrze FTP, Telnet, 
- zpracování XML, JSON

### Kdo python používá?
Google
- Youtube
- Google App Engine

Dropbox
- Klientská i serverová část

Netflix
- backend 

NASA
- Python on Mars

Reddit
1
*[Python success](http://www.python.org/about/success)*

## Instalace

### 1. Python
Python je součástí distribuce Linux systému

Do Windows je nutné doinstalovat interpret https://www.python.org/downloads/

### 2. Virtuální prostředí (venv)
Pokud vyvýjíte více projektů můžou nastávat problémy s kolizemi knihoven, ...

Proto je dobré mít pro každý projekt samostatné prostředí.

Dvě cesty:

1. IDE se o to postará, například [PyCharm](https://www.jetbrains.com/pycharm/)
2. Použít externí nástroj [Anaconda](https://www.anaconda.com/)

## Možnosti vývoje
V pythonu lze programovat mnoha způsoby, zde jsou základní tři:

### 1. Python interaktivní konsole
Pro rychlé prototypování lze spustit konzoli, do které je možné ihned zadávat příkazy.

Automaticky vypisuje výsledky

Spustíte v příkazovém řádku příkazem `python` nebo pomocí webové služby.
- Například [https://repl.it/languages/python3](https://repl.it/languages/python3) 

V Linuxu je možné použít script
```bash
!/usr/bin/env python
-*- coding: utf-8 -*-
```

### 2. Python script/zdrojový kód
Soubor zdrojových kódů uložených v v souboru s koncovkou `.py`.

Základní způsob pokud je vaším cílem vytvořit aplikaci

Je vhodné používat IDE a virtuální environment

Možnost řešit závislosti pomocí souboru `requirements.txt`

### 3. IPtyhon/Jupyter
Rozšiřující knihovna pro Python, součástí některých distribucí [Anaconda](https://www.anaconda.com/)
- příkaz `anaconda-navigator`

Pro vývoj je možné použít webovou konzoli, která je integrovanou součástí prostředí 
- Hlavní benefit v grafických výstupech
- Možnost sdílení scriptů
- Dokumentace přímo ve scriptu ve formátu **Markdown**.

Vhodné pro zpracování dat, vědecké výpočty, výuku ...

#### Instalace a spuštšní
1. Nastalovat [Python3](https://www.python.org/downloads/)
2. Spustit instal script 
```bash
pip install jupyter
```
3. Spuštení příkazem `jupyter notebook`

#### Ovládaní

- **ENTER**: Vstup do ediačního modu buňku
- **CTRL + ENTER**: Spuštení kódu v buňce
- Dva druhy buněk:
  1. S Python kodem: 
     - začnají `In[]`
  2. S **Markdown** textem
     - Syntaxe:  https://www.markdownguide.org/cheat-sheet/
     
