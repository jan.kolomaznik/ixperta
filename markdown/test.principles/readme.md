# Psaní testovatelného kódu

Import potřebných závislostí:
```python
#pip install pytest ipytest
import pytest
import ipytest
ipytest.autoconfig()
```

## Funkce bez vedlejších efektů
Psaní testů, zvláště tech v OOP programování není tak jednoduché, jako napsat test na funkci.

Spuštění části kódu často mohou mít často dopad na systém nebo na operační prostředí,
například:
- může změnit atributy třídy
- vytvořit soubor v systému
- uložit fotku do databáze.
- atd...

Těmto dopadům se obecně říká *vedlejší efekty* a pro testy přináší tři uskali:
1. Musíme me rovněž testovat
2. Musíme před a po nastavení spuštění testů vytvořit prostředí pro jejich testování
3. Musíme je po ukončení testu uklidit.

Obecně se ale dá říci, že je často jedná o nepříjemnou komplikací, která komplikuje testování a hlavně **je to častý zdroj chyb**.

Pokud v rámci psaní testů narazíte na metody/funkce které mají velké množství vedlejší efektů, a často bývá lepší řešením kód refaktorovat a tyto vedlejší efekty odstranit, nebo alespoň rozdělit do dobře definovaných celků, než vymýšlet komplikované test.

Dodržování tohoto principu, který se obesně nazvá [principu jediné odpovědnosti](https://cs.wikipedia.org/wiki/Princip_jedn%C3%A9_odpov%C4%9Bdnosti) je skvělý způsob, jak navrhnout kód, pro který je snadné psát opakovatelné a jednoduché testy jednotek a nakonec i spolehlivé aplikace.

![](media/side-effects.gif)

Existuje několik dalších přístupů, pro testování částí kódu, s množství vedlejších efektu:

technik, které můžete použít k testování částí aplikace, které mají mnoho vedlejších účinků:

1. **Refaktorovat kód**: tuto přístup jsem již zmínil výše, je ale natolik dobrý, že ho tu raději uvádí ještě jednou
2. **Mockování**: Jedná se o techniku, kde nahradíme vazby v testovaném objektu za "náhražky".
3. **Jasně definovat integrační a jednicové testy**: Jasné rozdělení techto dvou typů testů usnadňuje orientaci a zvyšuje přehlednost.

## Objektově orientované programování
Objektově orientované programování je typické množstvím vazeb a závislostí mezi mezi třídami.

Pro dobře testovatelný kód jsou důležité dodržovat některá pravidla:

1. Minimalizovat vazby a když už, tak je mít co nejlepší.
2. Rozdělit testy:
  - Unit test by měli testovat řády nezávisle na ostatních.
  - Integrační testy pak tují vazby mezi třídami.

[//]: <> (Zpracovat příklad na girearchii vazev)
```todo
Vazby podle síly
associace(use)B -use-> A In [3]:  class A:
    
    def foo(self):
        pass  In [4]:  class B:
    
    def boo(self, a):
        a.foo()  a = A()
b = B()


b.boo(a) # <- tady zniká a zanika vazba  silnějčí je Agregace:B o-- A (B ma agregační vazbu na A) class A:    
    def foo(self):
        pass  In [5]:  class B:

    def __init__(self, a):
        self.__a = a
    
    def boo(self):
        __a.foo()  a = A()
b = B(a) # <- tady zniká vazba

b.boo()  Silnější je kompoziceB *-- A (B má kompoziční vazu na a) In [6]:  class A:    
    def foo(self):
        pass  class B:

    def __init__(self):
        self._a = A()
    
    def boo(self):
        a.foo()
          b = B() # <- tady zniká vazba, ale magicky

b.boo()  Varianta 1: mohu použít monkypatch ale musím zístak instanci In [9]:  monkeypatch.setattr(B._a, "foo", None)  Varianta 2: mohu použít mock b._a = mock  varianta 1+2: In [10]:  class Mock_A:
    pass

monkeypatch.setattr(A, "__new__", Mock_A.__new__)  Děničnost:B --|> A (B je potomkem A)  In [11]:  class A:    
    def foo(self):
        pass  class B(A):
    
    def boo(self):
        b.foo()
```

### Závislosti mezi instancemi
Pro řešení závislostí mezi objekty v testech můžeme v úspěchem použít `fixture`, o kterých jsme se bavili v předchozích tématech.

POkud tedy máme dobře definovanou třídu, která má všechny své závislosti dobře definované a nastavitelné například pomocí konstruktoru, pak je toto nejsnazší cestou.
V takových případech s v testovacích frameworcích použíjí [mocky](https://en.wikipedia.org/wiki/Mock_object) nebo [stuby](https://en.wikipedia.org/wiki/Method_stub).

Pytest má v sobě integoravnou knihovnu Monkeypatch, která vše řeší trošku jinak:

### Monkey Path
Tatento nástroj řeší fituaci, kdu poteřehuje v testech volat funkčnost, která závisí na globálním nastavení nebo vyvolá kód který nechceme v testech volat, například přístup k síti.

Monkeypatch `fixture` umožňuje překrýt tyto metody a zamezit jejich volání.
Takto můžeme zabezpečit modifikaci s dat, praci s os, databází, ...

Monkeypatch `fixture` poskytuje metody úpravu takových funkcí a metod pro testovací účely:

    monkeypatch.setattr(obj, name, value, raising=True)
    monkeypatch.delattr(obj, name, raising=True)
    monkeypatch.setitem(mapping, name, value)
    monkeypatch.delitem(obj, name, raising=True)
    monkeypatch.setenv(name, value, prepend=False)
    monkeypatch.delenv(name, raising=True)
    monkeypatch.syspath_prepend(path)
    monkeypatch.chdir(path)


Všechny modifikace pomocí MonkeyPatch budou o ukončení testovací funkce odstraněny.
Parameter `raising=True` znamená, že monkypach vyhodí výjimku `KeyError` nebo `AttributeError` pokud se snažíme pracovat s metodami nebo atributy, které objekt nemá.

V rámci dokumentace je uveden [scénář použítí](https://docs.pytest.org/en/stable/monkeypatch.html).

#### Jednoduché příklady použití:
```python
%%run_pytest[clean]

from pathlib import Path

def getssh():
    """Simple function to return expanded homedir ssh path."""
    return Path.home() / ".ssh"

def test_getssh(monkeypatch):
    # mocked return function to replace Path.home
    # always return '/abc'
    def mockreturn():
        return Path("/abc")

    # Application of the monkeypatch to replace Path.home
    # with the behavior of mockreturn defined above.
    monkeypatch.setattr(Path, "home", mockreturn)

    # Calling getssh() will use mockreturn in place of Path.home
    # for this test with the monkeypatch.
    x = getssh()
    assert x == Path("/abc/.ssh")
```

Nebo jiný trochu podobný příklad, kdy definujeme mock mimo testovací funkci:
```python
import os

def current_directory():
    """
    Retrieve the current directory

    Returns: Current directory
    """
    return os.getcwd()
```

a Test k této funkci:
```python
%%run_pytest[clean]
  
def mock_getcwd():
    return '/example/1'

def test_current_directory(monkeypatch):
    """
    GIVEN a monkeypatched version of os.getcwd()
    WHEN current_directory() is called
    THEN check the current directory returned
    """
    monkeypatch.setattr(os, 'getcwd', mock_getcwd)
    assert current_directory() == '/example/1'
```

A třetí způsob abstrakce, kdy mock definujeme ve `fixture`:
```python
%%run_pytest[clean]
  
def mock_getcwd():
    return '/example/2'
  
@pytest.fixture
def patch_getcwd(monkeypatch):
    monkeypatch.setattr(os, 'getcwd', mock_getcwd)

def test_current_directory(patch_getcwd):
    """
    GIVEN a monkeypatched version of os.getcwd() in fixture patch_getcwd
    WHEN current_directory() is called
    THEN check the current directory returned
    """
    assert current_directory() == '/example/2'
```

#### Použití Monkey Patching pro vytváření mock objektů
Další mpřípakladem v dokumentaci je použití tohoto nástroje pro jednoduché mockování.

V tomto příkladu se používá metoda `monkeypatch.setattr`.
Cílem je vrátit z funkce objekt a ne hodnotu.

Mějme jednoducjé api pro práci s [githubem](https://github.com/):
```python
import requests

def get_user_followers(username):
    """Grab the JSON object from a given user's followers."""
    response = requests.get('https://api.github.com/users/{}/followers'.format(username))
    return response.json()

def get_follower_names(username):
    """Given a username of a GitHub user, return a list of follower usernames."""
    json_followers = get_user_followers(username)
    return [follower['login'] for follower in json_followers]
```

Můžeme si ověřit funkčnost:
```python
get_follower_names('Kolomaznik')
```

a pokud budeme vše zapsat do testu pak první nástřel může vypadat následovně:
```python
%%run_pytest[clean]

def test_get_follower_names_returns_name_list():
    assert 'xfoldvar' in get_follower_names('Kolomaznik')
```

Nyní si budeme chtít namalovat, aby se neptal reálného serveru.
Konkrétně budeme chtít upravit `requests` tak, aby vrátil objekt s chozími daty na kterém se dá volat metoda `.json()`.

```python
%%run_pytest[clean]

class MockResponse:

    # mock json() method always returns a specific testing dictionary
    @staticmethod
    def json():
        return [{"login": "foldvar"}]
  
def test_get_json(monkeypatch):

    # Any arguments may be passed and mock_get() will always return our
    # mocked object, which only has the .json() method.
    def mock_get(*args, **kwargs):
        return MockResponse()

    # apply the monkeypatch for requests.get to mock_get
    monkeypatch.setattr(requests, "get", mock_get)

  
    assert 'folder' in get_follower_names('Kolomaznik')
```

A ještě lépe, falešný `requests` lze navrhnout tak, aby byl použit pro všechny testy pomocí `fixture`, která se bude nacházet conftest.py a použít nastavení `autouse=True`.


### Plugin pytest-mock
Pytest nemá mocking přímo v sobě integrován, je proto nutné použít vhodný plugin.

```shell script
!pip install pytest-mock
```

#### Mocker
Tento plugin poskytuje `mocker fixture`, které jsou tenkou obálkou kolem opravného API poskytovaného mock balíčkem:

```python
%%run_pytest[clean]

import os

class UnixFS:

    @staticmethod
    def rm(filename):
        os.remove(filename)

def test_unix_fs(mocker):
    mocker.patch('os.remove')
    UnixFS.rm('file')
    os.remove.assert_called_once_with('file')
```

#### Spy
Objekt `mocker.spy` funguje ve všech případech přesně jako původní metoda, kromě toho, že `spy` také sleduje volání metod, návratové hodnoty a vyvolané výjimky.

```python
%%run_pytest[clean]

class Foo(object):
    def bar(self, v):
        return v * 2

def test_spy(mocker):
    foo = Foo()
    spy = mocker.spy(foo, 'bar')
    assert foo.bar(21) == 42

    spy.assert_called_once_with(21)
    assert spy.spy_return == 42
```

Objekt vrácený `mocker.spy` je objekt `MagicMock`, takže jsou k dispozici všechny standardní kontrolní funkce (jako `assert_called_once_with` ve výše uvedeném příkladu).

`mocker.spy` navíc nabízejí:
- `spy_return`: obsahuje vrácenou hodnotu sledované funkce.
- `spy_exception`: obsahuje poslední hodnotu výjimky vyvolanou sledovanou funkcí / metodou při posledním volání, nebo None, pokud nebyla vyvolána žádná výjimka.

#### Stub
`Stub` je simulovaný objekt, který přijímá jakékoli argumenty a je užitečný k testování zpětných volání. 

```python
%%run_pytest[clean]

def foo(on_something):
    on_something('foo', 'bar')


def test_stub(mocker):
    stub = mocker.stub(name='on_something_stub')

    foo(stub)
    stub.assert_called_once_with('foo', 'bar')
```

#### Použití decorator @mock.patch
Plugin také definuje dekorátor, který ulehčuje práci s definováním mocku:
POkud potřebuje definovat více mocku na sobě závislých, pak mohou vnikat nepřehledné a vnořené `with` struktury:

```python
%%run_pytest[clean]

import mock

def test_unix_fs():
    with mock.patch('os.remove'):
        UnixFS.rm('file')
        os.remove.assert_called_once_with('file')

        with mock.patch('os.listdir'):
            assert UnixFS.ls('dir') == expected
            # ...

    with mock.patch('shutil.copy'):
        UnixFS.cp('src', 'dst')
        # ...
```

Díky dekorátoru `@mock.path` pro zpřehlednění testů:

```python
%%run_pytest[clean]

@mock.patch('os.remove')
@mock.patch('os.listdir')
@mock.patch('shutil.copy')
def test_unix_fs(mocked_copy, mocked_listdir, mocked_remove):
    UnixFS.rm('file')
    os.remove.assert_called_once_with('file')

    assert UnixFS.ls('dir') == expected
    # ...

    UnixFS.cp('src', 'dst')
    # ...
```


## OOP best practices
Následující části se podíváme na 5 osvědčených konceptů. Které Vám pomohou vytvořit kvalitní a užitečné testy:

#### 1. Použijte mocker misto mock
Je lepší použít mocker `fixture` namísto používání moku přímo.
Proč:
- Jasně definuje platnost a rozsah existence mocku.
- Potřebujete jméně kódu
- Možnost pracovat s parametrizovaný testy

Zvažte následují příklad:
```python
%%run_pytest[clean]

try:
    import mock  # fails on Python 3
except ImportError:
    from unittest import mock


def first_test_fn():
    return 42


def another_test_fn():
    return 42


class TestManualMocking(object):
    """This is dangerous because we could forget to call ``stop``,
    or the test could error out; both would leak state across tests
    """

    @pytest.mark.xfail(strict=True, msg="We want this test to fail.")
    def test_manual(self):
        patcher = mock.patch("mocker_over_mock.first_test_fn", return_value=84)
        patcher.start()
        assert first_test_fn() == 42
        assert False
        patcher.stop()

    def test_manual_follow_up(self):
        assert first_test_fn() == 42, "Looks like someone leaked state!"


class TestDecoratorMocking(object):
    """This is better, but:
        1. Confusing when we start layering ``pytest`` decorators like
        ``@pytest.mark`` with ``@mock.patch``.
        2. Doesn't work when used with fixtures.
        3. Forces you to accept `mock_fn` as an argument even when the
        mock is just set up and never used in your test - more boilerplate.
    """

    @pytest.mark.xfail(strict=True, msg="We want this test to fail.")
    @mock.patch("mocker_over_mock.another_test_fn", return_value=84)
    def test_decorator(self, mock_fn):
        assert another_test_fn() == 84
        assert False

    def test_decorator_follow_up(self):
        assert another_test_fn() == 42

    @pytest.fixture
    @mock.patch("mocker_over_mock.another_test_fn", return_value=84)
    def mock_fn(self, _):
        return

    def test_decorator_with_fixture(self, mock_fn):
        assert another_test_fn() == 84, "@mock and fixtures don't mix!"


class TestMockerFixture(object):
    """This is best; the mocker fixture reduces boilerplate and
    stays out of the declarative pytest syntax.
    """

    @pytest.mark.xfail(strict=True, msg="We want this test to fail.")
    def test_mocker(self, mocker):
        mocker.patch("mocker_over_mock.another_test_fn", return_value=84)
        assert another_test_fn() == 84
        assert False

    def test_mocker_follow_up(self):
        assert another_test_fn() == 42    

    @pytest.fixture
    def mock_fn(self, mocker):
        return mocker.patch("mocker_over_mock.test_basic.another_test_fn", return_value=84)

    def test_mocker_with_fixture(self, mock_fn):
        assert another_test_fn() == 84

```
     
#### 2. Parametrizujte stejné chování, proveďte různé testy pro různé chování
Parametrizujte při prosazování stejného chování s různými vstupy a očekávanými výstupy. 
Proveďte samostatné testy odlišného chování. 
K popisu jednotlivých testovacích případů použijte samostatné test třídy.

Proč:
- Kopírování stejného kódu v různých testech znesnadňuje udržování testů.
- Nikdy neprovádějte smyčky v rámci kterých testujete.
  Selhání testu v jednom průchodu přeruší všechny ostatní testy.
- Pokud by parametrizace vedla ke komplexním funkcím s mnoha argumenty, pak je lepší napsat více samostatných trestů.
  Složité testy je obtížné udržovat a to může to vést k chybám.

```python
%%run_pytest[clean]

def divide(a, b):
    return a / b


@pytest.mark.parametrize("a, b, expected, is_error", [
    (1, 1, 1, False),
    (42, 1, 42, False),
    (84, 2, 42, False),
    (42, "b", TypeError, True),
    ("a", 42, TypeError, True),
    (42, 0, ZeroDivisionError, True),
])
def test_divide_antipattern(a, b, expected, is_error):
    if is_error:
        with pytest.raises(expected):
            divide(a, b)
    else:
        assert divide(a, b) == expected


@pytest.mark.parametrize("a, b, expected", [
    (1, 1, 1),
    (42, 1, 42),
    (84, 2, 42),
])
def test_divide_ok(a, b, expected):
    assert divide(a, b) == expected


@pytest.mark.parametrize("a, b, expected", [
    (42, "b", TypeError),
    ("a", 42, TypeError),
    (42, 0, ZeroDivisionError),
])
def test_divide_error(a, b, expected):
    with pytest.raises(expected):
        divide(a, b)
```

#### 3. Neupravujte hodnoty `fixture` v jiných `fixture`.
`Fixture` mohou užívat pro svou definici jiné `fixture` a stavět na nich svoje data. 
Nikdy ale ne-modifikujte data vstupní `fixture`, raději vytvářejte hluboké kopie.

Proč: 
- U testu se každá fixture provádí pouze jednou. 
  Více `fixture` však může závisled na jednom společném `fixture`, pokud některý z nich upraví hodnotu společného `fixture, pak všechny ostatní také uvidí upravenou hodnotu.
  To téměř jistě povede k neočekávanému chování.

```python
%%run_pytest[clean]

from copy import deepcopy

@pytest.fixture
def alex():
    return {
        "name": "Alex",
        "team": "Green",
    }


@pytest.fixture
def bala(alex):
    alex["name"] = "Bala"
    return alex


@pytest.fixture
def carlos(alex):
    _carlos = deepcopy(alex)
    _carlos["name"] = "Carlos"
    return _carlos


def test_antipattern(alex, bala):
    assert alex == {"name": "Alex", "team": "Green"}
    assert bala == {"name": "Bala", "team": "Green"}


def test_pattern(alex, carlos):
    assert alex == {"name": "Alex", "team": "Green"}
    assert carlos == {"name": "Carlos", "team": "Green"}
```

#### 4. Raději zpracujte s thhp responses před mockováním http requests.
Nikdy ručně nevytvářejte vlastní http response pro testy.
Pro tyto učely exisuje výborná knohvna [`responses`](https://github.com/getsentry/responses). kterou použijte k definici očekávané odpovědi ze skutečného API.

Proč: 
- Při integraci API REST služby již většinou máme k dispozici ukázkové odpovědi.
  Tento způsob je vývojářům dobře anámí a jsme zvyklí s ním pracovat.

```shell script
!pip install responses
```

```python
%%run_pytest[clean]

import responses
import requests

@responses.activate
def test_simple():
    responses.add(responses.GET, 'http://twitter.com/api/1/foobar',
                  json={'error': 'not found'}, status=404)

    resp = requests.get('http://twitter.com/api/1/foobar')

    assert resp.json() == {"error": "not found"}

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == 'http://twitter.com/api/1/foobar'
    assert responses.calls[0].response.text == '{"error": "not found"}'
```

#### 5. Upřednostňujte tmpdir před globálními testovacími artefakty
Nevytvářejte soubory v globálním adresáři testů / artefaktů pro každý test, který vyžaduje rozhraní systému souborů.
Místo toho použijte `fixture` tmpdir k vytváření souborů za běhu k práci s nimi.

Proč: 
- Globální artefakty jsou odstraněny z testů, které je používají, což ztěžuje jejich údržbu. 
- Jsou také statické a nemohou využívat přípravky a další skvělé techniky. 
- Vytváření souborů z dat pomocí `fixture` těsně před spuštěním testu je bezpečné a snadno udržovatelné.

```python
%%run_pytest[clean]

def process_file(fp):
    """Toy function that returns an array of line lengths."""
    return [len(l.strip()) for l in fp.readlines()]


@pytest.mark.parametrize("filename, expected", [
    ("first.txt", [3, 3, 3]),
    ("second.txt", [5, 5]),
])
def test_antipattern(filename, expected):
    with open("resources/" + filename) as fp:
        assert process_file(fp) == expected


@pytest.mark.parametrize("contents, expected", [
    ("foo\nbar\nbaz", [3, 3, 3]),
    ("hello\nworld", [5, 5]),
])
def test_pattern(tmpdir, contents, expected):
    tmp_file = tmpdir.join("testfile.txt")
    tmp_file.write(contents)
    with tmp_file.open() as fp:
        assert process_file(fp) == expected

```

----
Zdroje:
- [Getting Started With Testing in Python](https://realpython.com/python-testing/)
- [Monkeypatching](https://docs.pytest.org/en/stable/monkeypatch.html)
- [Pytest-mock](https://pypi.org/project/pytest-mock/)
- [5 Pytest Best Practices for Writing Great Python Tests](https://www.nerdwallet.com/blog/engineering/5-pytest-best-practices/)
- [Plugin pytest-mock](https://pypi.org/project/pytest-mock/)


