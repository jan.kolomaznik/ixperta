# Základy testování

Žádný programátor není dokonalý a každý dělá chyby.

> #### Śpatný nápad
> Nejpoužívanější přístup je asi metoda „zkusím to a uvidím“ – napíšu nějaký kód, spustím ho a pokud se dostaví očekávaný výsledek, považují kód za správný.
> Testování tedy není „něco navíc“, co běžní programátoři nedělají, je to něco, co dělá každý – jen někteří (ti méně líní) to dělají ručně a opakovaně (dokud daný kód nefunguje). Skutečně líný programátor ale nejradši napíše test, aby tuto opakovanou práci za něj vykonával nějaký kus kódu – může to být (ale málokdy bývá) náročnější než otestovat danou věc ručně, ale je to něco, co autor kódu udělá jednou a pak to za něj vykonává automat – ve výsledku tedy výrazná časová úspora.

## Kvalita software
Testování se určitě řadí mezi best practices.

Testování je velmi důležité a v určité části projektu dokonce nepostradatelné.

### Rozšiřování softwaru
Jakékoli rozšiřování softwaru, pokud jej děláte kvalitně, vždy znamená změnu stávajících funkčností.

Do jisté míry může kvalitní analýza a návrh připravit půdu pro budoucí úpravy, ale i když se budete hodně snažit, trh se mění nekontrolovaně a úspěšnou aplikaci bude třeba upravovat ze všech stran.

Pokud stávající funkce nebudete upravovat, začne vám vznikat redundantní kód.

Asi víte, že vyhýbat se úpravě stávajícího kódu aplikace se vůbec nevyplatí, trpěl by tím její návrh a do budoucna by bylo velmi těžké takový slepenec nějak upravovat nebo rozšiřovat, když byste museli změny provádět na několika místech, bloky kódu by se opakovaly a podobně.

Došli jsme tedy k tomu, že svou aplikaci budete stále měnit a přepisovat, takto vývoj softwaru prostě vypadá.

A kdo potom otestuje, že vše funguje? Člověk?

Když jsme došli k tomu, že musíme testovat, zda se předchozí funkčnost novou úpravou nerozbila, povězme si proč nemůže testování provádět člověk.

### Proč to nemůže dělat člověk?

Zamysleme se nad naivní představou.

#### Očekávání
Programátor nebo tester si sedne k počítači a prokliků jednu službu za druhou, jestli fungují.

#### Realita
Představte si, že takto testujete aplikaci, jste někde v polovině testovacího scénáře a naleznete chybu.
- Nejde napsat komentář k článku
- Chybu opravíte, úspěšně pokračuje až do konce scénáře.
- Otestovanou aplikaci nasadíte na produkční server.

#### Co se stane?
- další den vám přijde email, že nejdou kupovat články.
 Po chvíli zkoumání zjistíte, že oprava, kterou jste provedli, rozbila jinou funkčnost, kterou jste již otestovali předtím.

### Testy se musí provádět automaticky
Pokud se během testování provede oprava, musíte celý scénář provést od začátku!

A právě proto musí testy provádět stroj, který celou aplikaci prokliká obvykle maximálně za desítky minut a může to dělat znovu a znovu a znovu.

Proto píšeme automatické testy, toto vám bohužel většina návodů na internetu neřekne.

## Granularita testů
Zaměřme se nyní na to, co na aplikaci testujeme.

Typů testů je hned několik, obvykle nepokrývají úplně všechny možné scénáře (všechen kód), ale hovoříme o code coverage.

![](media/testovani_v_model.png)

[//]: <> (Doplnit urověn pro navrh a testovaní gui)

### Jednotkové testy (Unit testy)
Obvykle testují nejmenší jednotku kódu: objekt/metodu

Testujeme izolovaně, rychle, opakovatelně.

[//]: <> (Vytvetli skatku T.I.M.E.L.Y)

### Integrační testy
Právě integrační testy dohlíží na to, aby do sebe vše správně zapadalo.

### Akceptační testy
Tento typ testů je naopak úplně odstínění od toho, jak je aplikace uvnitř naprogramovaná.

Testují funkcionalitu celé aplikace pode use Case.

Tyto testy obvykle využívají framework Selenium, který umožňuje automaticky klikat ve webovém prohlížeči a simulovat internetového uživatele, co vaši aplikaci používá.

### Systémové testy
I když aplikace funguje dobře, na produkčním prostředí podléhá dalším vlivům, se kterými musíme rovněž počítat, např. že by měla zvládat obsluhovat tisíc uživatelů v jeden okamžik.
To bychom provedli zátěžovým testem, který spadá mezi systémové testy.

A mnohé další...

### Pyramida testů
![](media/test-pyramid.jpeg)

[//]: <> (## Rozsah testování)

## Testování a automatizace
Důležitým prvkem je automatizace spouštění testů a jejich začlenění do vývoje.
Automatické testy se dají spouštět:

1. __Ručně__: tedy na žádost programatátora
2. __Při push__: tedy když práci odesíláme do repozitáře (Git)
3. _Při překladu_: zde pouze pro pořádek, python nemá překlad ve smyslu jiných jazyků.
  * [Gitlab](https://docs.gitlab.com/ee/ci/examples/test-and-deploy-python-application-to-heroku.html)
4. __Při změne kódu__: vždy když se soubor uloží.
  * Různé nástoje: [pytest-xdist](https://pypi.org/project/pytest-xdist/)

### GitHub + Travis CI
Vaše testy nemusí běžet jen u vás na počítači, ale můžete je pouštět automaticky na službě Travis CI při každém pushnutí na GitHub.

Travis CI je zadarmo pro veřejné repozitáře na [travis-ci.org](https://travis-ci.org), pro soukromé repozitáře je placená verze na [travis-ci.com](https://travis-ci.com).
V rámci studentského balíčku můžete i tuto verzi využít zdarma.

Přihlaste se na [travis-ci.com](https://travis-ci.com) pomocí GitHubu (vpravo nahoře).
Pak opět vpravo nahoře zvolte Accounts a povolte Travis pro váš repozitář.

Do repozitáře přidejte soubor `.travis.yml`:

```yaml
language: python
python:
- '3.6'
- '3.7'
dist: xenial
install:
- python setup.py install
script:
- python setup.py test
```

[//]: <> (Zařadi příklad na code coverige)

[//]: <> (Integrace s gitlabem)

----
Zdroje:
- [Testování v Pythonu](https://www.zdrojak.cz/clanky/testovani-v-pythonu/)
- [naucse.python.cz](https://github.com/cvut/naucse.python.cz/blob/b181/lessons/beginners/testing/index.md)

