# Magické metody

Metody pojmenované `__nazev__` mají nějakou speciální funkci. 
[Perfektní přehled těchto metod](http://web.archive.org/web/20161022174026/http://www.rafekettler.com/magicmethods.html).

```python
class Zebrik(object):

    # konstruktor je magická metoda!
    def __init__(self, delka):
        self.delka = delka  # metry
    
    # volá se při použití operátoru `<` (lower than)
    def __lt__(self, other):
        return self.delka < other.delka
    
    # volá se při pokusu vypsat objekt
    def __str__(self):
        return "#" * self.delka
```

použití `__lt__` použi 
```python
dlouhy  =  Zebrik(9)
kratky  =  Zebrik(3)

dlouhy < kratky
```

řazení bez použití parametru `key` - interní použití `__lt__` 
```python
pozarni_sklad = sorted([Zebrik(2), Zebrik(18), Zebrik(6)])
```

použití `__str__`
```python
print(pozarni_sklad[0])
print(pozarni_sklad[1])
print(pozarni_sklad[2])
```

## Metoda `__str__`
Používá se pro převod oběku na textový řetězec.
```python
class Trida:
   def __init__(self, parametr):
       self.atribut = parametr

   def __str__(self):
       return str(self.atribut)

          
a = Trida("a")
print(a)
```

## Metoda `__repr__`
Narozdíl od metody `__str__`, která informuje o objekdy, metoda `__repr__` se používá k "oficiální" reprezentaci objektu.
```python
class Trida1:
   def __repr__(self):
       return "Moje trida"

class Trida2: pass

t1 = Trida1()
t2 = Trida2()
print(t1)
print(t2)
```

## Metoda `__add__`
Zavolá se v okamžiku, kdy je objekt sčšítán s jiným oběktem.
```python
class Trida:
  def __init__(self, cislo):
      self.cislo = cislo

  def __add__(self, other):
      return Trida(self.cislo+other.cislo)
 
t1=Trida(1)
t2=Trida(3)
t1+t2
print(t3.cislo)
```

Další metody matematických operaci jsou:
- **`+`**: `__add__(self, other)` 
- **`-`**: `__sub__(self, other)` 
- **`*`**: `__mul__(self, other)` 
- **`/`**: `__div__(self, other)` 
- **`%`**: `__mod__(self, other)` 

> #### Příklad
> Rozšiřte příklad s knihovnou takovým způsobem, že:
> - bude možné obsah jednotlivých knihoven spojit pomocí operátoru `+` 
> - Případně odečíst operátorem `-`
```python
# Řešení
```

## Metoda `__len__`
Použije se pro zjišťování delky objektu
```python
class Trida:
  def __init__(self):
      self.list = []

  def pridej(self,polozka):
      self.list.append(polozka)

  def __len__(self):
      return len(self.list)

t1=Trida()
t1.pridej("a")
t1.pridej("b")
print(len(t1))
```

> #### Příklad
> Rozšiřte příklad s knihovnou takovým způsobem, že bude možné zjistit počet knih pomocí `len()`
```python
# Řešení
```

## Metoda `__eq__`
Slouží pro test rovnosti dvou objektů
```python
class Trida:
  def __init__(self, cislo):
      self.cislo = cislo

  def __eq__(self, other):
      return self.cislo == other.cislo
 
t1=Trida(1)
t2=Trida(3)
t1 == t2
```

Další metody matematických operaci jsou:
- **`<`**: `__lt__(self, other)` 
- **`<=`**: `__le__(self, other)` 
- **`==`**: `__eq__(self, other)` 
- **`!=`**: `__ne__(self, other)` 
- **`>`**: `__gt__(self, other)` 
- **`>=`**: `__ge__(self, other)` 

> #### Příklad
> Rozšiřte příklad s knihovnou takovým způsobem, že bude možné porovnat dvě knihovny podle obsahu
```python
# Řešení
```

## Práce s atributy
```python
class Trida:
    def __init__(self):
        self.atr = "Ahoj"
       
t = Trida()
```

Vrátí True pokud atribut existuje
```python
hasattr(t, 'atr')   
```

Vrátí hodnotu atributu
```python
getattr(t, 'atr')   
```

Nastaví hodnotu atributu
```python
setattr(t, 'atr', 7000)
```

Smaže atribut
```python
delattr(t, 'atr')
```

## Metoda `__getattribute__`
Tato metoda se volá v okamžiku přistupu k atributu
```python
class Trida:
    def __init__(self):
        self.atr = "slon"
        
    def __getattribute__(self, name):
        print("Pristupuju k atributu: ", name)
        if (name == "atr"):
            return object.__getattribute__(self, name)
        return None

t1=Trida()
t1.atr
```

Obdobně exisuji i další metody, například `__setattribute__`

## Metoda `__iter__` a `__next__`
Implemntaci těchtometodu umožníme použí objekt ve `for` cyklu.
```python
import random
class MujIter:
   def __iter__(self):
       self.count = 10
       return self

   def __next__(self):
       if self.count > 0:
           self.count -= 1
           return random.random()
       else:
           raise StopIteration

iter = MujIter()
for i in iter:
   print(i)
```

> #### Příklad
> Rozšiřte příklad s knihovnou takovým způsobem, aby bylo možné použít for konstrukci pro procházení knižky. 
```python
# Řešení
```

# Třída - defaultní atributy
```python
class Trida:
   pass
```

jméno třídy
```python
Trida.__name__
```

jméno modulu
```python
Trida.__module__
```

Transformace třídy do slovníku
```python
print(Trida.__dict__)
```

Získání jmena třídy z objektu
```python
t = Trida()
t.__class__.__name__
```

# Dokumentace třídy
T5ídy se dokumentují podobně jako funkce
```python
class Trida:
   'Tohle je moje prvni trida'

   def __init__(self):
       pass
       

t = Trida()
print(Trida.__doc__)
print(t.__doc__)
help(Trida)
```

# Zjišťování typu
```python
a=10
type(a)
```

```python
b=[]
type(b)
```

```python
isinstance(a,int)
```

> #### Příklad
> Vytvořte instance různých typů a vypište jejich typ pomocí metody `type(<obj>)`
```python
# Řešení
```

> #### Příklad
> Vytvoře metodu, převezme neomezený počet paratetrů a spočítá kolik z nich je `str` a kolik `int`
```python
# Řešení
```

> #### Závěrečný příklad
> - Vytvořte třídu `KusTextu`, která umožní postupně sestavovat kusy textu.
> - Uvnitř třídy vytvořte metodu `pridej("text")`.
> - Jakmile bude přidaný text obsahovat tečku, inkremetujte **celkový počet vět** (použijte třídní atribut).
> - Zajistěte, aby bylo možné kus textu vypsat pomocí běžného `print()`.
> - Zajistěte možnost řetězení: `pridej("x").pridej("y").pridej("z")...`
```python
# TODO napsat třídu KusTextu
```

```python
# následující kód by měl ve finále fungovat

# xx = KusTextu()
# yy = KusTextu()
```

```python
# xx.pridej("Jak na Nový rok,")
# yy.pridej("Kdo jinému jámu kopá,")

# xx.pridej(" tak po celý rok.")
# yy.pridej(" sám do ní spadne.")
```

```python
# print(KusTextu.celkovy_pocet_vet)  # = 2
# print(xx)  # = Jak na Nový rok, tak po celý rok.
# print(yy)  # = Kdo jinému jámu kopá, sám do ní spadne.
```

```python
# zz = KusTextu()
# zz.pridej("Starého psa").pridej(" novým").pridej(" kouskům nenaučíš.")
# print(zz)  # = Starého psa novým kouskům nenaučíš.
```
