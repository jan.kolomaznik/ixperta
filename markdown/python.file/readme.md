# Práce se soubory

Pro přístup k souboru exituje příkaz `open`

Pro práce se souborem
- *Čtení*: metoda `read()`
- *Zapis*: metoda `write()`

Uzavření souboru po ukončení práce metoda `close()`

```python
f = open("static/empty.txt")
f.read()
f.close()
```

**V čem může být problém?** 

Zachyvení výjimku
```python
try:
    f = open("static/empty.txt")
    f.read()
finally:
    f.close()
```

nebo ješte líp:
```python
with open("static/leave-me-alone.txt") as f:
    f.read()
```

## Mody otevření soubotu
- `r` = **read** = čtení
- `w` = **write** = zápis (smaže existující data)
- `a` = **append** = přidávání (zápis za existující obsah)
- `b` = **binary** = binární režim (jediná možnost vynechat kódování)
- `+` = **čtení** i zápis
- módy lze kombinovat (wb+)

vytvoření textového soubotu a zapsání řetězce
```python
with open("static/test.txt", mode="w", encoding="utf8") as f:
    f.write("funguje to?\n")
    print("such print, very newline", file=f)
```

přečtení celého souboru najednou
```python
with open("static/test.txt", mode="r", encoding="utf8") as f:
    print(f.read())
```

> #### Příklad
> Načtěte od uživatele vstup a uložte jej do souboru. 
> Následně soubor načtěte a vypište jeho obsah.
```python
# Řešení
```

přečtení po řádcích
```python
with open("static/test.txt", mode="r", encoding="utf8") as f:
    for radka in f:
        print(radka)  # proč dvojí odřádkování?
```

přečtení *najednou* a rozdělení po řádcích
```python
radky = []
with open("static/test.txt", mode="r", encoding="utf8") as f:
    radky = f.readlines()
print(radky)
```

> #### Příklad
> Spočítejte žádky v souboru `static/monty-python-fairy-tale.txt`
```python
# Řešení
```

Metoda `seek(pozice)` umístí čtecí hlavu na danou pozici.
Má volitelný parametr **`whence = 0`**:
- **`0`**: Absolutní pozice od začátku souboru
- **`1`**: Relativní pozice od aktuální pozice
- **`2`**: Absolutni pozice od konce souboru
 
K metodě `seek` se váže metoda `tell()`, která vrátí aktuální pozici.
```python
with open("static/monty-python-fairy-tale.txt", mode="r", encoding="utf8") as f:
    f.seek(2579)
    f.readline() # Toto je první věta krále.
```

> #### Příklad
> Vypište všechny osoby a obsazení v pohádce `data/monty-python-fairy-tale.txt`
```python
# Řešení
```
