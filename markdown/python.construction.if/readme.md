# Ternární operátor
Mnoho jazyků má takzvaný "ternární operátor".

Tedy operátor který kombinuje výraz a `if` strukturu.

Příklad zápisu z jiných jazyků: `promena = (podminka) ? vyraz_1 : vyraz_2`

Python má něco podobného samozřejmě také. 
Jen se to celé zapisuje pomocí `if`

Syntaxce: `promena = vyraz_1 if podminka else vyraz_2`

> #### Příklad:
> Napište výraz, který ze dvou čícel vybere to větší
```python
a = 1
b = 2

vetsi_cislo = ???

# Výpis
vetsi_cislo
```