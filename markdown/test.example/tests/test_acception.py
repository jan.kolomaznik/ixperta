from textwrap import dedent

from src import power

def test_power_1():
    """
    GIVEN: síť obsahuje cyklus: T01 > T02 > T03
    WHEN: ???
    THEN: Ocekávám se algoritmu detekuje chybu
    """
    input = dedent("""
            T01 - T02: 15
            T02 - T03: 10
            T02 - T04: 2
            T03 - T01: 18
            """)
    output = "Stav site ERROR"
    power.main()