#!/ust/bin/python3

class Edge:
    def __init__(self, n_from, n_to, weight):
        self.n_from = n_from
        self.n_to = n_to
        self.weight = weight

    def __eq__(self, other):
        return (self.n_from == other.n_from and self.n_to == other.n_to) or (self.n_to == other.n_from and self.n_from == other.n_to)

class Graph(object): # Graph class for describing the electrical network
    def __init__(self):
        self.nodes = []
        self.edges = {}
        self.edge_obj = []
        self.cost = 0

    def add_line(self,line):
        data = [l.strip() for l in line.split(":")]
        weight = int(data[1])
        nodes = [d.strip() for d in data[0].split("-")]
        n_from = nodes[0]
        n_to = nodes[1]

        if n_from not in self.nodes:
            self.nodes.append(n_from)

        if n_to not in self.nodes:
            self.nodes.append(n_to)

        if n_from not in self.edges.keys():
            self.edges.update({n_from: [{n_to: weight}]})
        else:
            self.edges[n_from].append({n_to: weight})

        if n_to not in self.edges.keys():
            self.edges.update({n_to: [{n_from: weight}]})
        else:
            self.edges[n_to].append({n_from: weight})

    def remove_line(self, e_from, e_to):
        if len(self.edges[e_from]) > 1:
            for dest in self.edges[e_from]:
                if next(iter(dest)) == e_to:
                    self.edges[e_from].remove(dest)
        else:
            self.edges[e_from] = []

    def print_graph(self):
        print("Nodes:", end=" ")
        for n in self.nodes:
            print(n, end=", ")
        print()

        print("Edges: ")
        for e in self.edges:
            print("%s - %s" % (e, self.edges[e]))

    def get_edge_obj(self):
        for e in self.edges:
            n_to = self.edges[e]

            for to in n_to:
                node_name = next(iter(to))
                weight = to.get(node_name)
                edge = Edge(e, node_name, weight)
                # store only in one direction
                stored = False
                for prev_edge in self.edge_obj:
                    if (prev_edge == edge):
                        stored = True
                if not stored:
                    self.edge_obj.append(edge)