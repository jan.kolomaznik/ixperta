#!/bin/python3
import os, sys
from src.graph import Graph


class Power(Graph):
    def __init__(self):
        super().__init__()

    def moore(self, root=None):
        # representing queue operation
        queue = []

        if root is None:
            root = self.nodes[0]
        # start with root element
        visited = {}
        parent = {}
        for n in self.nodes:
            visited[n] = False
            parent[n] = None

        visited[root] = True
        queue.append(root)

        while queue:

            node = queue.pop()
            adj = self.edges.get(node)
            for a in adj:
                key = next(iter(a))
                if not visited[key]:
                    visited.update({key: True})
                    queue.append(key)
                    parent.update({key: node})
                elif parent[node] != key:
                    return True
        return False

def main():
    
    network = Power()
    for line in sys.stdin:
        # create graph
        network.add_line(line)

    cycle = network.moore()

    if cycle:
        print("Stav site ERROR")
    else:
        print("Stav site OK")


if __name__ == "__main__": # do not run when imported
   main()
