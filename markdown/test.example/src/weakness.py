#!/bin/python3
import os, sys
from reset import Reset

def main():
    
    network = Weakness()
    for line in sys.stdin:
        # create graph
        network.add_line(line)


    if network.components() != 1:
        print("Error! Multiple components!")

    network.find_articulation()

        


class Weakness(Reset): # inherit power for cycle detection
    def __init__(self):
        super().__init__()
        self.diff = {}

    def components(self):

        self.get_edge_obj()
        spanning = self.kruskal()  # get minimal spannign tree


        for nodes in spanning.nodes:
            ok = 0
            queue = []
            node = nodes

            visited = {}
            for n in spanning.nodes:
                visited[n] = False

            queue.append(node)
            visited[node] = True


            while queue:
                node = queue.pop()
                adj = spanning.edges.get(node)
                for a in adj:
                    key = next(iter(a))
                    if not visited[key]:
                        visited.update({key: True})
                        queue.append(key)

            for t in visited:
                if visited[t]:
                    ok += 1
            self.diff[nodes] = ok

        vals = set(sorted(self.diff.values()))
        return len(vals)

    def find_articulation(self):

        # remove node one by one and find out if there are multiple components

        for i,e in enumerate(self.edge_obj):
            tmp = self.edge_obj.pop(i)
            if self.components() > 1:
                print("%s -> %s" % (e.n_from, e.n_to))
            self.edge_obj.append(tmp)





            
if __name__ == "__main__": # do not run when imported
   main()