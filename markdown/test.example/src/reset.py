#!/bin/python3
import os, sys
from power import Power

def main():
    
    network = Reset()
    for line in sys.stdin:
        # create graph
        network.add_line(line)
        
    network.get_edge_obj()
    kruskal = network.kruskal()
    for o in network.order:
        print(o)
    print("Hodnoceni: %d" % kruskal.cost)


class Reset(Power): # inherit power for cycle detection
    def __init__(self):
        super().__init__()
        self.total_cost = 0
        self.order = []

    def kruskal(self):
        # sort edges by size
        self.edge_obj.sort(key=lambda x: x.weight)
        spanning = Power()

        for i in range(0, len(self.nodes)-1):
            edge = self.edge_obj[i]
            root = edge.n_from

            spanning.add_line("%s - %s: %d" % (edge.n_from, edge.n_to, edge.weight))

            if spanning.moore(root):
                spanning.remove_line(edge.n_from, edge.n_to)
                self.edge_obj[i].weight = 99999999
            else:
                self.order.append("%s - %s: %d" % (edge.n_from, edge.n_to, edge.weight))
                spanning.cost += edge.weight
        return spanning



if __name__ == "__main__": # do not run when imported
   main()