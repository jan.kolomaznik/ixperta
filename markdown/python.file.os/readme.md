# Modul `os`

Tento modul umožňuje používat funcke závislé na operačním systému.
Největší část modulu se věnuje práve praci s adresářovou strukturou a souboty.

K tomu modulu se ještě váže modul `shutil`, který poskytuje komplexnější funkce.

```python
import os
```

## Aktuální pracovní adresář
V operačním systému každá aplikace pracuje v jejím kořenovém adresáři. 
Obecně je pro něj zaužívaný symbol: `./`.

V python jsme jej schopni zjisiti funkcí `getcwd`:
```python
os.getcwd()
```

A je možné jej také změnit príkazem `chdir`
```python
os.chdir('./static/')
os.getcwd()
```

## Procházení adrsářů
K prohledáná adresářů nebo také spíš vypsání jejich slouží funkce `listdir()`.
Tato funkce vrací List:
```python
os.listdir() # Pokud není urcen adresář, prochází se aktuální.
```

Druhou funkcí je `scandir`, ta nevrací seznam (list) ale iterátor.
Je tedy výhodnější z hlediska paměti a také při použítí ve `for` cyklu nebo `itertools`.
```python
for dir in os.scandir():
    print(dir.path)
```

Poslední funkcí do trojice, o které se tu zmíníme je funkce `walk`.
Ta vrací iterátor, podobně jako `scandir`, ale iteruje přes všechny podadresáře.
Pro každý adresář vrací trojici: `(dirpath, dirnames, filenames)`.

```python
for dirpath, dirnames, filenames in os.walk("."):
    print(dirpath, dirnames, filenames)

```

## Další funce modulu os:

- `mkdir`: slouží s vytvoření adreáře. 
- `rename`: přejmenování adreáře nebo souboru
- `remove`: smazání adresáře nebo souboru, v tomto případě musí být adreář prázdný.

s
