# Dekorátory

Další věc, na kterou se podíváme, jsou *dekorátory* – způsob, jak si přizpůsobovat funkce.

Dekorátor je tedy fukce, obaluje jinou funkci. 
To která funkce bude obalena a která ne se určije pomocí `annotace` před definicí obalované funkce.

## Registrace funkce
Nejjednodušší použití dekorátorů je *registrace*: 
K funkci přidáme dekorátor a funkce se někam zaregistruje, uloží, aby se dala zavolat později.
Typický příklad je `@app.route` ve Flasku.

My si pro příklad budeme chtít udělat dekorátor pro kalkulačku.
Ta by měla fungovt tak, že uživatel zadá první vstup, pak název operace a pak druhý vztup.

Kalkulačka by pak mohla vypadat nějak takto:
    
    First number: 1
    Operation: add
    Second number: 2
    3

Samotná funkce kalkačky bude tedy následující:    
```python
def run_calc(operators) :
    a = int(input('First number: '))
    operator_name = input('Operation: ')
    b = int(input('Second number: '))
    func = operators[operator_name]
    print(func(a, b))
```
    
Bez použití dekorátorů by se to dalo napsat takto:

```python
operators = {}

def register_operator(func):
    operators[func.__name__] = func

def add(a, b):
    return a + b

register_operator(add)

run_calc(operators)
```

S použitím dekorátoru je funkce `register_operator` téměř stejná, jen použijeme speciální syntaxi se zavináčem.

```python
operators = {}

def register_operator(func):
    operators[func.__name__] = func
    return func

@register_operator
def add(a, b):
    return a + b

run_calc(operators)
```

Použití dekorátoru je jen zkrácený zápis pro volání dekorátoru jako funkce, volaní z předchozího příkladu jsou ekvivalentní tomuto:

```python
def add(a, b):
    return a + b

add = register_operator(add)
```

> #### Příklad:
> Chce vytvořit jednoduchou aplikaci na zprávu telefonních kontaktů. 
> Data bude uchovávat v ve slovníku, kde klíčem je název člověka a hodnotou je sezman jeho telefonních čícel.
>
> K seznamu chce postupně přidávat nové příkazy, které budete moci volat z příkazového řádku. 
> Syntaxe každého volání je `command argument`, command je defakto název funkce. 
> Pro začátek počítejme, že command má vždy jeden argument.
>
> Nejpre vytvořte seznam kontaktů, dekorátor `command` který bude funkce registorvat.
```python
# Řešení
```
> poté vytvořte funkci `add_person(jmeno)`, která do senzamu kontaktů přidá nového uživatele.
```python
# Řešení
```
> Funkci `select_person(jmeno)` která vybere jednoho cloveka z kontaktů
```python
# Řešení
```
> A funkci `add_number(cislo)`, která přidá v vybranému človeku nové telefonní číslo.
```python
# Řešení
```
> Tuto funkci poté zaregistrujte a použije v hlavní funkci.
```python
# Řešení
```

## Funkce vyššího řádu
Chování samotného `@` je tedy celkem triviální.
Magie (složitost) spočívá v tom, že dekorátor je většinou funkce vyššího řádu:
- bere jinou funkci jako argument a taky jinou funkci vrací.
- V případě registrace vrací stejnou funkci jako dostala – ale to není povinné.

Často se setkáme s dekorátory, které dekorovanou funkci nějak modifikují.
Například můžeme napsat dekorátor, který v naší kalkulačce převede vstup na reálná čísla.
Dělá to tak, že definuje *novou funkci*, která volá tu původní – ale před nebo
po tomto volání může dělat i něco jiného.

```python
def to_floats(func):
    def outer_function(a, b):
        a = float(a)
        b = float(b)
        return func(a, b)
    return outer_function

@to_floats
def add(a, b):
    """Adds two numbers"""
    return a + b

print(add(1, '2'))
```

Takto funguje většina dekorátorů, které mění chování dekorované funkce.

> #### Příklad:
> Vytvořte nový dekorátor, `person_select_requred`, která ověří, že příkaz je volán v době, kdy uživatel vybral člověka z kontaktů.
> Tento dekotáro použije u funkce `add_number`
```python
# Řešení
```

Naráží s tím ale na jeden problém: nově nadefinovaná funkce má vlastní jméno
(a dokumentační řetězec a podobné informace), což kazí iluzi, že jsme
původní funkci jen trošku změnili:

```python
print(add)
help(add)
```

Řešení je jednoduché – zkopírovat jméno, dokumentační řetězec atd. z jedné funkce na druhou.
Na to ve standardní knihovně existuje dekorátor jménem `functools.wraps`:

```python
import functools

def to_floats(func):
    @functools.wraps(func)
    def outer_function(a, b):
        a = float(a)
        b = float(b)
        return func(a, b)
    return outer_function
```

S `wraps` bude `help(add)` fungovat správně – ukáže původní jméno a dokumentační řetězec.

## Parametry dekorátoru
Z volání `wraps(func)` je vidět, že jako dekorátor můžeme použít i volání funkce, ne jen funkci samotnou.
Budeme-li chtít napsat dekorátor, který tohle umí, potřebujeme napsat funkci ještě vyššího řádu – totiž funkci, která po zavolání vrátí dekorátor:

```python
operators = {}

def register_operator(name):
    def decorator(func):
        operators[name] = func
        return func
    return decorator

@register_operator('+')
def add(a, b):
    return a + b

@register_operator('*')
def mul(a, b):
    return a * b

run_calc(operators)
```

Řádek `@register_operator('+')` dělá (jak už víme) to stejné, jako bychom hned za funkcí napsali `add = register_operator('+')(add)`.

> #### Příklad:
> Upravte dekorátor `command` tak aby přijímal jako parametr název příkazu.
```python
# Řešení
```
> Upravte všechny metody tak, aby nazev commandu nebyl shodný s názvem funkce. 
> Mužete použí camelCase nabo pomočky (add-contact)
```python
# Řešení
```

Budete-li chtít napsat dekorátor, který bere argumenty, a přitom ještě „mění“ dekorovanou funkci, dostanete se na tři funkce zanořené v sobě:

```python
import functools
operators = {}

def register_operator(name):
    def to_floats(func):

        @functools.wraps(func)
        def outer_function(a, b):
            a = float(a)
            b = float(b)
            return func(a, b)

        operators[name] = outer_function
        return outer_function

    return to_floats

@register_operator('+')
def add(a, b):
    return a + b

func = operators['+']
print(func(1, '2'))
```

Dekorátorů se na jedné funkci dá použít víc:

```python
@register_operator('×')
@register_operator('*')
def mul(a, b):
    return a * b
    
run_calc()
```

Úplně stejně jako funkce se dají dekorovat i třídy.
Dekorátor dostane třídu jako první argument a třída se nahradí tím, co dekorátor vrátí.

## Parametry dekorované funkce
V předchozích příkladech jsme vždy znali počet argumentů dekorované funkce.
Jinými slovy `register_operator` šel použit jen na funkce se dvěma parametry.

```python
@to_floats
def abs(a):
    if a > 0:
        return a
    else:
        return -a
         

abs("3")
``` 

Pokud chceme vytvořit dekoráro schopný pracovat s libilnou funcí, musíme ve vnítřní funkci použít dvojici parametrů: `*args, **kwargs`

```python
def log_params(func):

    @functools.wraps(func)
    def outer_function(*args, **kwargs):
        print(args, kwargs)
        return func(*args, **kwargs)
        
    return outer_function


@log_params
def abs(a):
    if a > 0:
        return a
    else:
        return -a
        
@log_params
def plus(a, b):
    return a + b
```

použití ...

```python
print(abs(3))
print(plus(b=1, a=2))
```

> #### Příklad:
> Upravte příklad s knihovnou tak, aby příkazy mohli mít libovolný počet parametrů.
> Dopňte funkci `print_numbers` a `print_contacts`, které vupíšou seznam čísel u aktuálního kontaktu a jména všech kontatů.
```python
# Řešení
```

---
Tento kuz vychází ze [GitHubu](https://github.com/cvut/naucse.python.cz/blob/b181/lessons/intro/magic/index.md)

Licence: [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)

Licence ukázek kódu: [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)