# Pickle

Pickle se používá pro serializaci a de-serializaci objektů.
Jedná se o proces převodu objektu v paměti do datového streamu, který je možné uložit na disk nebo poslat přes síť. 
Tento datový stream pak může býz znovu načten zpět do objektu Python. 

Pozor, nejedná se o datovou kompresi, ale převod objektu z jedné reprezentace (data v paměti s náhodným přístupem (RAM)) na jinou (text na disku).
Pokud chceme ušetřit místo na disku a datový stream komprimovat, musíme to udělat samostatně.

Používá se tedy v případě, že ve své plikaci potřebujete jistý stupň perzistence dat.
Tedy pokud Váš program si potřebuje pamatovat nějaké data mezi spuštěními, nebo když potřebujete data posílat po sití prostřednitví TCP protokolu, nebo třeba ukládat objekty do databáze.

Rovněž je velmi užitečné pro ML, v kamžiku, kdy si potřebujete uložit naučené modely pro další použití.

Naopak Pockle se nehodí pro výměnu dat mezi různými programi. 
Dokonce není zaručena kompatibilita mezi různými verzemi Pythonu!

## Import Pickle
Nejprve je potřeba modul naimportovat příkazem:
```python
import pickle
```

## Ukládání dat
Pickle se hodí pro ukládní:
- Boolean,
- Integer,
- Float,
- Complexních čísla,
- (normal and Unicode) Strings,
- Tuple,
- List,
- Set
- Dictionarys s picklable objects.

Vytvořme si tedy slovník obsající data:
```python
dogs_dict = { 'Ozzy': 3, 'Filou': 8, 'Luna': 5, 'Skippy': 10, 'Barco': 12, 'Balou': 9, 'Laika': 16 }
```

Pickle umí dále ukládat třídy a doknce i funkce.

Ne všechno lze pomocí Pickle uožit, například generátry, iterátory, vnitřní třídy, lambda výrazy, ... již tak tak snadno uložit nejdou.
Pro tyto případy je nutné použít specilání balíček jménem `dill`.

### Srovní Pickle a JSON
JSON má proroti Pickle několik výhod, předně je možné jej přečít a editovat bez speciálního nástroje.
Je nezávislý na programovacím jazyce a dá se snadno přenášet.
Také je oproti Pickle rychlejší.

Pickle je nicméně stále velmi dobrou možností, pokud chcete ukládat data pouze v Pythonu.

## Ukládání pickle do souboru
Pro uložení dat pomocí Pickle do souboru musíme pomocí metody `open()` otevřít soubor s parametrem `wb` tedy:
 - `w` pro zápis a případný přepis souboru.
 - `b` indikuje právi s binárním souborem.
 
Funkce modulu `piskle.dump()` se postará o serializaci objektu.
Pokud jako druhý parametr dáme ukazatel na soubor, je to něj rodnou zapsán:
 
```python
with open("dogs", mode="wb") as outfile:
    pickle.dump(dogs_dict, outfile)
```

Pro vykonání tohoto příkau by se měli data uložit do nově+ vytvořeného (nebo čerstvě přepsaného) souboru.

## Načítání dat ze souboru
Jedná se o obdobnou předchozího příkladu. 
soubor se ale použíbǘá v modu `r` = read.
A použije se metoda `pickle.load()` pro načtení:

```python
with open("dogs", mode="rb") as infile:
    new_dict = pickle.load(infile)
    
new_dict
```

Nyní můžeme snadno otestovat se uložená a načtená data jsou zhodná:
```python
new_dict == dogs_dict
```

## Komprese dat
Při ukládání velkých dat, může být váhodné soubor zkomprimovat.
K tomu je možné použít knihovny: `bz2`, která je oučástí základní knihony:

```python
import bz2
```

Uložení komprimovaných dat pak může vypadat následovně:
```python
with bz2.open("puppies", mode="wb") as f:
    pickle.dump(dogs_dict, f)
```

a načtení velmi podoně:
```python
with bz2.open("puppies", mode="rb") as f:
    puppies = pickle.load(f)
puppies
```

Test zhodnoti:
```python
dogs_dict == puppies
```
## Best practices
Pro psáci s Pickle platí několik základních pravidel, které je dobré doržovat:

1. Pickle by nemělo by nikdy použito pro přenos dat mezi dvěma rozdílnými (neznámými) systémy.
2. Pickle soubory si posílejte vždy jen po zabezpečených kanálech - **jedná se defakto o plain text**.
3. Ukládáte data na disk opatřete je podpisem s kontrolním součtem. 
Zabráníte tak útočníkovy napadnout Vaši aplikaci podvržením uložených dat.

### Příklad podpisu dat:
Pro podpis použijeme knihovnu `hmac`:

```python
import hmac
import hashlib
```

Nyní můžeme data podepsat a uložit:
```python
pickled_data = pickle.dumps(dogs_dict)
digest = hmac.new(b"secret-key", pickled_data, hashlib.sha1).hexdigest()
digest, pickled_data
```

### Naštení a kontrola itegrity a pravosti dat:
předokládejme, že jsem načetli digest, pickled_data a chceme je ověřit:

```python
new_digest = hmac.new(b'secret-key', pickled_data, hashlib.sha1).hexdigest()
if digest != new_digest:
    print('Integrity check failed')
else:
    print(pickle.loads(pickled_data))
```

Pokud bychom data poškodili:
```python
pickled_data = pickle.dumps({"dog": "Baskervilles"})

new_digest = hmac.new(b'secret-key', pickled_data, hashlib.sha1).hexdigest()
if digest != new_digest:
    print('Integrity check failed')
else:
    print(pickle.loads(pickled_data))
```
