## Datum a čas
Python poskytuje tři typy: : `date`, `time`, `datetime`.

Pro jejich použití je ale potřeba naimpotrovat modul, podobně jako jsme importovali modul pro `math`.
To je také důvod proč se tu o nich zmíníme jen okrajově:
```python
from datetime import datetime
``` 

pokud chceme získtát aktulní čas, pak můžeme použit funkci `now()`.
```python
start = datetime.now()
start
```

Dále můžeme z objektu získat konkrétní hodnoty, například rok:
```python
start.year, start.month, start.day, start.hour, start.minute, start.second
```

Pokud byvhom chtěli čas výpsat v lidsky čitelném formátu, můžeme použit funkci `strftime('%d.%m.%Y')`:
```python
start.x('%d.%m.%Y')
```

Existuje i funkce `strptime()` která naopak převede string na datum a čas:
```python
stop = datetime.strptime('23.08.2019', '%d.%m.%Y')
```

Pokud chceme zjistit, kolik času uběhlo, můžeme objekty jednoduše odečíst:
```python
stop - start
```