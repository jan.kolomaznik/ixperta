# Základní datové struktury
**`tuple()`**
kolekce lib. objektů, nelze měnit

**`list()`**
kolekce lib. objektů

**`set()`** 
kolekce lib. unikátních objektů

**`dict()`**
slovník, key-value

## N-tice (tuple)
Neměnné pole (indexujeme od 0).

je neměnný a k jednotlivým prvkům mhu přistoupit pomocí `[]`, obdobně jako jsme to již dělali u řetězce.

N-tice a přístup k prvnímu prvku
```python
ntice = (1, 2, 3)
ntice[0]
```

N-tice a přístup k poslednímu prvku
```python
ntice = (1, 2, 3)
ntice[-1]
```

Přístup mimo rozsah N-tice
```python
ntice = (1, 2, 3)
ntice[0]
```

N-tice mohou obsahovat různé typy
```python
("má", 4, "smíšené", "datové typy")
```

takzvaný "tuple unpacking" = rozbalení
```python
ntice = (1, 2, 3)
a, b, c = ntice
c
```

test zda N-tice obsahuje prvek
```python
ntice = (1, 2, 3)
2 in ntice
```

> #### Příklad:
> 1. Vytvořte ntici obsahujíci 2, 3, 4, 5 mocninu z uživatelem zadaného čísla.
> 2. Poté se zeptejte uživatele na číslo a ověřte, jestli je obsaženo ve vytvožené ntici.
```python
# Řešení
```

### Jednoprvké tuple
Zamyslete se nad následujícím kouskem kódu, co vlastně vytváří?
```python
t = (1)
```

Python interpretuje (naprosto logicky a správně) výraz `(1)`, jehož výsledek je jedna.

Jak tedy vytvořit jednoprvkový tuple?
Jedna varianta je použit funkci:
```python
tuple(1)
```

Ale nebyl by to python, kdyby nenabízel lepší řešení:
```python
(1,)
```

## Seznam (list)
Jako N-tice, akorát jej lze měnit.

K prvkům seznamu přistupuji přes indexy v `[]` stejně jako u řetězce nebo n-tice.

Vytvoření seznamu pomocí hranatých závorek
```python
seznam = [1, 2, 3]
print(seznam)
```

prázdný list pomocí 
```python
seznam = []
print(seznam)
```

nebo pomocí funkce `list()` a ntice případně jiné stuktury
```python
    ntice = (1,2,3) 
seznam = list(ntice)
print(seznam)
```

Podobně jako `tuple` může obsahovat různé prvky
```python
seznam = [123,'abc',1.23]
print(seznam)
```

> #### Příklad:
> Uživatel zadá seznam čísel oddělených mezerami.
> Metodou `split` z něj vytvoříte seznam.
>
> Vypište druhé a před-poslední prvek seznamu.
```python
seznam = input("Zadej čísla oddělená mezerou: ").split()
# Řešení
```

Určení počtu prvků v seznamu pomocí funkce `len()`
```python
seznam = [123,'abc',1.23]
len(seznam)
```

Dotaz na přítomnost prvku v seznamu
```python
seznam = [123,'abc',1.23]
prvek = 66
prvek in seznam
```

### Operace se seznamy (nemění vstupní seznam)
Seznamy můžeme sečíst, původní zůstanou nezměněné
```python
seznam = [123,'abc',1.23]
seznam + [4,5,6]
```

Seznamy můžeme i násobit číslem
```python
seznam = [123,'abc',1.23]
seznam * 2
```

Získání určité části seznamu
```python
seznam = [1,2,3,4,5,6,7,8,9]
seznam[2:5]
```

Odříznutí začátku
```python
seznam = [1,2,3,4,5,6,7,8,9]
seznam[2:]
```

Odříznutí konce
```python
seznam = [1,2,3,4,5,6,7,8,9]
seznam[:5]
```

Při řezání můžeme indexovat i od konce!
```python
seznam = [1,2,3,4,5,6,7,8,9]
seznam[:-2]
```

### Vyhledání v seznam
pomocí index

> #### Příklad:
> Vytvořte dva listy obsahující 3 libovolné položky.
> Vytvořte nový seznam obsahující dva poslední prvky z obou seznamů.
```python
# Řešení
```

### Modifikace seznamu
Změna konkrétní položky
```python
seznam = [123,'abc',1.23]
seznam[0] = 10
seznam
```

Připojení položky na konec seznamu
```python
seznam = [123,'abc',1.23]
seznam.append('ahoj')
seznam
```

Připojení jiného seznamu na konec seznamu
```python
seznam = [123,'abc',1.23]
seznam.extend(['ahoj', 'svete'])
seznam
```

Přidání položky na konkrétní pozici
```python
seznam = [123,'abc',1.23]
seznam.insert(1, 'kuk')
seznam
```

Odebrání položky ze seznamu podle indexu, `pop()` bez parametru odebere poslední prvek.
```python
seznam = [123,'abc',1.23]
print(seznam.pop(1))
seznam
```

Uspořádání prvků v seznamu
```python
seznam = ['bb', 'aa', 'cc'] 
seznam.sort()
seznam
```

Otočení počadí prvků v seznamu
```python
seznam = ['aa', 'bb', 'cc'] 
seznam.reverse()
seznam
```

### Generování seznamu čísel
Seznam s posloupností čísel se používá v cyklech
```python
list(range(4))
```

Nastavení mezí a přírůstku při generování seznamu čísel.
```python
list(range(-6,7,2))
```

> #### Příklad
> Vytvořte list `[1,2,3,4,5]` pomocí `range()`. 
> K listu přidejte položku `10` na druhou pozici a následně list seřaďte. 
```python
# Řešení
```

Spočítání výskytů nějakého prvku v seznamu.
```python
seznam = [123,"a",123,"b"]
seznam.count(123)
```

### Rozbalení seznamu
Stejně jako `tuple` umí Python rozbalit i seznam. 
Zvažte následující příklad:

```python
values = [1, 2, 3]
a, b, c = values
```

Ale pozor, pokud bychom chtěli seznam rozbalit do funkce (funkce probereme za chvíli) tak se seznam aplikuje pouze na první paramter.
Musíme tedy Pythonu říci, aby seznam rozbalil a to uděláme pomocí operátoru `*`

```python
def sum(a, b, c):
    return a + b + c

values = [1, 2, 3]
sum(*values)
```

## Množina (set)
Matematicky pojatá – každý prvek pouze jednou, je neuspořádaná.

Definice výčtem.
```python
{1, 1, 2, 2, 3, 4}
```

Převod z listu (funguje i naopak)
```python
seznam = ["a","b","c"]
set(seznam)
```

### Testy nad množinami
Dotaz jestlu je prvek v množině
```python
mnozina = {1, 2, 3}
prvek = 5
prvek in mnozina
```

Dotaz jestli jsou podmnožinou jiné množiny, existuji i opačný dotaz: `<set1>.issuperset(<set2>)`
```python
u = {1, 2, 3, 4}
v = {1, 2, 3}
v.issubset(u)
```

Zjištění počtu prvků v množině
```python
u = {1, 2, 3, 4}
len(u)
```

### Množinové operace (nemění vstupní množinu)
Průnik dvou množin, jinak také: `<set1>.intersection(<set2>)`
```python
u = {1, 2, 3, 4}
v = {3, 4, 5, 6}
u & v
```

Sjednocení dvou množin, jinak také: `<set1>.union(<set2>)`
```python
u = {1, 2, 3, 4}
v = {3, 4, 5, 6}
u | v
```

Rozdíl dvou množin, jinak také: `<set1>.difference(<set2>)`
```python
u = {1, 2, 3, 4}
v = {3, 4, 5, 6}
u - v
```

### Modifikace množin
Odebrání prvku z množiny
```python
u = {'a', 'x', 'y' ,'z'}
u.discard('y')
u
```

Přidání prvků do množinu
```python
u = {'a', 'x', 'y' ,'z'}
u.add('m')
u
```

> #### Příklad:
> Mějme dva seznamy se jmény zaměstnanců. 
> První seznam eviduje jména programátorů, druhý jména inženýrů. 
> Zaměstnanec může být současně na více seznamech. 
> 1. Vypište jména zaměstnanců jež jsou zapsáni na obou seznamech (tj. zaměstnanec je programátor i inženýr)
> 2. Vypište jména všech zaměstnanců.
```python
# Řešení
```

## Slovníky (dict)
**Klíč + hodnota**.

Jinde HashTable, Map, asociativní pole, ...

klasické schéma klíč - hodnota
implementováno pomocí hash table 

Vytvoření slovníku výčtem
```python
{'one': 1, 'two': 2, 'three': 3}
```

Vytvoření množiny funkcí `dict()`
```python
dict(one=1, two=2, three=3)
```

Vytvoření prázdného slovníku, ne množiny! pomocí `{}`
```python
{}
```

### Dotazu na slovnik
Získání prvků ze slovníku
```python
slovnik = {'one': 1, 'two': 2, 'three': 3}
print(slovnik['one'])
print(slovnik['two'])
```

Získání hodnoty ze slovníku, ale s výchozí hodnotou
```python
slovnik = {'one': 1, 'two': 2, 'three': 3}
slovnik.get("XXX", 0)
```

Získání množiny klíčů a hodnot a ze slovníku 
```python
slovnik = {'one': 1, 'two': 2, 'three': 3}
print(slovnik.keys())
print(slovnik.values())
print(slovnik.items())
```

Dotazy na přítomnost prvků ve slovníku
```python
slovnik = {'one': 1, 'two': 2, 'three': 3}
print("one" in slovnik)
print(1 in slovnik)
print(1 in slovnik.values())
print("one" in slovnik.items())
print(("one", 1) in slovnik.items())
```

### Modifikace slovníku

Přidáné nebo modifikace položky ve slovníku
```python
slovnik = {}
slovnik["one"] = 1
slovnik
```

Přidání více položok do slovníku
```python
slovnik = {"one":1, "two":2, "three":3}
slovnik.update({"four":4, "five":5})
slovnik
```

Modifikace zánamu ve slovníku
```python
slovnik = {"one":1, "two":2, "three":3}
slovnik["one"] = "jedna"
slovnik
```

Odtranění záznamu ze slovníku
```python
slovnik = {"one":1, "two":2, "three":3}
slovnik.pop("two")
slovnik
```

> #### Příklad:
> Potřebujeme evidovat jména lidí a pro každého 0 až N telefonních čísel.
> 
> - Zvolte vhodnou kombinaci datových typů, do které vše uložíte.
> - Připravte si proměnnou adresář.
```python
# Řešení
```

> - Vytvořte v adresáři prázdný záznam "Petr Novák".
```python
# Řešení
```

> - Přidejte Petrovi telefonní číslo "+420 123 456 789".
```python
# Řešení
```

> - Vypište všechna Petrova čísla.
```python
# Řešení
```

> - Přidejte Petrovi telefonní číslo "+666 987 654 321".
```python
# Řešení
```

> - Vypište Petrovo poslední číslo.
```python
# Řešení
```
> - Vypište, zdali v adresáři existuje člověk "Miro Žbirka".
```python
# Řešení
```

> - Vypište celkový počet osob uložených v adresáři.
```python
# Řešení
```

> - Vypište pouze jména osob uložených v adresáři.
```python
# Řešení
```

---

#### Další zdroje
- [Multiple assignment and tuple unpacking improve Python code readability](https://treyhunner.com/2018/03/tuple-unpacking-improves-python-code-readability/)

