# Regulární výrazy

Regulární výraz (regular expression – neplést s regulérním) je řetězec znaků se speciálním významem, které slouží jako předpis pro vyhledávání kusů textu. 
Pokud regulární výrazy neznáte, pak si je před čtením této kapitoli nejdříve nastudujte (třeba z www.regularnivyrazy.info), protože tato kapitola nepopisuje regulární výrazy, ale jen jak se dají použít v Pythonu.

Pro ladění regulárních výrazů existuje skvelá stránka: [regex101.com](https://regex101.com/).

## Modul `re`
Abychom mohli v Pythonu použít regulární výrazy, musíme nejprve provést `import`

```python
import re
```

## Základní použití:
Modul **re** obsahuje několik jednoduchých funkcí, které můžeme použít přímo.

### Funkce `search`
Nejjednoduší na použití je funkce `search()`, která, jak název napovídá, hledá v textu schody s regulárním výrazem.

Metoda vrátí objekt _sre.SRE_Match (match objekt), který obsahuje první (a jedinou) nalezenou schodu. 
Pokud `search()` schodu nenajde, vrací `None`.

Vyhledání textu v řetězci
```python
import re

str = "Praha je město"
mo = re.search(r"Praha",str)
if mo:
    print("je tam Praha")
else:
    print("neni tam Praha")
```

Z objektu `Match` lze ale získat i konkrétní podobu nalezeného podřetězce (pokud rexeg obsahuje napříkad znak `.` nebo `\w`) a také začátek a konec nalezeného podřetězce:
```python
m = re.search('eggs?', 'egg and egg are two eggs')
m.group(0), m.start(), m.end()

```

Určení zda řetězec obsahuje pouze čísla
```python
import re

str2 = "32732123"
mo = re.search(r"[0-9].",str2)
if mo:
    print("Jen cisla")
```

### Funkce `match`
Funkce `match()` funguje stejně jako `search()`, jen vyžaduje, aby hledaný vzor začínal na začátku prohledávaného řetězce.
```python
m = re.search('egg', 'xegg')
print(m.group(0))

m = re.match('egg', 'xegg')    # vrací None
print(type(m))
```

### Funkce `findall()`
Funkce `findall()` nalezne všechny schody v řetězci a vrátí je jako seznam.
```python
re.findall('eggs?', 'egg and egg are two eggs')
```

### Funkce `split()`
Funkce `split()` rozdělí řetězec podle regulárního výrazu.
```python
re.split('\W+', 'Words, words, words.')
```

## Kompilace regulárních výrazů
Funkce `compile()` modulu `re` slouží k vytvoření objektu regulárního výrazu z řetězce regulárního výrazu. 
Používání tohoto objektu namísto řetězce značně urychluje program. 
Kdykoliv budete nějaký regulární výraz používat více než jednou, nejdříve si jej zkomiplujte.

Zkompilovaný objekt můžete používat namísto textového řetězce. 
Navíc má i vlastní užitečné metody.
```python
pattern = re.compile('eggs?')
m = re.search(pattern, 'egg and egg are two eggs')
print("m.group(0):", m.group(0))
print("(m.start(), m.end())", m.start(), m.end())

re.findall(pattern, 'egg and egg are two eggs')
```

Zkompilovaný objekt obsahuje metody, které dělají totéž co funkce z modulu re. 
Jejich smyslem je jen ušetřit psaní. Následující dva výrazy jsou totožné.
```python
print(re.findall(pattern, 'egg and egg are two eggs'))
print(pattern.findall('egg and egg are two eggs'))
```

## Match objekt
Match objekt, který vrací například funkce `re.search()` nebo `re.match()` má několik užitečných metod a atributů. 
Nejdůležitější je určitě metoda `group()`.

```python
m = re.match(r"(\w+) (\w+)", "Malcolm Reynolds")
if m:
    print(m.group(0))
    print(m.group(1))
    print(m.group(2))
    print(m.groups())
```

----
Zdroje:
- [Regulární výrazy](https://www.sallyx.org/sally/python/regularni-vyrazy.php)
- [Řetězce](https://naucse.python.cz/lessons/beginners/str/)
- [Python: An Intro to Regular Expressions](http://www.blog.pythonlibrary.org/2016/06/08/python-an-intro-to-regular-expressions/)