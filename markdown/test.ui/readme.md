#Testování webových a GUI aplikací

## Testování webových dotazů
Mezi nejroštířenejší nástroje pro testování webových aplikací patrí [Selenium](https://www.selenium.dev/).

Jedná se o jazykově univerální nástroj pro psaní testů nad webowým gui. 
Kromě Pythonu poporuje také: Java, C#, Ruby JavaScriot a Kotlin.

Selenium ale není jedinným nástrojem pro testování webu exitují i další nástroje.
Mohu doporučit napříkad [WebPack](https://webpack.js.org/)

### Selenium a Python

Pro vyzkoušení si můžeme zkutit spustit jednoduchý příklad:
```python
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located

#This example requires Selenium WebDriver 3.13 or newer
with webdriver.Firefox() as driver:
    wait = WebDriverWait(driver, 10)
    driver.get("https://google.com/ncr")
    driver.find_element(By.NAME, "q").send_keys("cheese" + Keys.RETURN)
    first_result = wait.until(presence_of_element_located((By.CSS_SELECTOR, "h3>div")))
    print(first_result.get_attribute("textContent"))
```

[todo]: <> (## Interaktivní testování)
[todo]: <> (## Integrace prohlížeče)

## Možnosti testování GUI
Python umožňuje testovat i dektopové aplikace.

### Sikuli
Než se pustíme do nástojů v pythonu, odkáži Vás na projektu [Sikuli](http://sikuli.org/)

_V případě problému pak přímo stránky dokumentace: [Sikuli Documentation](http://doc.sikuli.org/)_.

Případně následovníka projekt: [SikuliX](http://sikulix.com/)

Jedná se sice o nástroj napsaný v jazyce Jave, ale je možné v něm psát i skypry v Jpytohnu (který odpovídý pythonu 2.7).
Hlavní výho sikuli je intuitivní IDE a možnsti hledání prvku na obrazovce pomocí orázků.

### PyAutoGUI
Jedná se o čistě python nástroj pro testování Gui.

Pro zajímavot je možné se podívat na video na youtube, kde tato knihovna hraje grafivkou hru [link](https://www.youtube.com/watch?v=lfk_T6VKhTE).

Na stránkách projektu je následující příklad:
```python
import pyautogui

screenWidth, screenHeight = pyautogui.size() # Get the size of the primary monitor.
currentMouseX, currentMouseY = pyautogui.position() # Get the XY position of the mouse.
pyautogui.moveTo(100, 150) # Move the mouse to XY coordinates.
pyautogui.click()          # Click the mouse.
pyautogui.click(100, 200)  # Move the mouse to XY coordinates and click it.
pyautogui.click('button.png') # Find where button.png appears on the screen and click it.
pyautogui.move(0, 10)      # Move mouse 10 pixels down from its current position.
pyautogui.doubleClick()    # Double click the mouse.
pyautogui.moveTo(500, 500, duration=2, tween=pyautogui.easeInOutQuad)  # Use tweening/easing function to move mouse over 2 seconds.
pyautogui.write('Hello world!', interval=0.25)  # type with quarter-second pause in between each key
pyautogui.press('esc')     # Press the Esc key. All key names are in pyautogui.KEY_NAMES
pyautogui.keyDown('shift') # Press the Shift key down and hold it.
pyautogui.press(['left', 'left', 'left', 'left']) # Press the left arrow key 4 times.
pyautogui.keyUp('shift')   # Let go of the Shift key.
pyautogui.hotkey('ctrl', 'c') # Press the Ctrl-C hotkey combination.
pyautogui.alert('This is the message to display.') # Make an alert box appear and pause the program until OK is clicked.
```

Pro instalaci na ubuntu doporučuji:

1. Vytvořit virtální prostředí:


    python3 -m venv venv
    source venv/bin/activate

2. Nastalovat knihovnu `tkinter`


    sudo apt-get install python3-tk python3-dev scrot

3. Spustit příkazem:
    
    
    python pyautogui_demo.py 
    
### pytest-qt
Jedná se plugin přímo do pytestu, který umožňje psát test pro aplikace napsané v [PySide](https://pypi.python.org/pypi/PySide), [PySide2](https://wiki.qt.io/PySide2) a [PyQt](http://www.riverbankcomputing.com/software/pyqt).

Na stránkách deokuemtace je následjívcí příklad:
```python
def test_hello(qtbot):
    widget = HelloWidget()
    qtbot.addWidget(widget)

    # click in the Greet button and make sure it updates the appropriate label
    qtbot.mouseClick(widget.button_greet, QtCore.Qt.LeftButton)

    assert widget.greet_label.text() == "Hello!"
```

Na různých fórech a diskuzích tent plugin často kombinují s předešlám nástojem PyAutoGUI.
    
    
----
Zdroje:
- [Sikuli](http://sikuli.org/)
- [SikuliX](http://sikulix.com/)
- [PyAutoGUI](https://pyautogui.readthedocs.io/en/latest/#)
- [Pytest-qt](https://pypi.org/project/pytest-qt/)