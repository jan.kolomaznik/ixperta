# Test-driven development

## Vývoj software s použitím testů

[//]: <> (Uvod)

### 1. Na staru je mšlenka, že chcete napsat metodu ...
- máte zhruba představu co by to mělo delat
- napíšete hlavičku metody

```python
def find_book(name, author = None):
    pass
```

### 2. Máte havičku co dál! 
- Rohodně ne hned implementovat
- Vymyslet komenář a to má
  a. Ještě neznáte implementaci a protot ji nemužet popsat do komentáře (popisovat implementac je špatně)
  b. Takže se soutředíte na učel a použítí metody
  c. HINT: zakažte si použít slova z názvu metody.

```python
def find_book_case(name, author = None):
    """
    When you need the collect of books with specific the specific title used this method.
    If you set tha name of the author then only his books are selected.
    """
    pass
```

### 3. Napisu test
- maximálně jednoduchý test udeálního použití

```python
import pytest

@pytest.fixture
def book_case():
    return [
        {"title": "Hobit",
         "printer": "argo",
        },
        {"title": "Hobit",
         "printer": "albatros",
        },
        {"title": "Pán prstenů",
         "printer": "argo",
        },
    ]

def test_find_book_case():
    assert len(find_book_case("Hobit")) == 2
```

Mate hotovo a jdete implementovat



[todo]: <> (## Ladění selhávajícího software)
[todo]: <> (## Ověření funkcanonality)

Testovaní knihoven


