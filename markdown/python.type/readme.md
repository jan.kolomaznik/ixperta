# Datové typy
Vše je objekt!

Dynamicky typovaný jazyk 
- proměně bez udaného datového typu


## Základní datové typy

- Logická hodnota: `bool`
- Celé číslo: `int`
- Desetinné číslo: `float`
- Komplexní číslo: `complex`
- Řetězce: `str`
- Datum a čas: `date`, `time`, `datetime`

## Prázdná hodnota
= null, nil, ...

```python
None
```

Porovnání s None => používáme `is`
```python
promena_s_hodnotou = "neco"
promena_s_hodnotou is None
```

```python
promena_bez_hodnoty = None
promena_bez_hodnoty is None
```

## Logické hodnoty (bool)
Pouze dvě hodnoty:
```python
pravda = True
nepravda = False
print(pravda, nepravda)
```

Negace a logické operátory
```python
print(not True)
print(True and False)
print(True or False)
```

Prakticky potřebujete použít proměnné
```python
pravda = True
nepravda = False
print(not pravda)
print(pravda and nepravda)
print(pravda or nepravda)
```

### Co je v Pythonu True?

No přece to, co není False. A False je:
- None
- False
- nula – 0, 0.0, 0j
- prázdná sekvence – '', (), [], {}
- další viz. [Truth Value Testing](https://docs.python.org/3/library/stdtypes.html#truth-value-testing)

Pozor na rozdílné způsoby vyhodnocení!
```python
x = 0
print(x)
print(x == False)
```

### Opertory porovnání
Pokud chcem zjistit, jestli se dvě hodnoty rovanjí požijeme `==`
```python
b = True
b == False
```

Pokud chceme zjistit, jestli se nerovnají `!=`
```python
b = True
b != False
```

**Pozor**, to že se dvě hodnoty rovnaji/nerovnají není to stejné, že **jsou stejné**.
Takže přestože platí že `1` je považováno za `True`
```python
a = 1
a == True
```

ale `1` není `True` 
```python
b = 1
b is True
```


## Číselné datové typy

Celé číslo
- int(<number>)
- zápis: `10`

Desetinné číslo
- float(<number>)
- zápis: `1.2`

Komplexní číslo 
- complex(<number>,<number>)
- zápis: `2+3j` nebo `2+3i`

### Základní operace 

```python
1 + 1
```

```python
8 - 1
```

```python
10 * 2
```

```python
(1 + 3) * 2
```

Dělení vždy vrací desetinné číslo
```python
35 / 5
```

Celočíselné dělení (ořízne desetinnou část)
```python
7 // 3
```

Module, tedy zbytek po celočíselném dělení
```python
7 % 3
```

Mocnění na n-tou mocninu
```python
2**4
```

Co když chci jiné číslo než mám?
```python
a = 1.5
b = 3
print("Desetinné číslo na celé", int(a))
print("Celé na desetinné ", float(b))
print("Dvě čísla na komplexní", complex(1,2))
```

> #### Příklad:
> Vytvořte vzoreček, který vypočítá kořeny kvadratické rovnice. Výsledky vypište na obrazovku.
>
> $x,y = \frac{-b \mp \sqrt{b^2 - 4ac}}{2a}$
```python
a, b, c = 1, 2, 1
#a, b, c = 1, 2, 5
# Řešení
d = ???
x = ???
y = ???
x, y
```

### Porovnávání
 
Provnání na rovnost a nerovnost
```python
1 == 1
```

```python
1 != 1
```

Porovnání na větší/menší
```python
1 < 10
```

```python
1 > 10
```

```python
2 <= 2
```

```python
2 >= 2
```

Zřetězení porovnání
```python
a = 2
1 < a <= 3
```

#### Příklad:
Mate tři proměnné `a`,`b`,`c`.
Ověřte zda splňují trojúhelníkovou nerovnost.

```python
a, b, c = 4, 4, 4
# Řešení
```

## Řetězce (str)
Jednoduché nebo dvojité uvozovky, nativní podpora UTF-8
```python
"Toto je řetězec."
```

```python
'Toto je také řetězec.'
```

### Skladní řetězců
```python
pozdrav = "ahoj"
jmeno = "Honza"
print(pozdrav + jmeno)
# Nebo ještě líp ???
```

Násobení řetězců
```python
3 * 'Python'
```

> #### Příklad:
> Pomocí sčítaní a násobení řetězců vypište slovo *prapraprababička*
```python
# Řešení
```

Zjišťování délky řetězce
```python
len('MujDlouhyText')
```

> #### Příklad:
> Načtěte text a vypište jeho délku
```python
# Řešení
```

### Řetězec jako seznam
Řetězec je seznam znaků!
![](media/str.png)

```python
slovo = 'Python' 
print(slovo[0])  
print(slovo[-6]) 
```

Získání části řetězce
```python
slovo = 'Python' 
print(slovo[-1]) 
print(slovo[0:2])
print(slovo[1:])
print(slovo[:2])
print(slovo[-2:]) 
print(slovo[:1] + slovo[2:])
print(slovo[1:-1])
print(slovo[:])
```

> #### Příklad:
> Načtěte vstup od uživatele a vypište jen jeho druhou polovinu. 
```python
# Řesení
```

## Formátování řetězce
Skládáme pomocí .format()
```python
"řetězec {} ze {} částí".format("složený", 3.14)
```

### Metody objektu `String`

*Metoda* (angl. *method*) je jako funkce – něco, co se dá zavolat.
Na rozdíl od funkce je svázaná s nějakým *objektem* (hodnotou).
Volá se tak, že se za objekt napíše tečka,
za ní jméno metody a za to celé se, jako u funkcí, připojí závorky
s případnými argumenty.

Řetězcové metody `upper()` a `lower()`
převádí text na velká, respektive malá písmena.

```python
retezec = 'Ahoj'
print(retezec.upper())
print(retezec.lower())
print(retezec)
```

> [note]
> Všimni si, že původní řetězec se nemění; metoda vrátí nový řetězec, ten
> starý zůstává.
>
> To je obecná vlastnost řetězců v Pythonu: jednou existující řetězec se už
> nedá změnit, dá se jen vytvořit nějaký odvozený.


#### Iniciály

Pro procvičení metod a vybírání znaků si zkus napsat program,
který se zeptá na jméno, pak na příjmení
a pak vypíše iniciály – první písmena zadaných jmen.

Iniciály jsou vždycky velkými písmeny
(i kdyby byl uživatel líný mačkat Shift).

```python
jmeno = input('Zadej jméno: ')
prijmeni = input('Zadej příjmení ')
inicialy = jmeno[0] + prijmeni[0]
print('Iniciály:', inicialy.upper())
```

Způsobů, jak takový program napsat, je více.
Lze například zavolat `upper()` dvakrát – zvlášť na jméno a zvlášť na příjmení.

Nebo to jde zapsat i takto –
metoda se dá volat na výsledku jakéhokoli výrazu:

```python
jmeno = input('Zadej jméno: ')
prijmeni = input('Zadej příjmení ')
print('Iniciály:', (jmeno[0] + prijmeni[0]).upper())
```

Doporučuji spíš první způsob, ten se smysluplnými názvy proměnných.
Je sice delší, ale mnohem přehlednější.

Řetězcových metod je celá řada.
Nejužitečnější z nich najdeš v [taháku](https://pyvec.github.io/cheatsheets/strings/strings-cs.pdf), který si můžeš stáhnout či vytisknout.

A úplně všechny řetězcové metody jsou popsány v [dokumentaci Pythonu](https://docs.python.org/3/library/stdtypes.html#string-methods) (anglicky; plné věcí, které ještě neznáš).

Všimni si, že `len` není metoda, ale funkce; píše se `len(r)`, ne `r.len()`.
Proč tomu tak je, to za nějakou dobu poznáš.


Nahrazení části řetězce
```python
slovo = 'den'
slovo.replace('d', "l")
```

Testování zda řetězec obsahuje písmena
```python
slovo = 'Python'
slovo.isalpha()
```

Testování zda řetězec obsahuje čísla
```python
slovo = 'Python'
slovo.isdigit()
```

> #### Příklad:
> Zjistěte, zda uživatelem zadaný vstup obsahuje pouze čísla.
```python
# Řešení
```


> #### Příklad
> 
> Zkus napsat funkci `zamen(retezec, pozice, znak)`.
> 
> Tato funkce vrátí řetězec, který má na dané pozici
> daný znak; jinak je stejný jako původní `retezec`. Např:
> 
```python
zamen('palec', 0, 'v') == 'valec'
zamen('valec', 2, 'j') == 'vajec'
```
>
> Pozor na to, že řetězce v Pythonu nelze měnit.
> Musíš vytvořit nový řetězec poskládaný z částí toho starého.
>
```python
# Řešení
```

## Datum a čas
Python poskytuje tři typy: : `date`, `time`, `datetime`.

Pro jejich použití je ale potřeba naimpotrovat modul, podobně jako jsme importovali modul pro `math`.
To je také důvod proč se tu o nich zmíníme jen okrajově:
```python
from datetime import datetime
``` 

pokud chceme získtát aktulní čas, pak můžeme použit funkci `now()`.
```python
start = datetime.now()
start
```

Dále můžeme z objektu získat konkrétní hodnoty, například rok:
```python
start.year, start.month, start.day, start.hour, start.minute, start.second
```

Pokud byvhom chtěli čas výpsat v lidsky čitelném formátu, můžeme použit funkci `strftime('%d.%m.%Y')`:
```python
start.x('%d.%m.%Y')
```

Existuje i funkce `strptime()` která naopak převede string na datum a čas:
```python
stop = datetime.strptime('23.08.2019', '%d.%m.%Y')
```

Pokud chceme zjistit, kolik času uběhlo, můžeme objekty jednoduše odečíst:
```python
stop - start
```

## Konverze datových typů
= Používáme funkce pojmenované dle datového typu.

```python
int("665") + 1
```

```python
int(3.14)
```

```python
float("3.14") * 2
```

```python
bool("cokoliv")
```
