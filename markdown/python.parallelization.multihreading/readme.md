# Multithreading

Multithreading je vláknový modul zahrnutý v Pythonu od verze 2.4.
Pposkytuje mnohem silnější podporu pro práci s vlákny než zastaralý modul `_thread`.

Poskytuje základní metody pro globální zprávu vláken:
* `threadding.activeCount()` - Vrací počet aktivních vláken v procesu.
* `threadding.currentThread()` - Vrací ukazatel na instanci aktuálního vlákna.
* `threadding.enumerate()` - Vrátí seznam všech aktivních objektů podprocesu.
* `threading.main_thread()` - Vrací instanci hlavního vlákna aplikace.

Každé vlákno ma dále své vlastní metody:
* `run()` - je vstupním bodem pro vlákno.
* `start()` - spustí vlákno voláním metody run.
* `join([time]) ` - čeká na ukončení jiného vlákena.
* `isAlive()` - kontroluje, zda vlákno stále probíhá.
* `getName()` - vrací název vlákna.
* `setName()` - nastavuje název vlákna.
* `deamon` - označuje vlákno jako demonické.

## Vytváření vláken pomocí Threading modulu
V uvodním příkladě jsme si ukázakali jak vytvořit vlákno jako wraper metody/funkce pomocí konstruktoru: `threading.Thread(target=calc_square, args=(number,))`.

Toto je jeden z několika možných způsobů. 
Druhým způspobem je definovat vlákno jako novou třídu: 

1. Vytvořením potomak třády `threading.Thread`.
2. Přepsat metodu `__init __(self [, args])` přidat další argumenty.
3. Poté přepište metodu `run(self [, args])` a implementujte, co by podproces měl udělat při spuštění.

Po vytvoření nové podtřídy vláken můžete vytvořit její instanci a poté spustit nové vlákno vyvoláním metody `start()`, která zase volá metodu `run()`.

```python
import threading
import time

exitFlag = 0

class myThread (threading.Thread):
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
    def run(self):
        print ("Starting " + self.name)
        print_time(self.name, self.counter, 5)
        print ("Exiting " + self.name)

def print_time(threadName, delay, counter):
    while counter:
        if exitFlag:
            threadName.exit()
        time.sleep(delay)
        print ("%s: %s" % (threadName, time.ctime(time.time())))
        counter -= 1

# Create new threads
thread1 = myThread(1, "Thread-1", 1)
thread2 = myThread(2, "Thread-2", 2)

# Start new Threads
thread1.start()
thread2.start()
thread1.join()
thread2.join()
print ("Exiting Main Thread")
```

## Synchronization
Díky *GIL* není synchronizace v pythnu tak kriciká jako v jiných jazycích, ale přesto je nutné s ní počítat.

Synchronizace vláken je definována jako mechanismus, který zajišťuje, že dvě nebo více souběžných vláken neprovedou současně nějaký konkrétní programový segment známý jako *kritická sekce*.

Například v níže uvedeném diagramu se 3 vlákna pokusí získat přístup ke sdílenému prostředku nebo kritické sekci současně.

![](media/multithreading-python-1.png)



Souběžné přístupy ke sdíleným prostředkům mohou vést k počkození dat.
Tedy dvě nebo více vláken mohou přistupovat ke sdíleným datům a snaží se je změnit současně. 
Výsledkem je, že hodnoty proměnných mohou být nepředvídatelné a mohou se lišit v závislosti na časování kontextových přepínačů procesů.

Vezměte v úvahu níže uvedený program:
```python
import threading 

# global variable x 
x = 0

def increment(): 
    """ 
    function to increment global variable x 
    """
    global x 
    x += 1

def thread_task(): 
    """ 
    task for thread 
    calls increment function 100000 times. 
    """
    for _ in range(100000): 
        increment() 

def main_task(): 
    global x 
    # setting global variable x as 0 
    x = 0

    # creating threads 
    t1 = threading.Thread(target=thread_task) 
    t2 = threading.Thread(target=thread_task) 

    # start threads 
    t1.start() 
    t2.start() 

    # wait until threads finish their job 
    t1.join() 
    t2.join() 

for i in range(10): 
    main_task() 
    print("Iteration {0}: x = {1}".format(i,x)) 
```

### Vysvětlení situace
Ve funkci `main_task` se vytvoří dvě vlákna `t1` a `t2` a globální proměnná `x` je nastavena na 0.
Každé vlákno má cílovou funkci `thread_task`, ve které se inkrementální funkce nazývá 100 000 krát.
Funkce `increment()` zvýší globální proměnnou x o 1 v každém volání.
Očekávaná konečná hodnota `x` je `200 000`, ale to, co dostaneme v 10 iteracích funkce `main_task`, jsou některé odlišné hodnoty.

To se děje kvůli současnému přístupu vláken ke sdílené proměnné `x`. 
Tato nepředvídatelnost v hodnotě `x` není ničím jiným než problémem souběžného přístupu.

Níže je uveden diagram, který ukazuje, jak může dojít ke stavu závodu ve výše uvedeném programu:

![](media/multithreadingfinal.png)

Všimněte si, že očekávaná hodnota x ve výše uvedeném diagramu je 12, ale kvůli stavu obou vláken se ukáže, že je 11!

Proto potřebujeme nástroj pro správnou synchronizaci mezi více vlákny.

## Třída Lock (zámek)
Jedním ze způsobů syncronizace jsou zámky.
Zámek je implementován pomocí objektu `Semaphore` poskytovaného operačním systémem.

Třída `Lock` poskytuje následující metody:
* `acquire([blocking])` : Získání zámku. 
    Zámek může být blokující nebo neblokující.
    * Při vyvolání argumentu blokování nastaveného na `True` (výchozí) je provádění podprocesu blokováno, dokud není zámek odemknut, pak je zámek nastavený na uzamčený a vrátí `True`.
    * Při vyvolání argumentu blokování nastaveného na hodnotu `False` není provádění podprocesu blokováno. 
        Pokud je zámek odemknutý, nastavte jej na uzamknutou a vraťte `True` nebo vrací `False`, pokud je zemknutý, ale vlákno pokračuje okamžítě dál.
* `release()`: Pro uvolnění zámku.
    * Když je zámek uzamčen, resetujte jej na odemknutý a vraťte se. 
        Pokud jsou některá další vlákna blokována a čeká na odemknutí zámku, nechte pokračovat přesně v jednom z nich.
    * Pokud je zámek již odemknutý, je vyvolána výjimka `ThreadError`.
     
Zvažte příklad uvedený níže:
```python
import threading 

# global variable x 
x = 0

def increment(): 
    """ 
    function to increment global variable x 
    """
    global x 
    x += 1

def thread_task(lock): 
    """ 
    task for thread 
    calls increment function 100000 times. 
    """
    for _ in range(100000): 
        lock.acquire() 
        increment() 
        lock.release() 

def main_task(): 
    global x 
    # setting global variable x as 0 
    x = 0

    # creating a lock 
    lock = threading.Lock() 

    # creating threads 
    t1 = threading.Thread(target=thread_task, args=(lock,)) 
    t2 = threading.Thread(target=thread_task, args=(lock,)) 

    # start threads 
    t1.start() 
    t2.start() 

    # wait until threads finish their job 
    t1.join() 
    t2.join() 

for i in range(10): 
    main_task() 
    print("Iteration {0}: x = {1}".format(i,x)) 

```

### Vysvětlení situace
Pokusme se pochopit výše uvedený kód krok za krokem:

Nejprve se vytvoří objekt Lock pomocí:

    lock = threading.Lock ()

Poté je zámek předán jako argument cílové funkce:

    t1 = threading.Thread (target = thread_task, args = (lock,))
    t2 = threading.Thread (target = thread_task, args = (lock,))

V kritické části cílové funkce aplikujeme zámek pomocí metody `lock.acquire()`. 
Jakmile je zámek získán, žádné jiné vlákno nemá přístup do kritické sekce (zde funkce přírůstku), dokud není zámek uvolněn pomocí metody `lock.release()`.

    lock.acquire()
    increment()
    lock.release()

Jak vidíte ve výsledcích, konečná hodnota x vychází 200 000 pokaždé (což je očekávaný konečný výsledek).

Níže je uveden diagram, který ukazuje implementaci zámků ve výše uvedeném programu:

![](media/multithreading-python-3.png)

### Bezpečnější zápis zámků
Aby jsem omezili nebezpečí nechtěného neodemknutí zámku, je vhodné používat zámky (a další synchronizační struktury) v `with` bloku:

Zápis:
```python
with some_lock:
    # do something...
```

je ekvivaletní k 
```python
some_lock.acquire()
try:
    # do something...
finally:
    some_lock.release()
```

### Vlastnění zámků
Základní zámek není vlastněn zádným vláknem, je tedy ve stuvu zamčeno/odemčeno.

Pokud jedno vlákno se pokusí ten stejný zámek zíkat opakovaně, tak je u druhého pokusu přeručeno.
Pokud potřebujete, aby jedno hlákno získávalo zámek opakovaně (například kvuli rekurzi), pak použijte třídu `RLock`, která jinak fungije stejně jako `Lock`.

### Nevýhody synchronizace
Synchronizace ale přináši dvě základní rizika, se kterými se musí programátor vytovnat:
* Synchronizace stojí nějakou provozní režii. 
    Zkuste změžit dobu výpočtu obou předchozích případů pomocí `%%timeit`
* Může dovést k uzánutí *deat locku*, kdy program přestane ragovat.

## Zasílání zpráv mezi vlákny
Zámky se používání k ochraně *kritických sekcí*.
Uvažujme ale jiný příklad, kdy máme dvě vlákna:

* `thread-1`: geneguje data
* `thread-2`: je zpracovává

Potřebuje zařídit aby vlákno 2 čekalo, dokud první vlákno nedokončí generování.
V takovýchto případech **nikdy** a to **bez výjimky** nepoužívejt čekání ve smyčce. 
Pro tyto učely můžeme použít `Condition` objekt.

#### Do not kill ...
![](media/pinguin.jpeg)

V následujícím příkladu spotřebitelská vlákna čekají na nastavení podmínky před pokračováním. 
Producentské vlákno je zodpovědné za nastavení stavu a za oznámení dalších vláken, které mohou pokračovat.
```python
import threading
import time

def consumer(cv):
    print('Consumer thread started ...')
    with cv:
        print('Consumer waiting ...')
        cv.wait()
        print('Consumer consumed the resource')

def producer(cv):
    print('Producer thread started ...')
    with cv:
        print('Making resource available')
        print('Notifying to all consumers')
        cv.notifyAll()

condition = threading.Condition()
cs1 = threading.Thread(target=consumer,args=(condition,))
cs2 = threading.Thread(target=consumer, args=(condition,))
pd = threading.Thread(target=producer, args=(condition,))

cs1.start()
time.sleep(2)
cs2.start()
time.sleep(2)
pd.start()
```

Všimněte si, že jsme nepoužívali metody `acquire()` a `release()`, místo nich jsme použili block `with`.
Místo toho naše vlákna použitá s získat zámek spojený s podmínkou.

Metoda `wait()` uvolní zámek a poté jej zablokuje, dokud jej jiný podproces neprobudí voláním `notify()` nebo `notify_all()`.

Všimněte si, že metody `notify()` a `notify_all()` jsou dovnže požívány v zámku a neuvolňují jej.
To znamená, že probuzené vlákno nebo vlákna se nevrátí ze svého volání `wait()` okamžitě, ale pouze tehdy, když se vlákno, které volalo `notify()` nebo `notify_all()` opustí sekci se zámkym.

Typický styl programování pomocí proměnných podmínek používá zámek k synchronizaci přístupu k některému sdílenému stavu.
Vlákna, která mají zájem o konkrétní změnu stavu, volání opakovaně `wait()`, dokud neuvidí požadovaný stav.
Zatímco vlákna, která modifikují volání státu, volají  `notify() nebo `notify_all()`, když změní stav.
 
Například následující kód představuje obecnou situaci výrobce a spotřebitele s neomezenou kapacitou vyrovnávací paměti:
    
    # Consume one item
    with cv:
        while not an_item_is_available():
            cv.wait()
        get_an_available_item()
    
    # Produce one item
    with cv:
        make_an_item_available()
        cv.notify()

## Další synchronizační strutury:
* [Semaphore Objects](https://docs.python.org/3/library/threading.html#semaphore-objects)
* [Event Objects](https://docs.python.org/3/library/threading.html#event-objects)
* [Timer Objects](https://docs.python.org/3/library/threading.html#timer-objects)
* [Barrier Objects](https://docs.python.org/3/library/threading.html#barrier-objects)

