# Testování databázových aplikací

V následující si ukážeme jak se daji pomocí pythonu testovat databáze.

Nejprve intalace potřebných závislotí a importy potřebných knihoven:
```python
#pip install pytest ipytest
import pytest
import ipytest
ipytest.autoconfig()
```

[todo]: <> (## Nastavení výchozího stavu)
[todo]: <> (## Testované operace)
[todo]: <> (## Ověření cílového stavu)

## Fixtures
`Fixture` se skvěle hodí i pro testování databází.
Umožňují nám totiž vykonat otevření databáze před testy.

Navíc se můžeme snadno rohodnout, jetli chcme otevřít novou databázi pro každý test, nebo držet jedno spojenní pro všechny testy.

_Z hledika atomičnosti testů je lepší první přístup._

V takovém případě ale potřebujeme i každé spojení po ukončení testu uzavřít.
Pokud potřebujete po použití s fixturou ještě něco udělat, můžete místo return použít yield. 
Často se to používá u zdrojů, které je po použití potřeba nějak finalizovat či zavřít, například u databázových spojení. 
Zde je ilustrační příklad, který si můžete rovnou vyzkoušet:

```python
%%run_pytest[clean]

class DBConnection:
    def __init__(self, name):
        print('Creating connection for ' + name)
        ...

    def select(self, arg):
        return arg

    def cleanup(self):
        print('Cleaning up connection')
        ...


@pytest.fixture
def connection():
    d = DBConnection('sqlite')
    yield d
    d.cleanup()


@pytest.mark.parametrize('arg', (1, float, None))
def test_with_fixture(connection, arg):
    assert arg == connection.select(arg)
```

Standardní výstup (stderr a stdout) z testů se normálně zobrazuje, jen když test selže. Chceme-li výstup vidět u všech testů, je třeba použít přepínač -s.

I `fixture` jdou parametrizovat, jen trochu jiným způsobem než testovací funkce: 
parametry předané dekorátoru `pytest.fixture` získáme ze speciálního parametru `request`, který obsahuje informace o probíhajícím testu:

```python
@pytest.fixture(params=('sqlite', 'postgres'))
def connection(request):
    d = DBConnection(request.param)
    yield d
    d.cleanup()
```



### Použití pluginu 
https://pypi.org/project/pytest-docker-db/

----
##### Zdroje:
- [Nauč se Python: Testování 2](https://naucse.python.cz/lessons/intro/testing/)
- [5 Pytest Best Practices for Writing Great Python Tests](https://www.nerdwallet.com/blog/engineering/5-pytest-best-practices/)