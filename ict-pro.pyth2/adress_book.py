import functools

commands = {}
contacts = {}
select = None

def command(func):
    commands[func.__name__] = func
    return func

@command
def add_person(name):
    contacts[name] = []

@command
def select_person(name):
    global select
    select = name
    print(f"Person {select} is selected.")

@command
def add_number(number):
    if select is None:
        print(f"User funceion {select_person.__name__} first.")
        return

    contacts[select].append(number)
    print(f"Person {select} has numbers: {contacts[select]}.")    

while True:
    cmd = input("$")
    if cmd == "exit":
        break
    command, arg = cmd.split(" ", 2)
    commands[command](arg)
