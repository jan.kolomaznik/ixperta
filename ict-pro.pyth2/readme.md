Python - pokročilé programování
=============================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Kurz je určen vývojářům, kteří ovládají základy programování v jazyce Python a chtěji prohloubit své znalosti o pokročilejší techniky jako zpracování textu, tvorbu vícevláknových a paralelních aplikací, případně i jejich ladění.

Osnova kurzu:
------------

- **Seznámení s jazykem Python**
  - Variace (PyPy, Cython, Jython, RPython, Stackless Python, IronPython), 
  - Virtální prostředí
- **Základy jazyka**
- **Datové typy**
    - **[Řetězce: Formátování](00-python.type.str.ipynb)**
    - **[Regulární výrazy](01-python.type.regex.ipynb)**
- **Základní konstrukce jazyka**
    - **[Ternární operátor](02-python.construction.if.ipynb)**
    - **[For při generování seznamů](03-python.construction.for.ipynb)**
- **Struktury (generátory a iterátory)**
    * data-class
    * named-tuple
- **Funkce**
    - **[Lambdas](04-python.function.lambdas.ipynb)**
    - **[Dekorátory](05-python.function.decorator.ipynb)**
    - **[Functools](06-python.function.functools.ipynb)**
    * Generatory
    - **[Itertools](07-python.function.itertools.ipynb)**
- **Třídy a objekty**
    - **[Magické metody](08-python.object.magic.ipynb)**
    - **[First-Class Objects](09-python.object.first-class.ipynb)**
    - **[Deskriptory](10-python.object.descriptor.ipynb)**
    - **[Konstruktor](11-python.object.constuctor.ipynb)**
    - **[Metatřídy](12-python.object.metaclass.ipynb)**
    * Vícenásobná dědičnost
- **Paralelizace**
    - **[Úvod multithreading vs. multiprocessing](13-python.parallelization.ipynb)**
    - **[Multithreading](14-python.parallelization.multihreading.ipynb)**
    - **[Multiprocessing](15-python.parallelization.multiprocessing.ipynb)**
    * Concurrent.futures
- **Práce s daty**
    * pickle
    * shelve
    * json
    * databáze
- **Tvorba aplikace**
    * Vytvoření a organizace velkého projektu
    * Tvorba vlastního modulu a jeho distribuce
    * logování
- **Testování**
    - **[Testování](16-python.test.unittest.ipynb)**
 